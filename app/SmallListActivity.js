import React, {Component} from 'react';
import {View, FlatList, Image, Text, TouchableWithoutFeedback, RefreshControl} from 'react-native';
import {RequestsController} from "./components/utils/RequestsController";
import {SmallCardAdapter} from "./components/SmallCardAdapter";
import {SafeAreaView} from 'react-navigation';
import LottieView from "lottie-react-native";

export class SmallListActivity extends React.Component {
    constructor(props) {
        super(props);
        this.token = props.navigation.getParam('token', 'NA');
        this.page = props.navigation.getParam('page', 'saved');
        this.state = {
            data: [],
            showDeletes: false,
            isLoading: false
        };
        this.postLoaded = 0;
        this.listRef = new Map();
        this.openSingleItem = this.openSingleItem.bind(this);
        this.showDeletes = this.showDeletes.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.loadPosts = this.loadPosts.bind(this);
        this.loadPosts();
    }

    async loadPosts() {
        this.setState(() => {
            return {
                isLoading: true
            }
        });
        let method = RequestsController.getSaveds;
        if (this.page === 'liked') method = RequestsController.getLikeds;
        else if (this.page === 'history')
            method = RequestsController.getHistory;
        let d = await method(this.token, 10, this.postLoaded);
        if ((d !== undefined && d.length !== 0 || this.state.data.length !== 0)) {
            this.setState(() => {
                return {data: this.state.data.concat(d), isLoading: false};
            });
            this.postLoaded += 10;
        } else {
            this.setState(() => {
                return {data: undefined, isLoading: false};
            });
        }
    }

    openSingleItem(post_id) {
        this.props.navigation.navigate('SingleItem', {
            id: post_id,
            token: this.token
        });
    }

    showDeletes() {
        this.setState(() => {
            return {showDeletes: true}
        });
    }

    async deleteItem() {
        for (let index in this.state.data) {
            let item = this.state.data[index];
            if (this.listRef.get(item.post_id) !== null) {
                let id = this.listRef.get(item.post_id).deleteSelf();
                if (id !== "-1") {
                    if (this.page === 'like')
                        RequestsController.sendRemoveUpVote(this.token, id);
                    if (this.page === 'history')
                        RequestsController.sendRemoveHistory(this.token, id);
                    if (this.page === 'saved')
                        RequestsController.sendRemoveSaved(this.token, id);
                    this.listRef.delete(item.post_id);
                    //console.log("deleteing ", index);
                    this.state.data.splice(index, 1);
                }
            }
        }
        //console.log("now delete is false");
        this.setState(() => {
            return {showDeletes: false, data: this.state.data}
        });
    }

    viewConfig = {viewAreaCoveragePercentThreshold: 10};

    render() {
        let list = this.state.data === undefined ?
            <View style={{
                alignItems: 'center', justifyContent: 'center',
                flex: 1, backgroundColor: "#FFF"
            }}>
                <Text style={{textAlign: 'center', fontSize: 18, fontFamily: 'IRANSansMobile'}}>داده
                    ای برای نمایش وجود
                    ندارد</Text>
            </View> :
            <FlatList
                data={this.state.data}
                keyExtractor={(item, index) => item.post_id.toString()}
                extraData={this.state.showDeletes}
                viewabilityConfig={this.viewConfig}
                onEndReached={() => {
                    if (!this.state.isLoading) {
                        this.loadPosts();
                    }
                }}
                onEndReachedThreshold={0.9}
                renderItem={(item) =>
                    <SmallCardAdapter
                        item={item}
                        key={item.post_id} token={this.token}
                        show={this.state.showDeletes}
                        callbackDelete={this.showDeletes}
                        ref={(ref) => {
                            this.listRef.set(item.item.post_id, ref);
                        }}
                        callback={this.openSingleItem}/>
                }/>;
        let delIcon = !this.state.showDeletes ? null :
            <TouchableWithoutFeedback onPress={this.deleteItem}>
                <Image source={require("./components/img/ic_trash.png")}
                       style={{
                           height: 22, width: 22, resizeMode: 'contain',
                           position: 'absolute', right: 25, top: 15
                       }}/>
            </TouchableWithoutFeedback>;
        let anim = this.state.isLoading ?
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <LottieView
                    source={require("./components/animation_loading")}
                    style={{alignSelf: 'center', height: 50, position: 'absolute'}}
                    autoPlay
                    resizeMode={"contain"}
                    loop/></View> : null;
        return (
            <SafeAreaView style={{flexDirection: 'column', flex: 1, backgroundColor: "#FFFFFF"}}>
                <View style={{
                    height: 52, backgroundColor: "#F5F4F4", flexDirection: 'row'
                }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.pop()}>
                        <Image source={require("./components/img/ic_back.png")}
                               style={{
                                   alignSelf: 'flex-start', height: 22, width: 22, resizeMode: 'contain',
                                   marginTop: 15, marginLeft: 25
                               }}/>
                    </TouchableWithoutFeedback>
                    <View style={{
                        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
                        alignItems: 'center', justifyContent: 'center'
                    }}>
                        <Image source={require("./components/img/ic_logo.png")}
                               style={{
                                   height: 32, width: 32, resizeMode: 'contain'
                               }}/>
                    </View>
                    {delIcon}
                </View>
                {list}
                {anim}
            </SafeAreaView>
        );
    }
}