"use strict";
import React from 'react';
import {
    Animated, AppState, BackHandler, Dimensions, FlatList, Image,
    ImageBackground, PixelRatio, RefreshControl, Text, TouchableWithoutFeedback, View
} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import {CardAdapter} from './components/CardAdapter';
import {ProfileView} from './components/ProfileView';
import {RequestsController} from './components/utils/RequestsController';
import {GoogleAnalyticsTracker} from 'react-native-google-analytics-bridge';
import LottieView from 'lottie-react-native';
import Toast from 'react-native-easy-toast';
import {DBManager} from "./components/utils/DBManager";
import LinearGradient from "react-native-linear-gradient";
import ChannelList from "./components/ChannelList";
import Orientation from 'react-native-orientation';
import uuid from 'react-native-uuid';
import CategoryIcon from "./components/utils/CategoryIcon";
import firebase from "react-native-firebase";
import {NotificationsAndroid} from 'react-native-notifications'
import {Grayscale} from 'react-native-color-matrix-image-filters';
import PTRView from 'react-native-pull-to-refresh';
import OnBoardingButton from "./components/utils/OnBoardingButton";
import App from "../App";
import SplashScreen from 'react-native-splash-screen'
import RF from "react-native-responsive-fontsize"

export default class MainActivity extends React.Component {
    static tracker = new GoogleAnalyticsTracker('UA-119886205-1');
    static sID = uuid.v1();
    static totalSaveds = 0;
    static totalChannels = 3;
    static totalLikeds = 0;
    static totalDislikeds = 0;
    static totalImpression = 0;
    static totalReported = 0;
    static consecutiveDays = 0;
    static userLevel = 0;
    static currentLevelStars = [false, false, false, false, false];
    static token = null;

    constructor(props) {
        super(props);
        this.state = {
            currentTab: 0,
            data: [],
            videos: [],
            tags: [],
            for_you_icon: '',
            categories: [],
            currentItem: undefined,
            isLoading: true,
            showStar: false,
            showRobot: false,
            maskShow: false,
            maskOp: new Animated.Value(),
            marginBottomAnimated: new Animated.Value(),
            marginBottomSecond: new Animated.Value(),
            scaleRobotTab: new Animated.Value(0),
            appState: AppState.currentState,
            onBoardingPage: -2,
            onBoardingItemSelected: 0,
            level: 0,

            fixed_categories: [
                [
                    {
                        title: 'مذهبی',
                        selected: false,
                        id: 'a5215a19',
                        src: require('./components/category/religious.png')
                    },
                    {
                        title: 'ورزشی',
                        selected: false,
                        id: '34eccb19',
                        src: require('./components/category/sport.png')
                    },
                    {
                        title: 'سلبریتی',
                        selected: false,
                        id: 'ikflcnol',
                        src: require('./components/category/celebrity.png')
                    }],
                [
                    {
                        title: 'سلامت',
                        selected: false,
                        id: '978b02ef',
                        src: require('./components/category/health.png')
                    },
                    {
                        title: 'تاریخی',
                        selected: false,
                        id: '0779b214',
                        src: require('./components/category/history.png')
                    },
                    {
                        title: 'سینما',
                        selected: false,
                        id: 'd47fa331',
                        src: require('./components/category/cinema.png')
                    }],
                [
                    {
                        title: 'خبری',
                        selected: false,
                        id: 'e57020c9',
                        src: require('./components/category/news.png')
                    },
                    {
                        title: 'کودک',
                        selected: false,
                        id: '5a4c540d',
                        src: require('./components/category/baby.png')
                    },
                    {
                        title: 'سرگرمی',
                        selected: false,
                        id: '4fca0c8c',
                        src: require('./components/category/entertainment.png')
                    }],
                [
                    {
                        title: 'جنجالی',
                        selected: false,
                        id: '5a4c540d',
                        src: require('./components/category/hot.png')
                    },
                    {
                        title: 'حیوانات',
                        selected: false,
                        id: 'c5126fe9',
                        src: require('./components/category/animals.png')
                    },
                    {
                        title: 'جملات',
                        selected: false,
                        id: '4ec49fdf',
                        src: require('./components/category/sentences.png')
                    }],
                [
                    {
                        title: 'آشپزی',
                        selected: false,
                        id: 'd5e82c24',
                        src: require('./components/category/cooking.png')
                    },
                    {
                        title: 'دانستنی',
                        selected: false,
                        id: '94e9f425',
                        src: require('./components/category/knowledge.png')
                    },
                    {
                        title: 'روانشناسی',
                        selected: false,
                        id: '5b90bd61',
                        src: require('./components/category/psychology.png')
                    }]
            ]
        };
        this.robotIcons = [require('./components/img/mr_robot_one.png'), require('./components/img/mr_robot_two.png'),
            require('./components/img/mr_robot_three.png'), require('./components/img/mr_robot_four.png')];
        this.robotFilledIcons = [
            require('./components/img/mr_robot_one_filled.png'),
            require('./components/img/mr_robot_two_filled.png'),
            require('./components/img/mr_robot_three_filled.png'),
            require('./components/img/mr_robot_four_filled.png')];
        this.animRobotTab = this.state.scaleRobotTab.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.5, 1]
        });
        this.postLoaded = 0;
        this.videosLoaded = 0;
        this.trendRef = new Map();
        this.postListRef = null;
        this.tagListRef = null;
        this.channelListRef = null;
        this.impression = new Map();
        this.reportChannelName = "";
        this.reportPostId = "";
        this.maxWidth = 0;
        this.timeOnTab = new Date().getTime();
        this.timwWhenGoBack = -1;
        this.channelListClickedYet = false;
        this.alertForInteractable = false;
        this.viewItemsChanged = this.viewItemsChanged.bind(this);
        this.tagClicked = this.tagClicked.bind(this);
        this.goFullScreen = this.goFullScreen.bind(this);
        this.goSingleItem = this.goSingleItem.bind(this);
        this.reportChannel = this.reportChannel.bind(this);
        this.showToast = this.showToast.bind(this);
        this.loadPosts = this.loadPosts.bind(this);
        this.onBoardingChannel = this.onBoardingChannel.bind(this);
        this.handleBackPress = this.handleBackPress.bind(this);
        this.hideVPN = this.hideVPN.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this._rotateStar = this._rotateStar.bind(this);
        this._showStar = this._showStar.bind(this);
        this.checkUserStats = this.checkUserStats.bind(this);
        this._handleLayout = this._handleLayout.bind(this);
        this._itemClicked = this._itemClicked.bind(this);
        this.afterOnBoarding = this.afterOnBoarding.bind(this);
        this.categoryChanged = this.categoryChanged.bind(this);
        this.getToken();

        this.lastTag = undefined;
        this.currentTag = undefined;
        this.first_run = undefined;
        this.onBoardingRef = null;
        this.starRotation = new Animated.Value(0);
        this.starTopMargin = new Animated.Value(0);
        this.starLeftMargin = new Animated.Value(0);
        this.starRotationScale = this.starRotation.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        this.starScale = new Animated.Value(1);

        this.pageThreeTop = new Animated.Value(-400);
        this.pageFourTop = new Animated.Value(-500);
        this.pageFiveBottom = new Animated.Value(-500);
        this.pageSixBottom = new Animated.Value(-500);

        this.pageSevenRight = new Animated.Value();
        this.pageSevenLeft = new Animated.Value();
        this.pageEightRight = new Animated.Value();
        this.pageEightLeft = new Animated.Value();

        this.currentItemRightMargin = new Animated.Value(-100);
    }

    _handleLayout(event) {
        this.setState({
            borderRadius: event.nativeEvent.layout.width / 3
        });
    }

    _itemClicked(event, flag) {
        this.state.fixed_categories.forEach((item) => {
            if (item[0].id === event.id) {
                RequestsController.dislikeCategory(MainActivity.token, item[0].id, !item[0].selected);
                item[0].selected = !item[0].selected;
            }
            if (item.length > 1 && item[1].id === event.id) {
                RequestsController.dislikeCategory(MainActivity.token, item[1].id, !item[1].selected);
                item[1].selected = !item[1].selected;

            }
            if (item.length > 2 && item[2].id === event.id) {
                RequestsController.dislikeCategory(MainActivity.token, item[2].id, !item[2].selected);
                item[2].selected = !item[2].selected;
            }
        });
        this.state.onBoardingItemSelected += flag ? +1 : -1;
        if (this.state.onBoardingItemSelected === 2 || this.state.onBoardingItemSelected === 13)
            this.onBoardingRef.changeState(false);
        else if (this.state.onBoardingItemSelected === 3 || this.state.onBoardingItemSelected === 12)
            this.onBoardingRef.changeState(true);
    }

    async afterOnBoarding() {
        this.setState(() => {
            return {
                onBoardingPage: -1,
                data: [],
                videos: []
            }
        });
        MainActivity.currentLevelStars[0] = true;
        DBManager.setUserLevel('star0', true);

        await this.loadTags();
        await this.loadPosts();
        await this.loadVideos();
        setTimeout(() =>
            this.setState(() => {
                return {onBoardingPage: 2}
            }), 20000);
    }

    onBoardingChannel() {
        switch (this.state.onBoardingPage) {
            case 0:
                return (
                    <View style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        right: 0,
                        left: 0,
                        alignItems: 'center'
                    }}>
                        <LinearGradient
                            colors={["#641e87", "#444c97", "#0d809b"]}
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}>
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'space-around',
                                paddingStart: 30,
                                paddingEnd: 30
                            }}>
                                <Image
                                    source={require('./img/logo.png')}
                                    style={{
                                        width: 110,
                                        height: 110,
                                        marginTop: 20
                                    }}
                                    resizeMode={'contain'}/>
                                <Text
                                    style={{
                                        color: "#FFFFFF",
                                        fontSize: RF(5),
                                        fontFamily: 'IRANSansBold',
                                        textAlign: 'center'
                                    }}>
                                    «من» دستیار شخصی محتوایی
                                </Text>
                                <Text
                                    style={{
                                        color: "#FFFFFF",
                                        fontSize: RF(4),
                                        fontFamily: 'IRANSansMobile',
                                        textAlign: 'center'
                                    }}>
                                    در این اپلیکیشن هوش مصنوعی، محتوا را از سراسر وب و شبکه‌های اجتماعی جمع می‌کند و
                                    بر
                                    اساس رفتار شما در اپلیکیشن، محتوا را برای شما شخصی سازی می‌کند.
                                </Text>
                                <TouchableWithoutFeedback
                                    onPress={() => {
                                        this.setState(() => {
                                            return {onBoardingPage: 1}
                                        });
                                    }}>
                                    <View
                                        style={{
                                            borderRadius: 26,
                                            backgroundColor: "#FFFFFF",
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            marginTop: 40,
                                            width: 103,
                                            height: 52
                                        }}>
                                        <Text
                                            style={{
                                                fontSize: MainActivity.normalize(20),
                                                fontFamily: 'IRANSansBold',
                                                color: '#4A4A4A'
                                            }}>
                                            باشه
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <View/>
                            </View>
                        </LinearGradient>
                    </View>
                );
            case 1:
                return (
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0,
                            alignItems: 'center'
                        }}>
                        <LinearGradient
                            colors={["#9C1CBB", "#8B5BFF", "#7495FA", "#10C9EE"]}
                            start={{x: 0, y: 0}}
                            end={{x: 1, y: 0}}>
                            <LinearGradient
                                colors={["rgba(0,0,0,0.2)", "rgba(36,1,0,0.9)", "rgba(36,1,0,1)", "rgba(36,1,0,1)"]}
                                start={{x: 0, y: 0}}
                                end={{x: 0, y: 1}}>
                                <View
                                    style={{
                                        flex: 1,
                                        paddingStart: 10,
                                        paddingEnd: 10,
                                        alignItems: 'center'
                                    }}>
                                    <View
                                        style={{
                                            alignItems: 'center'
                                        }}>
                                        <View
                                            style={{
                                                borderRadius: 26,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                marginBottom: 20
                                            }}>
                                            <Text
                                                style={{
                                                    fontSize: RF(5),
                                                    fontFamily: 'IRANSansBold',
                                                    color: '#FFFFFF',
                                                    textAlign: 'center',
                                                    marginStart: 30,
                                                    marginEnd: 30,
                                                    marginTop: 15
                                                }}>
                                                حداقل سه موضوعی که
                                                دوست
                                                <Text
                                                    style={{
                                                        color: '#FF0000',
                                                        paddingStart: 2,
                                                        paddingEnd: 2
                                                    }}>
                                                    {' '}نداری {' '}
                                                </Text>
                                                رو انتخاب کن
                                            </Text>
                                        </View>
                                    </View>
                                    <FlatList
                                        style={{flex: 1}}
                                        ListFooterComponent={() => {
                                            return (
                                                <View
                                                    style={{height: this.maxWidth / 3}}>
                                                </View>
                                            );
                                        }}
                                        keyExtractor={(item, index) => index.toString()}
                                        showsVerticalScrollIndicator={false}
                                        data={this.state.fixed_categories}
                                        renderItem={({item}) => {
                                            return (
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        height: this.maxWidth / 3 + 10,
                                                        width: this.maxWidth - 20,
                                                        alignItems: 'center'
                                                    }}>
                                                    <CategoryIcon
                                                        height={this.maxWidth / 3 + 10}
                                                        bool={item[0].selected}
                                                        image={item[0].src}
                                                        caption={item[0].title}
                                                        item={item[0]}
                                                        callbackClicked={this._itemClicked}
                                                    />
                                                    <CategoryIcon
                                                        height={this.maxWidth / 3 + 10}
                                                        bool={item[1].selected}
                                                        image={item[1].src}
                                                        caption={item[1].title}
                                                        item={item[1]}
                                                        callbackClicked={this._itemClicked}
                                                    />
                                                    <CategoryIcon
                                                        height={this.maxWidth / 3 + 10}
                                                        bool={item[2].selected}
                                                        image={item[2].src}
                                                        caption={item[2].title}
                                                        item={item[2]}
                                                        callbackClicked={this._itemClicked}
                                                    />
                                                </View>)
                                        }}
                                    />
                                    <View
                                        style={{
                                            position: 'absolute',
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            alignItems: 'center'
                                        }}>
                                        <LinearGradient
                                            style={{width: '100%'}}
                                            colors={["rgba(36,1,0,0)", "rgba(36,1,0,0.7)", "rgba(36,1,0,1)"]}
                                            start={{x: 0, y: 0}}
                                            end={{x: 0, y: 1}}>
                                            <View
                                                style={{
                                                    width: '100%',
                                                    alignItems: 'center'
                                                }}>
                                                <OnBoardingButton
                                                    callbackClicked={this.afterOnBoarding}
                                                    ref={(ref) => this.onBoardingRef = ref}/>
                                            </View>
                                        </LinearGradient>
                                    </View>
                                </View>
                            </LinearGradient>
                        </LinearGradient>
                    </View>
                );
            case 2:
                return (
                    <TouchableWithoutFeedback>
                        <View
                            style={{
                                position: 'absolute',
                                top: 0, bottom: 0,
                                right: -30,
                                left: -30
                            }}>
                            <View
                                style={{flex: 1}}
                            />
                            <LinearGradient
                                colors={["#00CBF6", "#9c4af9", "#9C2BB5"]}
                                start={{x: 1, y: 0}} end={{x: 0, y: 1}}
                                style={{borderRadius: this.maxWidth / 2 + 40}}>
                                <View
                                    style={{
                                        width: this.maxWidth + 80,
                                        height: this.maxWidth + 80,
                                        alignItems: 'center'
                                    }}>
                                    <View
                                        style={{flex: 1}}/>
                                    <Image
                                        style={{
                                            width: 90,
                                            height: 80
                                        }}
                                        resizeMode={'stretch'}
                                        source={require('./components/img/mr_robot_one.png')}/>
                                    <View
                                        style={{flex: 1, justifyContent: 'center'}}>
                                        <Text
                                            style={{
                                                fontSize: RF(4.5),
                                                fontFamily: 'IRANSansBold',
                                                color: '#FFFFFF',
                                                marginTop: 10,
                                            }}>

                                            سلام به اپ «من» خوش اومدی
                                        </Text>
                                    </View>
                                    <Text
                                        style={{
                                            fontSize: 16,
                                            fontFamily: 'IRANSansMobile',
                                            marginTop: 10,
                                            marginStart: 60,
                                            marginEnd: 60,
                                            color: '#FFFFFF',
                                            textAlign: 'center',
                                            flex: 2
                                        }}>
                                        اگه می‌خوای که ربات هوش مصنوعی{'\n'} دقیقا محتوایی که دوس داری رو بهت{'\n'} نشون
                                        بده، باید
                                        یه
                                        کارایی انجام بدی.
                                    </Text>
                                    <Text
                                        style={{
                                            fontSize: RF(4.5),
                                            fontFamily: 'IRANSansBold',
                                            marginTop: 10,
                                            marginStart: 60,
                                            marginEnd: 60,
                                            color: '#FFFFFF',
                                            textAlign: 'center',
                                            flex: 1
                                        }}>
                                        آماده ای؟
                                    </Text>
                                    <TouchableWithoutFeedback 
                                        onPress={() => {
                                        this.pageThreeTop.setValue(-400);
                                        this.setState(() => {
                                            return {onBoardingPage: -1}
                                        });
                                        this.postListRef.scrollToIndex({index: 0, viewPosition: 0});
                                        this.setState(() => {
                                            return {onBoardingPage: 3, currentTab: 0}
                                        });
                                        setTimeout(() => this.setState(() => {
                                            Animated.timing(
                                                this.pageThreeTop, {
                                                    toValue: 0,
                                                    duration: 400
                                                }
                                            ).start();
                                        }), 1000);
                                    }}>
                                        <View style={{
                                            borderRadius: 26, backgroundColor: '#FFFFFF', alignItems: 'center',
                                            justifyContent: 'center', marginTop: 15, width: 103, height: 52,
                                        }}>
                                            <Text style={{
                                                fontSize: MainActivity.normalize(20),
                                                fontFamily: 'IRANSansBold',
                                                color: '#4A4A4A'
                                            }}>
                                                بله
                                            </Text>
                                        </View>

                                    </TouchableWithoutFeedback>
                                    <View
                                        style={{
                                            flex: 1
                                        }}/>
                                </View>

                            </LinearGradient>
                            <View style={{flex: 1}}/>
                        </View>
                    </TouchableWithoutFeedback>
                );
            case 3:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View
                            style={{
                                position: 'absolute',
                                top: this.pageThreeTop,
                                bottom: 0,
                                right: 0,
                                left: 0
                            }}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.pageFourTop.setValue(-500);
                                this.setState(() => {
                                    return {onBoardingPage: 4}
                                });
                                setTimeout(() => this.setState(() => {
                                    Animated.timing(
                                        this.pageFourTop,
                                        {
                                            toValue: 0,
                                            duration: 500
                                        }
                                    ).start();
                                }), 1000);
                            }}>
                                <Animated.View
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        right: -85,
                                        left: -215,
                                        height: this.maxWidth + 300, marginTop: -400, opacity: 0.95,
                                        backgroundColor: "#4f2afb", borderRadius: this.maxWidth / 2 + 150
                                    }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>

                                <Animated.View
                                    style={{
                                        flexDirection: 'row',
                                        borderRadius: 41,
                                        width: '100%',
                                        paddingBottom: 5,
                                        marginLeft: 0,
                                        top: 80,
                                        padding: 5
                                    }}>

                                    <LottieView
                                        source={require("./components/animation_warning")}
                                        style={{height: 86, width: 86, zIndex: 2, marginLeft: 1, marginTop: -10}}
                                        autoPlay
                                        autoSize={false}
                                        resizeMode={'cover'}
                                        loop/>
                                    <View style={{
                                        backgroundColor: '#FFFFFF', position: 'absolute',
                                        borderRadius: 27, left: 37, top: 13, zIndex: 3
                                    }}>
                                        <Image source={require('./components/img/ic_add.png')}
                                               style={{width: 28, height: 28}}/>
                                    </View>
                                    <View style={{flex: 1}}/>
                                </Animated.View>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.pageFourTop.setValue(-500);
                                    this.setState(() => {
                                        return {onBoardingPage: 4}
                                    });
                                    setTimeout(() => this.setState(() => {
                                        Animated.timing(
                                            this.pageFourTop,
                                            {
                                                toValue: 0,
                                                duration: 500
                                            }
                                        ).start();
                                    }), 1000);
                                }}>
                                    <View
                                        style={{
                                            position: 'absolute',
                                            right: 0,
                                            top: 0,
                                            width: this.maxWidth / 2,
                                            paddingTop: 72,
                                            marginRight: 45
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: 'IRANSansBold',
                                                fontSize: MainActivity.normalize(18),
                                                color: '#FFFFFF'
                                            }}>
                                            اضافه کردن منبع
                                        </Text>
                                        <Text
                                            style={{
                                                fontFamily: 'IRANSansMobile',
                                                fontSize: MainActivity.normalize(16),
                                                color: '#FFFFFF'
                                            }}>
                                            اگه از کانال، سایت و
                                            یا پیجی خوشت اومد، اضافش کن
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
            case 4:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View
                            style={{
                                position: 'absolute',
                                top: this.pageFourTop,
                                bottom: 0,
                                right: 0,
                                left: 0
                            }}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.postListRef.scrollToIndex({index: 2, viewPosition: 1});
                                this.pageFiveBottom.setValue(-500);
                                this.setState(() => {
                                    return {onBoardingPage: 5}
                                });
                                setTimeout(() => this.setState(() => {
                                    Animated.timing(
                                        this.pageFiveBottom,
                                        {
                                            toValue: 0,
                                            duration: 400
                                        }
                                    ).start();
                                }), 400);
                            }}>
                                <View style={{
                                    position: 'absolute', top: 0, right: -205, left: -95,
                                    height: this.maxWidth + 300, marginTop: -400, opacity: 0.95,
                                    backgroundColor: "#F8C648", borderRadius: this.maxWidth / 2 + 150
                                }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View
                                    style={{
                                        flexDirection: 'row', borderRadius: 41, width: '100%',
                                        paddingBottom: 5, top: 72, padding: 5
                                    }}>

                                    <LottieView
                                        source={require("./components/animation_warning")}
                                        style={{height: 86, width: 86, zIndex: 2, marginLeft: -10, marginTop: -10}}
                                        autoPlay
                                        autoSize={false}
                                        resizeMode={'cover'}
                                        loop/>
                                    <View style={{
                                        backgroundColor: '#FFFFFF', position: 'absolute',
                                        borderRadius: 27, left: 15, top: 13, zIndex: 3
                                    }}>
                                        <Image source={require('./components/img/ic_del.png')}
                                               style={{width: 28, height: 28}}/>
                                    </View>
                                    <View style={{flex: 1}}/>
                                </View>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.postListRef.scrollToIndex({index: 2, viewPosition: 1});
                                    this.pageFiveBottom.setValue(-500);
                                    this.setState(() => {
                                        return {onBoardingPage: 5}
                                    });
                                    setTimeout(() => this.setState(() => {
                                        Animated.timing(
                                            this.pageFiveBottom,
                                            {
                                                toValue: 0,
                                                duration: 400
                                            }
                                        ).start();
                                    }), 400);
                                }}>
                                    <View
                                        style={{
                                            position: 'absolute',
                                            right: 0,
                                            top: 0,
                                            width: this.maxWidth / 2,
                                            paddingTop: 72,
                                            marginRight: 40
                                        }}>
                                        <Text style={{
                                            fontFamily: 'IRANSansBold',
                                            fontSize: MainActivity.normalize(18),
                                            color: '#FFFFFF'
                                        }}>حذف
                                            منبع

                                        </Text>
                                        <Text style={{
                                            fontFamily: 'IRANSansMobile',
                                            fontSize: MainActivity.normalize(16),
                                            color: '#FFFFFF'
                                        }}>
                                            اگه با کانال یا پیجی حال نکردی می‌تونی حذفش کنی!
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
            case 5:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View style={{
                            position: 'absolute',
                            top: 0,
                            bottom: this.pageFiveBottom,
                            right: 0,
                            left: 0
                        }}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.pageSixBottom.setValue(-500);
                                this.setState(() => {
                                    return {onBoardingPage: 6}
                                });
                                setTimeout(() => this.setState(() => {
                                    Animated.timing(
                                        this.pageSixBottom,
                                        {
                                            toValue: 0,
                                            duration: 400
                                        }
                                    ).start();
                                }), 700);
                            }}>
                                <View
                                    style={{
                                        position: 'absolute',
                                        right: -95,
                                        left: -205,
                                        height: this.maxWidth + 300,
                                        bottom: -400,
                                        opacity: 0.95,
                                        backgroundColor: "#47C0A5",
                                        borderRadius: this.maxWidth / 2 + 150
                                    }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flex: 1}}/>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View
                                    style={{
                                        position: 'absolute',
                                        right: 5,
                                        bottom: 20,
                                        width: this.maxWidth / 2 + 5,
                                        paddingBottom: 5,
                                        paddingRight: 25
                                    }}>
                                    <Text
                                        style={{
                                            fontFamily: 'IRANSansBold',
                                            fontSize: MainActivity.normalize(18),
                                            color: '#FFFFFF',
                                            marginRight: 10
                                        }}>
                                        حال کردی!
                                    </Text>
                                    <Text style={{
                                        fontFamily: 'IRANSansMobile',
                                        fontSize: MainActivity.normalize(16),
                                        color: '#FFFFFF'
                                    }}>
                                        اگه از یه محتوایی خوشت اومد، حتما به «من» بگو، تا علاقت رو بفهمه
                                    </Text>
                                </View>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.pageSixBottom.setValue(-500);
                                    this.setState(() => {
                                        return {onBoardingPage: 6}
                                    });
                                    setTimeout(() => this.setState(() => {
                                        Animated.timing(
                                            this.pageSixBottom, {
                                                toValue: 0,
                                                duration: 400
                                            }).start();
                                    }), 700);
                                }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            borderRadius: 41,
                                            width: '100%',
                                            paddingBottom: 5,
                                            bottom: 10,
                                            padding: 5
                                        }}>
                                        <LottieView
                                            source={require("./components/animation_warning")}
                                            style={{
                                                height: 126,
                                                width: 126,
                                                zIndex: 2,
                                                marginLeft: 23,
                                                marginTop: 2
                                            }}
                                            autoPlay
                                            autoSize={false}
                                            resizeMode={'cover'}
                                            loop/>
                                        <View style={{flex: 1}}/>
                                        <View
                                            style={{
                                                backgroundColor: '#FFFFFF',
                                                position: 'absolute',
                                                borderRadius: 25,
                                                left: 90,
                                                top: 45,
                                                zIndex: 3
                                            }}>
                                            <Image
                                                source={require('./components/img/ic_upvote_filled.png')}
                                                style={{
                                                    width: 50,
                                                    height: 50
                                                }}/>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
            case 6:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View
                            style={{
                                position: 'absolute',
                                top: 0,
                                bottom: this.pageSixBottom,
                                right: 0,
                                left: 0
                            }}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this.pageEightLeft.setValue(-this.maxWidth);
                                    this.pageEightRight.setValue(this.maxWidth);
                                    this.setState(() => {
                                        return {onBoardingPage: 8, currentTab: 3}
                                    });
                                    setTimeout(() => this.setState(() => {
                                        Animated.timing(
                                            this.pageEightLeft,
                                            {
                                                toValue: 0,
                                                duration: 400
                                            }
                                        ).start();
                                        Animated.timing(
                                            this.pageEightRight,
                                            {
                                                toValue: 0,
                                                duration: 400
                                            }
                                        ).start();
                                    }), 700);
                                }}>
                                <View style={{
                                    position: 'absolute', right: -95, left: -205,
                                    height: this.maxWidth + 300, bottom: -400, opacity: 0.95,
                                    backgroundColor: "#FF0020", borderRadius: this.maxWidth / 2 + 150
                                }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flex: 1}}/>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.pageEightLeft.setValue(-this.maxWidth);
                                    this.pageEightRight.setValue(this.maxWidth);
                                    this.setState(() => {
                                        return {onBoardingPage: 8, currentTab: 3}
                                    });
                                    setTimeout(() => this.setState(() => {
                                        Animated.timing(
                                            this.pageEightLeft,
                                            {
                                                toValue: 0,
                                                duration: 400
                                            }
                                        ).start();
                                        Animated.timing(
                                            this.pageEightRight,
                                            {
                                                toValue: 0,
                                                duration: 400
                                            }
                                        ).start();
                                    }), 700);
                                }}>
                                    <View style={{
                                        position: 'absolute', left: 0, bottom: 20, width: this.maxWidth / 2,
                                        paddingBottom: 5, paddingLeft: 25
                                    }}>
                                        <Text style={{
                                            fontFamily: 'IRANSansBold',
                                            fontSize: MainActivity.normalize(18),
                                            color: '#FFFFFF'
                                        }}>
                                            خوشت نیومد!
                                        </Text>
                                        <Text style={{
                                            fontFamily: 'IRANSansMobile',
                                            fontSize: MainActivity.normalize(16),
                                            color: '#FFFFFF'
                                        }}>
                                            اگه محتوایی رو دوست نداشتی، این دکمه رو بزن
                                            که دیگه ازش بهت نشون نده!
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <View style={{
                                    flexDirection: 'row', borderRadius: 41, width: '100%',
                                    paddingBottom: 5, bottom: 10, padding: 5
                                }}>
                                    <View style={{flex: 1}}/>
                                    <LottieView
                                        source={require("./components/animation_warning")}
                                        style={{height: 126, width: 126, zIndex: 2, marginRight: 49, marginTop: 1}}
                                        autoPlay
                                        autoSize={false}
                                        resizeMode={'cover'}
                                        loop/>
                                    <TouchableWithoutFeedback onPress={() => {
                                        this.pageEightLeft.setValue(-this.maxWidth);
                                        this.pageEightRight.setValue(this.maxWidth);
                                        this.setState(() => {
                                            return {onBoardingPage: 8, currentTab: 3}
                                        });
                                        setTimeout(() => this.setState(() => {
                                            Animated.timing(
                                                this.pageEightLeft,
                                                {
                                                    toValue: 0,
                                                    duration: 400
                                                }
                                            ).start();
                                            Animated.timing(
                                                this.pageEightRight,
                                                {
                                                    toValue: 0,
                                                    duration: 400
                                                }
                                            ).start();
                                        }), 700);
                                    }}>
                                        <View style={{
                                            backgroundColor: '#FFFFFF', position: 'absolute',
                                            borderRadius: 25, right: 90, top: 45, zIndex: 3
                                        }}>
                                            <Image source={require('./components/img/ic_downvote_filled.png')}
                                                   style={{width: 50, height: 50}}/>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
            case 7:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View
                            style={{
                                position: 'absolute',
                                top: 0,
                                bottom: 0,
                                right: this.pageSevenRight,
                                left: this.pageSevenLeft
                            }}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.setState(() => {
                                        return {onBoardingPage: 10}
                                    }
                                );
                            }}>
                                <View style={{
                                    position: 'absolute', right: -105, left: 25,
                                    height: this.maxWidth + 80, bottom: -50, opacity: 0.95,
                                    backgroundColor: "#600480", borderRadius: this.maxWidth / 2 + 40
                                }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flex: 1}}/>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View style={{
                                    position: 'absolute', right: 10, bottom: 150, width: this.maxWidth * 0.8,
                                    paddingBottom: 0, paddingRight: 15
                                }}>
                                    <Text style={{
                                        fontFamily: 'IRANSansBold',
                                        fontSize: MainActivity.normalize(18),
                                        color: '#FFFFFF'
                                    }}>
                                        کانال ها رو ویرایش کن!
                                        {'\n'}
                                        دسته بندی ها رو کم و زیاد کن!
                                    </Text>
                                    <Text style={{
                                        fontFamily: 'IRANSansMobile',
                                        fontSize: MainActivity.normalize(16),
                                        color: '#FFFFFF'
                                    }}>
                                        اگه قبلا کانال یا دسته‌بندی خاصی رو کم و زیاد کردی، میتونی دوباره اینجا ویرایش
                                        کنی!
                                    </Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row', borderRadius: 30, width: '100%',
                                    paddingBottom: 5, bottom: 0, padding: 0
                                }}>
                                    <View style={{flex: 1}}/>
                                    <LottieView
                                        source={require("./components/animation_warning")}
                                        style={{
                                            height: 72,
                                            width: 72,
                                            zIndex: 2,
                                            marginRight: 91,
                                            marginBottom: -14
                                        }}
                                        autoPlay
                                        autoSize={false}
                                        resizeMode={'cover'}
                                        loop/>
                                    <TouchableWithoutFeedback onPress={() => {
                                        this.setState(() => {
                                                return {onBoardingPage: 10}
                                            }
                                        );
                                    }}>
                                        <View style={{
                                            backgroundColor: '#FFFFFF',
                                            position: 'absolute',
                                            borderRadius: 25,
                                            right: 115,
                                            bottom: 15,
                                            zIndex: 3
                                        }}>
                                            <Image source={require('./img/ic_cross_filled.png')}
                                                   style={{width: 20, height: 20}}/>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
            case 8:
                return (
                    <TouchableWithoutFeedback>
                        <Animated.View
                            style={{
                                position: 'absolute',
                                top: 0,
                                bottom: 0,
                                right: this.pageEightRight,
                                left: this.pageEightLeft
                            }}>
                            <TouchableWithoutFeedback onPress={() => {
                                this.setState(() => {
                                    return {onBoardingPage: 9, currentTab: 0}
                                });
                                DBManager.setLaunchState("false");
                            }}>
                                <View style={{
                                    position: 'absolute', left: -70, right: -20,
                                    height: this.maxWidth + 90, bottom: -80, opacity: 0.95,
                                    backgroundColor: "#0AA01E", borderRadius: this.maxWidth / 2 + 45
                                }}/>
                            </TouchableWithoutFeedback>
                            <View style={{flex: 1}}/>
                            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                                <View style={{
                                    position: 'absolute', left: 10, bottom: 70, width: this.maxWidth * 0.8,
                                    paddingBottom: 0, paddingLeft: 15
                                }}>
                                    <Text style={{
                                        fontFamily: 'IRANSansBold',
                                        fontSize: MainActivity.normalize(18),
                                        color: '#FFFFFF'
                                    }}>
                                        از وضعیت رباتت باخبر شو
                                    </Text>
                                    <Text style={{
                                        fontFamily: 'IRANSansMobile',
                                        fontSize: MainActivity.normalize(16),
                                        color: '#FFFFFF'
                                    }}>
                                        ربات هوش مصنوعی تو الان کودکه و خیلی کم تو رو میشناسه!
                                        تو این صفحه از وضعیتش باخبر شو و با انجام کارهای گفته شده اونو ارتقا بده
                                    </Text>
                                </View>
                                <TouchableWithoutFeedback onPress={() => {
                                    this.setState(() => {
                                        return {onBoardingPage: 9, currentTab: 0}
                                    });
                                    DBManager.setLaunchState("false");
                                }}>
                                    <View style={{
                                        flexDirection: 'row', borderRadius: 30, width: '100%',
                                        paddingBottom: 0, bottom: 0, padding: 0
                                    }}>
                                        <View style={{flex: 1}}/>
                                        <LottieView
                                            source={require("./components/animation_warning")}
                                            style={{
                                                height: 72,
                                                width: 72,
                                                zIndex: 2,
                                                marginRight: 16,
                                                marginBottom: -12
                                            }}
                                            autoPlay
                                            autoSize={false}
                                            resizeMode={'cover'}
                                            loop/>

                                        <View style={{
                                            backgroundColor: '#FFFFFF', position: 'absolute',
                                            borderRadius: 10, right: 40, bottom: 15, zIndex: 3
                                        }}>
                                            <Image source={require('./components/img/mr_robot_one.png')}
                                                   style={{width: 20, height: 20}}/>
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </Animated.View>
                    </TouchableWithoutFeedback>
                );
        }
    }

    _rotateStar() {
        this.starRotation.setValue(0);
        Animated.timing(
            this.starRotation, {
                toValue: 1, duration: 1500
            }
        ).start(() => this._rotateStar());
    }

    _showStar() {
        this.starTopMargin.setValue(0);
        this.starLeftMargin.setValue(0);
        this.state.scaleRobotTab.setValue(0);
        this.setState({showStar: true});
        let height = Dimensions.get('window').height;
        this._rotateStar();

        let Sound = require('react-native-sound');
        Sound.setCategory('Playback');
        let err = false;
        var whoosh = new Sound('wand.mp3', Sound.MAIN_BUNDLE, (error) => {
            if (error) {
                err = true;
            } else {
                whoosh.play();
            }
        });


        Animated.sequence(
            [
                Animated.timing(this.starScale, {
                    toValue: 2, duration: 800
                })
            ]
        ).start(() => {
            Animated.timing(this.starScale, {
                toValue: 0.01, duration: 800
            }).start();
            Animated.timing(this.starLeftMargin, {
                toValue: this.maxWidth - 50, duration: 800
            }).start();
            Animated.timing(this.state.scaleRobotTab, {
                toValue: 1, duration: 800
            }).start();
            Animated.timing(this.starTopMargin, {
                toValue: height - 30, duration: 800
            }).start(() => {
                this.setState({showStar: false});
                RequestsController.onBoardingStatus(MainActivity.token, MainActivity.userLevel + 1, MainActivity.currentLevelStars[0],
                    MainActivity.currentLevelStars[1], MainActivity.currentLevelStars[2],
                    MainActivity.currentLevelStars[3], MainActivity.currentLevelStars[4]);
                setTimeout(() => this._showRobot(), 2500);
            });
        });
    }

    _showRobot() {
        if (MainActivity.currentLevelStars[0] && MainActivity.currentLevelStars[1] && MainActivity.currentLevelStars[2]
            && MainActivity.currentLevelStars[3] && MainActivity.currentLevelStars[4]) {
            MainActivity.currentLevelStars = [false, false, false, false, false];
            DBManager.setUserLevel('star0', false);
            DBManager.setUserLevel('star1', false);
            DBManager.setUserLevel('star2', false);
            DBManager.setUserLevel('star3', false);
            DBManager.setUserLevel('star4', false);

            MainActivity.userLevel++;
            DBManager.saveSettingValue('userLevel', MainActivity.userLevel);
            MainActivity.totalChannels = 0;
            DBManager.setUserLevel('totalChannels', 0);
            MainActivity.totalSaveds = 0;
            DBManager.setUserLevel('totalSaveds', 0);
            MainActivity.totalImpression = 0;
            DBManager.setUserLevel('totalImpression', 0);
            MainActivity.totalReported = 0;
            DBManager.setUserLevel('totalReported', 0);
            MainActivity.totalLikeds = 0;
            DBManager.setUserLevel('totalLikeds', 0);
            MainActivity.totalDislikeds = 0;
            DBManager.setUserLevel('totalDislikeds', 0);
            MainActivity.consecutiveDays = 0;
            DBManager.setUserLevel('consecutiveDays', 0);
            DBManager.setUserLevel('userLevel', MainActivity.userLevel);

            RequestsController.onBoardingStatus(MainActivity.token, MainActivity.userLevel, false, false, false, false, false);
            this.starTopMargin.setValue(0);
            this.starLeftMargin.setValue(0);
            this.state.scaleRobotTab.setValue(0);
            this.setState({showRobot: true});
            let height = Dimensions.get('window').height;
            Animated.sequence(
                [
                    Animated.timing(this.starScale, {
                        toValue: 2, duration: 800
                    })
                ]
            ).start(() => {
                Animated.timing(this.starScale, {
                    toValue: 0.01, duration: 800
                }).start();
                Animated.timing(this.starLeftMargin, {
                    toValue: this.maxWidth - 50, duration: 800
                }).start();
                Animated.timing(this.state.scaleRobotTab, {
                    toValue: 1, duration: 800
                }).start();
                Animated.timing(this.starTopMargin, {
                    toValue: height - 30, duration: 800
                }).start();
            }, () => this.setState({currentTab: 3}));
        }
    }

    categoryChanged(iD) {
        this.state.tags.forEach((item) => {
            if (item.id === iD) {
                item.state = 1 - item.state;
                this.setState((prev) => ({tags: prev.tags}));
                return;
            }
        });
    }

    checkUserStats(stat, flag) {
        switch (stat) {
            case 0: //like
                MainActivity.totalLikeds += flag ? -1 : +1;
                //console.log('totalLikeds is ', MainActivity.totalLikeds);
                DBManager.setUserLevel('totalLikeds', MainActivity.totalLikeds);
                if (MainActivity.userLevel === 0 && !MainActivity.currentLevelStars[2] && MainActivity.totalLikeds === 5) {
                    MainActivity.currentLevelStars[2] = true;
                    DBManager.setUserLevel('star2', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[2] && MainActivity.totalLikeds === 50) {
                    MainActivity.currentLevelStars[2] = true;
                    DBManager.setUserLevel('star2', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 2 && !MainActivity.currentLevelStars[2] && MainActivity.totalLikeds === 100) {
                    MainActivity.currentLevelStars[2] = true;
                    DBManager.setUserLevel('star2', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 3 && !MainActivity.currentLevelStars[2] && MainActivity.totalLikeds === 200) {
                    MainActivity.currentLevelStars[2] = true;
                    DBManager.setUserLevel('star2', true);
                    this._showStar();
                }
                break;
            case 1: //dislike
                MainActivity.totalDislikeds += flag ? -1 : +1;
                DBManager.setUserLevel('totalDislikeds', MainActivity.totalDislikeds);
                // console.log('currentLevelStars is ', !MainActivity.currentLevelStars[3]);
                // console.log('currentLevelStars is ', MainActivity.userLevel);
                // console.log('currentLevelStars is ', MainActivity.totalDislikeds === 5);
                if (MainActivity.userLevel === 0 && !MainActivity.currentLevelStars[3] && MainActivity.totalDislikeds === 5) {
                    MainActivity.currentLevelStars[3] = true;
                    DBManager.setUserLevel('star3', true);
                    setTimeout(() => this._showStar(), 1000);
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[3] && MainActivity.totalDislikeds === 50) {
                    MainActivity.currentLevelStars[3] = true;
                    DBManager.setUserLevel('star3', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 2 && !MainActivity.currentLevelStars[3] && MainActivity.totalDislikeds === 100) {
                    MainActivity.currentLevelStars[3] = true;
                    DBManager.setUserLevel('star3', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 3 && !MainActivity.currentLevelStars[3] && MainActivity.totalDislikeds === 200) {
                    MainActivity.currentLevelStars[3] = true;
                    DBManager.setUserLevel('star3', true);
                    this._showStar();
                }
                break;
            case 2: //save share
                MainActivity.totalSaveds += flag ? -1 : +1;
                DBManager.setUserLevel('totalSaveds', MainActivity.totalSaveds);
                if (MainActivity.userLevel === 0) {
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[0] && MainActivity.totalSaveds === 10) {
                    MainActivity.currentLevelStars[0] = true;
                    DBManager.setUserLevel('star0', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 2 && !MainActivity.currentLevelStars[0] && MainActivity.totalSaveds === 50) {
                    MainActivity.currentLevelStars[0] = true;
                    DBManager.setUserLevel('star0', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[0] && MainActivity.totalSaveds === 100) {
                    MainActivity.currentLevelStars[0] = true;
                    DBManager.setUserLevel('star0', true);
                    this._showStar();
                }
                break;
            case 3: //report
                if (MainActivity.userLevel === 3 && !MainActivity.currentLevelStars[4] && MainActivity.totalReported === 100) {
                    MainActivity.currentLevelStars[0] = true;
                    DBManager.setUserLevel('star4', true);
                    this._showStar();
                }
                break;
            case 4: //channel
                MainActivity.totalChannels += flag ? +1 : -1;
                DBManager.setUserLevel('totalChannels', MainActivity.totalChannels);
                if (MainActivity.userLevel === 0 && !MainActivity.currentLevelStars[1] && MainActivity.totalChannels === 5) {
                    MainActivity.currentLevelStars[1] = true;
                    DBManager.setUserLevel('star1', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[1] && MainActivity.totalChannels === 10) {
                    MainActivity.currentLevelStars[1] = true;
                    DBManager.setUserLevel('star1', true);
                    this._showStar();
                }
                break;
            case 5: //impression
                DBManager.setUserLevel('totalImpression', MainActivity.totalImpression);
                //console.log('totalImpression', MainActivity.totalImpression);
                if (MainActivity.userLevel === 0 && !MainActivity.currentLevelStars[4] && MainActivity.totalImpression === 50) {
                    MainActivity.currentLevelStars[4] = true;
                    DBManager.setUserLevel('star4', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 1 && !MainActivity.currentLevelStars[4] && MainActivity.totalImpression === 300) {
                    MainActivity.currentLevelStars[4] = true;
                    DBManager.setUserLevel('star4', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 2 && !MainActivity.currentLevelStars[4] && MainActivity.totalImpression === 1000) {
                    MainActivity.currentLevelStars[4] = true;
                    DBManager.setUserLevel('star4', true);
                    this._showStar();
                }
                break;
            case 6: //day in app
                if (MainActivity.userLevel === 2 && !MainActivity.currentLevelStars[1] && MainActivity.consecutiveDays === 10) {
                    MainActivity.currentLevelStars[1] = true;
                    DBManager.setUserLevel('star1', true);
                    this._showStar();
                } else if (MainActivity.userLevel === 3 && !MainActivity.currentLevelStars[1] && MainActivity.consecutiveDays === 300) {
                    MainActivity.currentLevelStars[1] = true;
                    DBManager.setUserLevel('star1', true);
                    this._showStar();
                }
                break;
        }
    }

    async getToken() {
        MainActivity.tracker.dispatchWithTimeout(60);
        let fcm = await DBManager.getSettingValue('fcm_token');
        if (fcm !== null && fcm !== "null")
            MainActivity.token = await RequestsController.loadToken(fcm);
        else
            MainActivity.token = await RequestsController.loadToken("");
        this.first_run = await DBManager.getLaunchState();
        this.channelListClickedYet = await DBManager.getSettingValue('clicked_yet');
        if (this.channelListClickedYet === "null" || this.channelListClickedYet === null)
            this.channelListClickedYet = false;
        if (this.first_run === "true") {
            //if ("true" === "true") {
            this.setState({onBoardingPage: 0});
            console.log("hiding");
            SplashScreen.hide();
        } else {
            //console.log('Main Props ', App.getPostID());
            if (App.getPostID() !== undefined && App.getPostID() !== null) {
                this.props.navigation.navigate('SingleItem', {
                    id: App.getPostID(),
                    token: MainActivity.token
                });
            }
            await this.loadTags();

            console.log("hiding");
            SplashScreen.hide();
            await this.loadPosts();
            await this.loadVideos();
        }
        MainActivity.userLevel = await DBManager.getSettingValue('userLevel');
        if (MainActivity.userLevel === null)
            MainActivity.userLevel = 0;
        else
            MainActivity.userLevel = parseInt(MainActivity.userLevel);
        this.setState({level: MainActivity.userLevel});

        this.timeOnTab = new Date().getTime();
        MainActivity.tracker.trackScreenView("Trends");
        MainActivity.tracker.trackEvent("Session", "Started", {label: "Started", value: "1"});
        MainActivity.tracker.trackEvent("BottomNavigation", "Click", {label: "trends", value: "1"});

        let country = await RequestsController.getIPInfo();
        if (country !== 'Iran') {
            this.state.data = [...[{post_type: 6, post_id: -1}], ...this.state.data];
        }

        const enabled = await firebase.messaging().hasPermission();
        // if (enabled) {
        //     console.log('has permission');
        // } else {
        //     console.log('does not have permission');
        // }

        let d = new Date();
        let n = d.getDate();
        let o = d.getMonth();
        let date = await DBManager.getSettingValue('lastDay');
        let month = await DBManager.getSettingValue('lastMonth');
        if (date === null) {
            DBManager.saveSettingValue('lastDay', d.getDate());
            DBManager.saveSettingValue('lastMonth', d.getMonth());
            DBManager.saveSettingValue('consecutiveDays', 0);
            MainActivity.setconsecutiveDays(0);
        } else {
            if (n.toString() === date && o.toString() === month) {
            } //earlier today
            else if (o.toString() === month && (parseInt(date) + 1) === n) {
                DBManager.saveSettingValue('lastDay', d.getDate());
                DBManager.saveSettingValue('lastMonth', d.getMonth());
                DBManager.saveSettingValue('consecutiveDays', MainActivity.consecutiveDays + 1);
                MainActivity.setconsecutiveDays(MainActivity.consecutiveDays + 1);
            } else if (o === (parseInt(month) + 1) && n === 1 && (date === '30' || date === '29' || date === '28')) {
                DBManager.saveSettingValue('lastDay', d.getDate());
                DBManager.saveSettingValue('lastMonth', d.getMonth());
                DBManager.saveSettingValue('consecutiveDays', MainActivity.consecutiveDays + 1);
                MainActivity.setconsecutiveDays(MainActivity.consecutiveDays + 1);
            } else {
                DBManager.saveSettingValue('lastDay', d.getDate());
                DBManager.saveSettingValue('lastMonth', d.getMonth());
                DBManager.saveSettingValue('consecutiveDays', 0);
                MainActivity.setconsecutiveDays(0);
            }
        }
        await DBManager.getUserLevel();
        RequestsController.onBoardingStatus(MainActivity.token, MainActivity.userLevel + 1, MainActivity.currentLevelStars[0],
            MainActivity.currentLevelStars[1], MainActivity.currentLevelStars[2],
            MainActivity.currentLevelStars[3], MainActivity.currentLevelStars[4]);
    }

    hideVPN() {
        this.state.data.splice(0, 1);
        this.setState({
            data: this.state.data
        });
        //react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets
        // -dest android/app/src/main/res
    }

    static normalize(size) {
        let number = PixelRatio.get();
        if (number >= 2.5) {
            return size;
        } else if (number > 1.5)
            return size - 1;
        else
            return size - 2;
    }

    async loadPosts(refresh = false) {
        this.setState(() => {
            return {
                isLoading: true
            }
        });

        this.state.data.forEach(async item => {
            if (this.trendRef.get(item.post_id) !== null) {
                if (this.impression.has(item.post_id)) {
                    await RequestsController.sendImpression(MainActivity.token, item.post_id,
                        Math.floor(((new Date().getTime()) - this.impression.get(item.post_id)) / 400));
                    this.impression.delete(item.post_id);
                    if (this.trendRef.get(item.post_id).getImpressionSent !== undefined &&
                        this.trendRef.get(item.post_id).getImpressionSent !== null &&
                        this.trendRef.get(item.post_id).getImpressionSent()) {
                        MainActivity.totalImpression++;
                        this.checkUserStats(5, null);
                    }
                } else {
                    if (!refresh)
                        await RequestsController.sendImpression(MainActivity.token, item.post_id, 0);
                }
                if (this.trendRef.get(item.post_id).itemLeftOut !== undefined &&
                    this.trendRef.get(item.post_id).itemLeftOut !== null)
                    this.trendRef.get(item.post_id).itemLeftOut();
            }
        });
        this.trendRef = new Map();
        this.impression = new Map();

        if (this.currentTag === undefined) {//Normal Posts
            if (this.lastTag !== undefined || refresh) {//last posts are useless, must init
                this.postLoaded = 0;
                refresh = true;
                this.lastTag = undefined;
                this.setState({currentItem: undefined});
            }
            let d = await
                RequestsController.getPosts(MainActivity.token, 10, this.postLoaded, false, MainActivity.sID);

            if (d === null) {
                if (refresh)
                    this.setState(() => {
                        return {isLoading: false, data: []};
                    });
                else
                    this.setState(() => {
                        return {isLoading: false};
                    });
                return;
            }
            this.postLoaded += 10;
            if (d !== undefined) {
                if (!refresh)
                    this.setState(() => {
                        return {data: this.state.data.concat(d), isLoading: false};
                    });
                else {
                    this.setState(() => {
                        return {data: d, isLoading: false};
                    }, () => this.postListRef.scrollToIndex({index: 0, viewPosition: 0}));
                }
            } else {
                if (refresh)
                    this.setState(() => {
                        return {isLoading: false, data: []};
                    });
                else
                    this.setState(() => {
                        return {isLoading: false};
                    });
            }
        } else {//From Tag
            if (this.currentTag === this.lastTag) {//same tag
                if (refresh)
                    this.postLoaded = 0;
                let d = await
                    RequestsController.getPostsFromCategory(MainActivity.token, this.currentTag.id, 10, this.postLoaded, MainActivity.sID);
                if (d === null) {
                    if (refresh)
                        this.setState(() => {
                            return {isLoading: false, data: []};
                        });
                    else
                        this.setState(() => {
                            return {isLoading: false};
                        });
                    return;
                }
                this.postLoaded += 10;

                if (d !== undefined)
                    if (!refresh)
                        this.setState(() => {
                            return {data: [...this.state.data, ...d], isLoading: false};
                        });
                    else {
                        this.trendRef = new Map();
                        this.impression = new Map();
                        this.setState(() => {
                            return {data: d, isLoading: false};
                        }, () => this.postListRef.scrollToIndex({index: 0, viewPosition: 0}));
                    }
                else {
                    if (refresh)
                        this.setState(() => {
                            return {isLoading: false, data: []};
                        });
                    else
                        this.setState(() => {
                            return {isLoading: false};
                        });
                }
            } else {//new tag, we can not concat
                this.currentItemRightMargin.setValue(-100);
                this.setState(() => {
                    return {currentItem: this.currentTag};
                }, () => {
                    Animated.timing(this.currentItemRightMargin, {
                        toValue: 0,
                        duration: 500
                    }).start();
                });
                this.postLoaded = 0;
                this.lastTag = this.currentTag;
                let d = await
                    RequestsController.getPostsFromCategory(MainActivity.token, this.currentTag.id, 10, this.postLoaded, MainActivity.sID);
                this.postLoaded += 10;
                if (d !== undefined && d !== null && d.length !== 0)
                    this.setState(() => {
                        return {currentItem: this.currentTag, data: d, isLoading: false};
                    }, () => this.postListRef.scrollToIndex({index: 0, viewPosition: 0}));
                else {
                    this.setState(() => {
                        return {currentItem: this.currentTag, isLoading: false, data: []};
                    });
                }
            }
        }
    }

    async loadVideos(refresh = false) {
        this.setState(() => {
            return {
                isLoading: true
            }
        });
        if (refresh) this.videosLoaded = 0;
        let d = await
            RequestsController.getPosts(MainActivity.token, 10, this.videosLoaded, true, MainActivity.sID);
        if (d === null) {
            this.setState(() => {
                return {isLoading: false, videos: []};
            });
            return;
        }
        this.videosLoaded += 10;
        if (d !== undefined)
            if (!refresh)
                this.setState(() => {
                    return {videos: this.state.videos.concat(d), isLoading: false};
                });
            else
                this.setState(() => {
                    return {videos: d, isLoading: false};
                }, () => this.postListRef.scrollToIndex({index: 0, viewPosition: 0}));
        else
            this.setState(() => {
                return {isLoading: false, videos: []};
            });
    }

    async loadTags() {
        let t = await
            RequestsController.getCategories(MainActivity.token);
        let items = [];
        for (let index in t) {
            if (t[index].name === 'شخصی') {
                this.state.for_you_icon = t[index].thumbnail.xhdpi;
                continue;
            }
            let item = [{'item': t[index]}, {'selected': t[index].state !== 0}];
            items.push(item);
            if (index % 3 === 2) {
                this.state.categories.push(items);
                items = [];
            }
        }
        if (items.length > 0)
            this.state.categories.push(items);// = [...this.state.categories, ...items];
        this.setState(() => {
            return {tags: t, categories: this.state.categories};
        });
    }

    viewItemsChanged(props) {
        if (props.changed !== undefined)
            props.changed.forEach(async item => {
                if (this.trendRef.get(item.item.post_id) !== null && this.trendRef.get(item.item.post_id) !== undefined) {
                    //console.log('item ', item);
                    if (this.impression.has(item.item.post_id)) {
                        await RequestsController.sendImpression(MainActivity.token, item.item.post_id,
                            Math.floor(((new Date().getTime()) - this.impression.get(item.item.post_id)) / 400));
                        this.impression.delete(item.item.post_id);
                        if (this.trendRef.get(item.item.post_id).getImpressionSent !== undefined &&
                            this.trendRef.get(item.item.post_id).getImpressionSent !== null &&
                            this.trendRef.get(item.item.post_id).getImpressionSent()) {
                            MainActivity.totalImpression++;
                            this.checkUserStats(5, null);
                        }
                    } else {
                        this.impression.set(item.item.post_id, new Date().getTime())
                    }
                    if (this.trendRef.get(item.item.post_id).itemLeftOut !== undefined &&
                        this.trendRef.get(item.item.post_id).itemLeftOut !== null)
                        this.trendRef.get(item.item.post_id).itemLeftOut();
                }
            });
    }

    viewConfig = {viewAreaCoveragePercentThreshold: 10};

    componentDidMount() {
        Orientation.lockToPortrait();
        this.maxWidth = Dimensions.get('window').width;
        AppState.addEventListener('change', this._handleAppStateChange);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        this.messageListener = firebase.messaging().onMessage((message) => {
            const notification = new firebase.notifications.Notification()
                .setNotificationId('notificationId')
                .setTitle('My notification title')
                .setBody('My notification body')
                .android.setChannelId('channelId')
                .android.setSmallIcon('logo')
                .setData({
                    key1: 'value1',
                    key2: 'value2',
                });
            firebase.notifications().displayNotification(notification);
        });
        firebase.messaging().getToken().then(
            async fcm_token => {
                console.log('fcm_token ', fcm_token);
                let fcm = DBManager.getSettingValue('fcm_token');
                firebase.messaging().subscribeToTopic('all');
                if (fcm !== fcm_token) {
                    DBManager.saveSettingValue('fcm_token', fcm_token);
                    MainActivity.token = await RequestsController.loadToken(fcm_token);
                }
            }
        ).catch((e) => console.log('error ', e));
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
        this.messageListener();
    }

    handleBackPress = () => {
        if (this.state.maskShow) {
            this.reportChannel(null, null);
            return true;
        }
        //ToDo Check if we are in activity
        return false;
    };

    _handleAppStateChange = (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
            //console.log('App has come to the foreground!')
            if (new Date().getTime() - this.timwWhenGoBack > 60 * 400) {
                MainActivity.tracker.trackEvent("Session", "Started", {
                    label: "Started",
                    value: "1"
                }, {session: "start"});
            }
        }
        if (nextAppState.match(/inactive|background/)) {
            //console.log('App has come to the foreground!');
            MainActivity.sID = uuid.v1();
            MainActivity.tracker.trackEvent("Session", "Stopped", {label: "Stopped", value: "1"});
            this.timwWhenGoBack = new Date().getTime();
        }
        this.setState({appState: nextAppState});
    };


    tagClicked(item) {
        this.currentTag = item;
        if (item !== undefined)
            this.tagListRef.scrollToIndex({index: this.state.tags.length - 1, viewPosition: 1});
        this.loadPosts();
    }

    goFullScreen(media_url, type, time = 0, post_id) {
        this.props.navigation.navigate('FullScreen', {
            media_url: media_url, isVideo: type === 3, time: time,
            token: MainActivity.token, post_id: post_id
        });
    }

    goSingleItem(post_id) {
        this.props.navigation.navigate('SingleItem', {
            id: post_id,
            token: MainActivity.token
        });
    }

    showToast(msg, dur) {
        this.refs.toast.show(msg, dur);
    }

    static setTotalDislikeds(val) {
        MainActivity.totalDislikeds = val;
    }

    static settotalLikeds(val) {
        MainActivity.totalLikeds = val;
    }

    static settotalReported(val) {
        MainActivity.totalReported = val;
    }

    static settotalSaveds(val) {
        MainActivity.totalSaveds = val;
    }

    static settotalImpression(val) {
        MainActivity.totalImpression = val;
    }

    static settotalChannels(val) {
        MainActivity.totalChannels = val;
    }

    static setconsecutiveDays(val) {
        MainActivity.consecutiveDays = val;
    }

    static getUserLevel() {
        return MainActivity.userLevel;
    }

    static getToken() {
        return MainActivity.token;
    }

    static getstar1() {
        if (MainActivity.currentLevelStars[0])
            return 0.0;
        else if (MainActivity.userLevel === 0) {
            return 1.0;
        } else if (MainActivity.userLevel === 1) {
            return MainActivity.totalSaveds / 10;
        } else if (MainActivity.userLevel === 2) {
            return MainActivity.totalSaveds / 50;
        } else {
            return MainActivity.totalSaveds / 100;
        }
    }

    static getstar2() {
        if (MainActivity.currentLevelStars[1])
            return 0.0;
        else if (MainActivity.userLevel === 0) {
            return MainActivity.totalChannels / 5;
        } else if (MainActivity.userLevel === 1) {
            return MainActivity.totalChannels / 10;
        } else if (MainActivity.userLevel === 2) {
            return MainActivity.consecutiveDays / 10;
        } else {
            return MainActivity.consecutiveDays / 20;
        }
    }

    static getstar3() {
        if (MainActivity.currentLevelStars[2])
            return 0.0;
        else if (MainActivity.userLevel === 0) {
            return MainActivity.totalLikeds / 5;
        } else if (MainActivity.userLevel === 1) {
            return MainActivity.totalLikeds / 50;
        } else if (MainActivity.userLevel === 2) {
            return MainActivity.totalLikeds / 100;
        } else {
            return MainActivity.totalLikeds / 200;
        }
    }

    static getstar4() {
        if (MainActivity.currentLevelStars[3])
            return 0.0;
        else if (MainActivity.userLevel === 0) {
            return MainActivity.totalDislikeds / 5;
        } else if (MainActivity.userLevel === 1) {
            return MainActivity.totalDislikeds / 50;
        } else if (MainActivity.userLevel === 2) {
            return MainActivity.totalDislikeds / 100;
        } else {
            return MainActivity.totalDislikeds / 200;
        }
    }

    static getstar5() {
        if (MainActivity.currentLevelStars[4])
            return 0.0;
        else if (MainActivity.userLevel === 0) {
            return MainActivity.totalImpression / 50;
        } else if (MainActivity.userLevel === 1) {
            return MainActivity.totalImpression / 300;
        } else if (MainActivity.userLevel === 2) {
            return MainActivity.totalImpression / 1000;
        } else {
            return MainActivity.totalReported / 50;
        }
    }

    removeItem(postID) {
        let data = [...this.state.data];
        for (let index in data) {
            if (data[index].post_id === postID) {
                data.splice(index, 1);
                break;
            }
        }
        this.setState({data: data});
    }

    static sendEvent(category, action, label) {
        MainActivity.tracker.trackEvent(category, action, {label: label, value: "1"});
    }

    reportChannel(post_id) {
        this.reportPostId = post_id;
        this.setState(() => {
            return {maskShow: !this.state.maskShow};
        });
        if (post_id === null) {
            this.state.marginBottomSecond.setValue(-700);
            this.state.marginBottomAnimated.setValue(-500);
            return
        }
        this.state.maskOp.setValue(0.0);
        Animated.timing(
            this.state.maskOp, {
                toValue: 0.75,
                duration: 400,
                delay: 100
            }
        ).start();
        this.state.marginBottomSecond.setValue(-700);
        Animated.timing(
            this.state.marginBottomSecond, {
                toValue: 0,
                duration: 400,
                delay: 600
            }).start();
    }

    render() {
        // if (this.first_run === undefined)
        //     return null;
        let Tabs = require('react-native-scrollable-tab-view');
        let tagView = this.state.currentItem === undefined ?
            <View>
                <Image
                    style={{
                        borderColor: "#50E3C2",
                        borderWidth: 2,
                        borderRadius: 8,
                        height: 55,
                        width: 82,
                        marginStart: 5,
                        marginEnd: 5,
                        justifyContent: 'flex-end',
                        marginTop: 15,
                        marginBottom: 12
                    }}
                    source={{uri: this.state.for_you_icon}}/>

                <Text
                    style={{
                        position: 'absolute',
                        textAlign: 'center',
                        color: "#ffffff",
                        left: 0,
                        right: 0,
                        bottom: 10,
                        paddingStart: 12,
                        paddingEnd: 12,
                        fontSize: 10,
                        fontFamily: 'IRANSansMobile'
                    }}>
                    شخصی
                </Text>
            </View>
            :
            <View
                style={{flexDirection: 'row'}}>
                <Animated.View
                    style={{
                        marginRight: this.currentItemRightMargin
                    }}>
                    <View
                        style={{
                            borderColor: "#50E3C2",
                            borderWidth: 2,
                            borderRadius: 8,
                            marginStart: 5,
                            marginEnd: 5,
                            marginTop: 13,
                            marginBottom: 10
                        }}>
                        <ImageBackground
                            style={{
                                height: 55,
                                width: 82,
                                borderRadius: 6,
                                justifyContent: 'flex-end'
                            }}
                            source={{uri: this.state.currentItem.thumbnail.xhdpi}}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: "#50E3C2",
                                    alignItems: 'center',
                                    paddingStart: 12,
                                    paddingEnd: 12,
                                    fontFamily: 'IRANSansMobile',
                                    fontSize: 10
                                }}>
                                {this.state.currentItem.name}
                            </Text>
                        </ImageBackground>
                    </View>
                </Animated.View>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.tagClicked(undefined);
                    }}>
                    <View>
                        <Grayscale>
                            <Image
                                style={{
                                    height: 55,
                                    width: 82,
                                    borderRadius: 6,
                                    marginStart: 5,
                                    marginEnd: 5,
                                    justifyContent: 'flex-end',
                                    marginTop: 15,
                                    marginBottom: 10
                                }}
                                source={{uri: this.state.for_you_icon}}/>
                        </Grayscale>
                        <Text
                            style={{
                                position: 'absolute',
                                textAlign: 'center',
                                color: "#ffffff",
                                left: 0,
                                right: 0,
                                bottom: 20,
                                paddingStart: 12,
                                paddingEnd: 12,
                                fontSize: 10,
                                fontFamily: 'IRANSansMobile'
                            }}>
                            شخصی
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>;
        let anim = this.state.isLoading ?
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <LottieView
                    source={require("./components/animation_loading")}
                    style={{alignSelf: 'center', height: 50, position: 'absolute'}}
                    autoPlay
                    resizeMode={"contain"}
                    loop/>
            </View> : null;
        let star = this.state.showStar ?
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Animated.View
                    style={{
                        transform: [
                            {rotate: this.starRotationScale},
                            {scale: this.starScale}],
                        marginTop: this.starTopMargin,
                        marginLeft: this.starLeftMargin
                    }}>
                    <Image
                        source={require("./components/img/ic_star.png")}
                        style={{height: 70, aspectRatio: 1}}
                        resizeMode={"stretch"}/>
                </Animated.View>
            </View> : null;
        let robot = this.state.showRobot ?
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Animated.View
                    style={{
                        transform: [
                            {scale: this.starScale}],
                        marginTop: this.starTopMargin,
                        marginLeft: this.starLeftMargin
                    }}>
                    <Image
                        source={this.robotIcons[this.state.level]}
                        style={{height: 70, aspectRatio: 1.18}}
                        resizeMode={"stretch"}/>
                </Animated.View>
            </View> : null;
        let mask = !this.state.maskShow ? null :
            <TouchableWithoutFeedback onPress={() => this.reportChannel(null, null)}>
                <View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0
                }}>
                    <Animated.View style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: "#000000",
                        opacity: this.state.maskOp
                    }}/>
                    <View style={{flex: 1}}/>
                    <View>
                        <Animated.View
                            style={{
                                bottom: this.state.marginBottomSecond,
                                right: 0,
                                left: 0,
                                position: 'absolute'
                            }}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this.reportChannel(null, null);
                                }}>
                                <View
                                    style={{
                                        backgroundColor: "#FFFFFF",
                                        opacity: 1
                                    }}>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center'
                                        }}>
                                        <Image
                                            source={require("./components/img/ic_back.png")}
                                            style={{
                                                alignSelf: 'flex-start',
                                                height: 22,
                                                width: 22,
                                                resizeMode: 'contain',
                                                marginTop: 15,
                                                marginLeft: 25
                                            }}/>
                                        <View style={{flex: 1}}/>
                                        <Text style={{
                                            fontSize: MainActivity.normalize(18),
                                            color: "#000000", textAlign: 'right',
                                            marginRight: 20,
                                            padding: 10,
                                            fontFamily: 'IRANSansMobile'
                                        }}>
                                            دلیل گزارش</Text>
                                    </View>
                                    <View style={{height: 5, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    RequestsController.reportContent(MainActivity.token, this.reportPostId, 1);
                                    this.reportChannel(null, null);
                                }}>
                                <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                    <Text style={{
                                        fontSize: MainActivity.normalize(18),
                                        color: "#000000", textAlign: 'right',
                                        marginRight: 20,
                                        padding: 10,
                                        fontFamily: 'IRANSansMobile'
                                    }}>محتوای
                                        غیراخلاقی</Text>
                                    <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    RequestsController.reportContent(MainActivity.token, this.reportPostId, 2)
                                }}>
                                <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                    <Text style={{
                                        fontSize: MainActivity.normalize(18),
                                        color: "#000000", textAlign: 'right',
                                        marginRight: 20, fontFamily: 'IRANSansMobile',
                                        padding: 10,
                                    }}>تبلیغ</Text>
                                    <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    RequestsController.reportContent(MainActivity.token, this.reportPostId, 3);
                                    this.reportChannel(null, null);
                                }}>
                                <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                    <Text style={{
                                        fontSize: MainActivity.normalize(18),
                                        color: "#000000", textAlign: 'right',
                                        marginRight: 20,
                                        padding: 10,
                                        fontFamily: 'IRANSansMobile'
                                    }}>محتوای
                                        کذب</Text>
                                    <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    RequestsController.reportContent(MainActivity.token, this.reportPostId, 4);
                                    this.reportChannel(null, null);
                                }}>
                                <View
                                    style={{
                                        backgroundColor: "#FFFFFF",
                                        opacity: 1
                                    }}>
                                    <Text style={{
                                        fontSize: MainActivity.normalize(18),
                                        color: "#000000", textAlign: 'right',
                                        marginRight: 20, fontFamily: 'IRANSansMobile',
                                        padding: 10,
                                    }}>تکراری</Text>
                                    <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    RequestsController.reportContent(MainActivity.token, this.reportPostId, 5);
                                    this.reportChannel(null, null);
                                }}>
                                <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                    <Text style={{
                                        fontSize: MainActivity.normalize(18),
                                        color: "#000000", textAlign: 'right',
                                        marginRight: 20, fontFamily: 'IRANSansMobile',
                                        padding: 10,
                                    }}>قدیمی</Text>
                                    <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                                </View>
                            </TouchableWithoutFeedback>
                        </Animated.View>
                    </View>
                </View>
            </TouchableWithoutFeedback>;
        const trendIcon = this.state.currentTab === 0 ? require("./img/ic_refresh_filled.png") : require("./img/nav_trend.png");
        const videosIcon = this.state.currentTab === 1 ? require("./img/ic_refresh_filled.png") : require("./img/nav_video.png");
        const channelIcon = this.state.currentTab === 2 ? require("./img/ic_cross_filled.png") : require("./img/ic_cross.png");
        const profileIcon = this.state.currentTab === 3 ? this.robotFilledIcons[this.state.level] :
            this.robotIcons[this.state.level];

        return (
            <SafeAreaView style={{flex: 1}}>
                <View
                    style={{flex: 1}}>
                    <Tabs
                        locked={true} initialPage={0}
                        page={this.state.currentTab}
                        renderTabBar={() => {
                            return <View height={0}/>
                        }}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: '#FFFFFF'
                            }}
                            key="1">
                            <View
                                style={{
                                    flexDirection: 'row'
                                }}>
                                <FlatList
                                    data={this.state.tags}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    ref={(ref) => {
                                        this.tagListRef = ref;
                                    }}
                                    style={{
                                        alignSelf: 'baseline',
                                        direction: 'ltr',
                                        marginLeft: 15,
                                        marginRight: -90
                                    }}
                                    ListFooterComponent={() => {
                                        return (
                                            <View
                                                style={{
                                                    height: 55,
                                                    width: 55,
                                                    borderRadius: 6,
                                                    marginStart: 5,
                                                    marginEnd: 5,
                                                    justifyContent: 'flex-end',
                                                    marginTop: 15,
                                                    marginBottom: 10
                                                }}/>
                                        )
                                    }}
                                    keyExtractor={(item, index) => item.id.toString()}
                                    renderItem={
                                        ({item}) =>
                                            item.id !== "for_you" && (this.state.currentItem === undefined ||
                                                this.state.currentItem.id !== item.id) && item.state !== 1 ?
                                                <TouchableWithoutFeedback
                                                    onPress={() => {
                                                        this.tagClicked(item);
                                                    }}>
                                                    <View>
                                                        <Grayscale>
                                                            <Image
                                                                style={{
                                                                    height: 55,
                                                                    width: 82,
                                                                    borderRadius: 6,
                                                                    marginStart: 5,
                                                                    marginEnd: 5,
                                                                    justifyContent: 'flex-end',
                                                                    marginTop: 15,
                                                                    marginBottom: 10
                                                                }}
                                                                source={{uri: item.thumbnail.xhdpi}}/>
                                                        </Grayscale>
                                                        <Text
                                                            style={{
                                                                position: 'absolute',
                                                                textAlign: 'center',
                                                                color: "#ffffff",
                                                                left: 0,
                                                                right: 0,
                                                                bottom: 10,
                                                                paddingStart: 12,
                                                                paddingEnd: 12,
                                                                fontSize: 10,
                                                                fontFamily: 'IRANSansMobile'
                                                            }}
                                                            key={item.id}>
                                                            {item.name}
                                                        </Text>
                                                    </View>
                                                </TouchableWithoutFeedback> :
                                                null
                                    }/>
                                <LinearGradient
                                    style={{
                                        paddingLeft: 20
                                    }}
                                    colors={['rgba(255,255,255,0.0)', 'rgba(255,255,255,1)', 'rgba(255,255,255,0.9)',
                                        'rgba(255,255,255,1)', 'rgba(255,255,255,1)', 'rgba(255,255,255,1)']}
                                    start={{x: 0, y: 0}}
                                    end={{x: 1, y: 0}}>
                                    {tagView}
                                </LinearGradient>
                            </View>
                            <View style={{flex: 1}}>
                                <FlatList
                                    data={this.state.data}
                                    extraData={this.state.data}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={(item, index) => item.post_id}
                                    viewabilityConfig={this.viewConfig}
                                    onViewableItemsChanged={this.viewItemsChanged}
                                    ListFooterComponent={() => {
                                        return (
                                            this.state.data.length !== 0 ?
                                                <TouchableWithoutFeedback
                                                    onPress={() => {
                                                        if (!this.state.isLoading) {
                                                            this.loadPosts();
                                                        }
                                                    }}>
                                                    <View
                                                        style={{
                                                            alignItems: 'center',
                                                            backgroundColor: '#3F51B5',
                                                            flexDirection: 'row',
                                                            justifyContent: 'center',
                                                            paddingTop: 5,
                                                            paddingBottom: 5
                                                        }}>
                                                        <Image
                                                            style={{
                                                                width: 30,
                                                                height: 30,
                                                                padding: 15,
                                                                marginEnd: 10
                                                            }}
                                                            source={require('./components/img/refresh.png')}/>
                                                        <Text
                                                            style={{
                                                                color: '#FFFFFF',
                                                                fontFamily: 'IRANSansMobile',
                                                                fontSize: 14
                                                            }}>
                                                            در حال بارگزاری
                                                        </Text>
                                                    </View>
                                                </TouchableWithoutFeedback> : null
                                        );
                                    }}
                                    onEndReached={() => {
                                        if (!this.state.isLoading) {
                                            this.loadPosts();
                                        }
                                    }}
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={false}
                                            onRefresh={() => this.loadPosts(true)}
                                        />
                                    }
                                    onEndReachedThreshold={5}
                                    style={{flex: 1, backgroundColor: "#FFFFFF"}}
                                    ref={(ref) => {
                                        this.postListRef = ref
                                    }}
                                    renderItem={(item) =>
                                        <CardAdapter
                                            item={item}
                                            callbackFullScreen={this.goFullScreen}
                                            callbackSingleItem={this.goSingleItem}
                                            callbackReport={this.reportChannel}
                                            showToastCallback={this.showToast}
                                            callbackRemove={this.removeItem}
                                            callbackCheckStat={this.checkUserStats}
                                            closeVPN={this.hideVPN}
                                            ref={(ref) => {
                                                this.trendRef.set(item.item.post_id, ref);
                                            }}/>
                                    }
                                />
                                {anim}
                                {mask}
                                {(this.state.data === null || this.state.data.length === 0) && !this.state.isLoading ?
                                    <View style={{
                                        position: 'absolute',
                                        top: 0,
                                        bottom: 0,
                                        left: 0,
                                        right: 0,
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <PTRView onRefresh={() => this.loadPosts(true)}>

                                            <Text style={{
                                                flex: 1,
                                                marginTop: (Dimensions.get('window').height - 100) / 2,
                                                textAlign: 'center',
                                                fontSize: MainActivity.normalize(18),
                                                fontFamily: 'IRANSansMobile'
                                            }}>داده
                                                ای برای نمایش وجود
                                                ندارد</Text>

                                        </PTRView>
                                    </View>
                                    : null}
                            </View>
                        </View>
                        <View style={{flex: 1}}>
                            <FlatList
                                data={this.state.videos}
                                showsVerticalScrollIndicator={false}
                                keyExtractor={(item, index) => item.post_id}
                                viewabilityConfig={this.viewConfig}
                                initialNumToRender={3}
                                onViewableItemsChanged={this.viewItemsChanged}
                                onEndReached={() => {
                                    if (!this.state.isLoading) {
                                        this.loadVideos();
                                    }
                                }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={false}
                                        onRefresh={() => this.loadVideos(true)}
                                    />
                                }
                                onEndReachedThreshold={0.9}
                                style={{flex: 1, backgroundColor: "#FFFFFF"}}
                                renderItem={(item) =>
                                    <CardAdapter
                                        item={item}
                                        callbackFullScreen={this.goFullScreen}
                                        callbackSingleItem={this.goSingleItem}
                                        callbackReport={this.reportChannel}
                                        showToastCallback={this.showToast}
                                        callbackRemove={this.removeItem}
                                        callbackCheckStat={this.checkUserStats}
                                        closeVPN={this.hideVPN}
                                        ref={(ref) => {
                                            this.trendRef.set(item.item.post_id, ref);
                                        }}/>
                                }
                            />
                            {anim}
                            {mask}
                            {this.state.videos.length === 0 && !this.state.isLoading ?
                                <View style={{
                                    position: 'absolute',
                                    top: 0,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: "#FFF"
                                }}>
                                    <Text style={{
                                        textAlign: 'center',
                                        fontSize: MainActivity.normalize(18),
                                        fontFamily: 'IRANSansMobile'
                                    }}>داده
                                        ای برای نمایش وجود
                                        ندارد</Text>
                                </View> : null}
                        </View>
                        <ChannelList
                            data={this.state.categories}
                            callback={this.props.navigation}
                            callbackCheckStat={this.checkUserStats}
                            callbackCategory={this.categoryChanged}
                            ref={(ref) => this.channelListRef = ref}/>
                        <ProfileView
                            callback={this.props.navigation}
                            level={this.state.level}/>
                    </Tabs>
                </View>
                <View style={{
                    height: 52,
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: "#F5F4F4"
                }}>
                    <TouchableWithoutFeedback onPress={
                        () => {
                            if (this.state.currentTab !== 0) {
                                MainActivity.tracker.trackScreenView("Trends");
                                if (this.state.currentTab === 1)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Videos"});
                                else if (this.state.currentTab === 3)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Profile"});
                                else if (this.state.currentTab === 2)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "ChannelList"});
                                this.timeOnTab = new Date().getTime();
                                this.setState(() => {
                                    return {currentTab: 0};
                                });
                            } else {
                                if (!this.state.isLoading) {
                                    this.loadPosts(true);
                                }
                            }
                            MainActivity.tracker.trackEvent("BottomNavigation", "Click", {label: "trends", value: "1"});
                        }}>
                        <Image
                            source={trendIcon}
                            style={{
                                flex: 1,
                                width: 20,
                                height: 20
                            }}
                            resizeMode={'contain'}/>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={
                        () => {
                            if (this.state.currentTab !== 1) {
                                MainActivity.tracker.trackScreenView("Videos");
                                if (this.state.currentTab === 0)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Trends"});
                                else if (this.state.currentTab === 3)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Profile"});
                                else if (this.state.currentTab === 2)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "ChannelList"});
                                this.timeOnTab = new Date().getTime();
                                this.setState(() => {
                                    return {currentTab: 1};
                                });
                            } else {
                                if (!this.state.isLoading) {
                                    this.loadVideos(true);
                                }
                            }
                            MainActivity.tracker.trackEvent("BottomNavigation", "Click", {label: "videos", value: "1"});
                        }}>
                        <Image
                            source={videosIcon}
                            style={{
                                flex: 1,
                                width: 20,
                                height: 20
                            }}
                            resizeMode={'contain'}/>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            if (this.state.currentTab !== 2) {
                                MainActivity.tracker.trackScreenView("ChanelList");
                                if (this.state.currentTab === 0)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Trends"});
                                else if (this.state.currentTab === 1)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Videos"});
                                else
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Profile"});
                                this.timeOnTab = new Date().getTime();
                                this.setState(() => {
                                    return {currentTab: 2};
                                });
                            }
                            MainActivity.tracker.trackEvent("BottomNavigation", "Click", {
                                label: "channelList",
                                value: "1"
                            });
                            this.setState(() => {
                                return {currentTab: 2};
                            }, () => this.channelListRef.loadChannels());
                            if (this.channelListClickedYet === false && (
                                this.state.onBoardingPage === 9 || this.state.onBoardingPage === -2)) {
                                this.pageSevenRight.setValue(-this.maxWidth);
                                this.pageSevenLeft.setValue(this.maxWidth);
                                this.setState(() => {
                                    return {onBoardingPage: -2}
                                }, () => {
                                    this.channelListClickedYet = true;
                                    DBManager.saveSettingValue('clicked_yet', 'true')
                                });
                                setTimeout(() => this.setState(() => {
                                    Animated.timing(
                                        this.pageSevenRight,
                                        {
                                            toValue: 0,
                                            duration: 400
                                        }
                                    ).start();
                                    Animated.timing(
                                        this.pageSevenLeft,
                                        {
                                            toValue: 0,
                                            duration: 400
                                        }
                                    ).start();
                                }), 700);
                            }
                        }}>
                        <Image
                            source={channelIcon}
                            style={{
                                flex: 1,
                                width: 20,
                                height: 20
                            }}
                            resizeMode={'contain'}/>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={
                        () => {
                            if (this.state.currentTab !== 3) {
                                MainActivity.tracker.trackScreenView("Profile");
                                if (this.state.currentTab === 0)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Trends"});
                                else if (this.state.currentTab === 1)
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "Videos"});
                                else
                                    MainActivity.tracker.trackTiming("BottomNavigation", new Date().getTime() - this.timeOnTab, {name: "ChannelList"});
                                this.timeOnTab = new Date().getTime();
                                this.setState(() => {
                                    return {currentTab: 3};
                                });
                            }
                            MainActivity.tracker.trackEvent("BottomNavigation", "Click", {
                                label: "profile",
                                value: "1"
                            });
                        }}>
                        <Animated.Image
                            source={profileIcon}
                            style={{
                                flex: 1,
                                width: 20,
                                height: 20,
                                transform: [{scale: this.animRobotTab}]
                            }}
                            resizeMode={'contain'}/>
                    </TouchableWithoutFeedback>
                </View>
                <Toast ref={"toast"}/>
                {this.onBoardingChannel()}
                {star}
                {robot}
            </SafeAreaView>
        );
    }
}