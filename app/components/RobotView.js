import React, {Component} from 'react';
import {
    Text, ScrollView, Image, View, TouchableWithoutFeedback, SafeAreaView, ART
} from 'react-native';
import MainActivity from "../MainActivity";
import * as shape from 'd3-shape';

const d3 = {
    shape,
};

const {
    Surface,
    Group,
    Shape,
} = ART;

const robot = [
    require('./img/mr_robot_one.png'), require('./img/mr_robot_two.png'),
    require('./img/mr_robot_three.png'), require('./img/mr_robot_four.png')];
const title = [
    "برای اینکه رباتت با هوش‌تر بشه و بیشتر متناسب با سلیقت رفتار کنه، باید مراحل زیر رو طی کنی تا بتونی به نتایج دلخواهت برسی.\n" +
    "تا الان یک ستارش رو گرفتی، همین راه رو ادامه بده!",

    "تبریک میگم…\n" +
    "ربات شما رشد خیلی خوبی داشته و الان به درجه سرباز رسیده. این یعنی به صورت کلی با سلیقت آشنا شده و میدونه حدودا چه چیزهایی رو دوست داری! \n" +
    "وقتشه اون رو تبدیل به فرمانده کنی تا خیلی بهت شبیه بشه!",

    "تبریک فرمانده…\n" +
    "ربات شما از درجه سربازی به فرماندهی رسیده. \n" +
    "این یعنی الان به خیلی دقیق‌تر میدونه که تو چه چیزهایی رو دوست داری و از چیا بدت میاد…\n" +
    "حالا اگه می‌خوای رباتت دقیقا بشه عین خودت باید کارهای زیر رو هم انجام بدی\n" +
    "تا پادشاهی راهی نمونده!",

    "پادشاهیتون استوار…\n" +
    "ربات شما به درجه پادشاه رسیده… تقریبا میشه گفت یعنی با شما خیلی فرقی نداره و کاملا با سلایق شما آشناست… اما برای توسعه قلمرو شناخت می‌تونید تعداد ستاره‌تون رو بیشتر کنید که دژتون هم مستحکم بشه…\n" +
    "بعد از انجام فعالیت‌های زیر پیشنهادهای جذابی براتون باز میشه پادشاه… موفق باشید"
];

const criteri_1 = [
    "حذف کردن ۳ دسته بندی محتوایی",
    "ذخیره و یا به اشتراک گذاری ۱۰ پست",
    "ذخیره و یا به اشتراک گذاری ۵۰ پست",
    "ذخیره و یا به اشتراک گذاری ۱۰۰ پست"
];
const criteri_2 = [
    "حذف یا اضافه کردن ۵ کانال",
    "حذف یا اضافه کردن ۱۰ کانال",
    "حضور ۱۰ روز متوالی در اپلیکیشن",
    "حضور ۲۰ روز متوالی در اپلیکیشن"
];
const criteri_3 = [
    "لایک ۵ پست",
    "لایک ۵۰ پست",
    "لایک ۱۰۰ پست",
    "لایک ۲۰۰ پست"
];
const criteri_4 = [
    "دیس لایک ۵ پست",
    "دیس لایک ۵۰ پست",
    "دیس لایک ۱۰۰ پست",
    "دیس لایک ۲۰۰ پست"
];
const criteri_5 = [
    "دیدن ۵۰ محتوا",
    "دیدن ۳۰۰ پست",
    "دیدن ۱۰۰۰ پست",
    "ریپورت ۵۰ پست"
];

const stars = [require('./img/ic_star.png'), require('./img/ic_star_empty.png')];

export class RobotView extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            level: MainActivity.getUserLevel()
        };
    }

    render() {
        const arcGenerator1 =
            d3.shape
                .arc()
                .outerRadius(15)
                .innerRadius(0)
                .startAngle(0)
                .endAngle(MainActivity.getstar1() * 6.28);
        const arcGenerator2 =
            d3.shape
                .arc()
                .outerRadius(15)
                .innerRadius(0)
                .startAngle(0)
                .endAngle(MainActivity.getstar2() * 6.28);
        const arcGenerator3 =
            d3.shape
                .arc()
                .outerRadius(15)
                .innerRadius(0)
                .startAngle(0)
                .endAngle(MainActivity.getstar3() * 6.28);
        const arcGenerator4 =
            d3.shape
                .arc()
                .outerRadius(15)
                .innerRadius(0)
                .startAngle(0)
                .endAngle(MainActivity.getstar4() * 6.28);
        const arcGenerator5 =
            d3.shape
                .arc()
                .outerRadius(15)
                .innerRadius(0)
                .startAngle(0)
                .endAngle(MainActivity.getstar5() * 6.28);
        return (
            <SafeAreaView
                style={{
                    backgroundColor: '#FFFFFF',
                    flex: 1
                }}>
                <View
                    style={{
                        height: 52,
                        backgroundColor: "#F5F4F4",
                        flexDirection: 'row'
                    }}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigation.pop()}>
                        <Image
                            source={require("./img/ic_back.png")}
                            style={{
                                alignSelf: 'flex-start',
                                height: 22,
                                width: 22,
                                resizeMode: 'contain',
                                marginTop: 15,
                                marginLeft: 25
                            }}/>
                    </TouchableWithoutFeedback>
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Image
                            source={require("./img/ic_logo.png")}
                            style={{
                                height: 32,
                                width: 32,
                                resizeMode: 'contain'
                            }}/>
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            flex: 1,
                            paddingStart: 30,
                            paddingEnd: 30
                        }}>
                        <Image
                            style={{
                                width: 88,
                                height: 74,
                                alignSelf: 'center',
                                marginTop: 25
                            }}
                            resizeMode={'stretch'}
                            source={robot[this.state.level]}/>
                        <Text
                            style={{
                                fontFamily: 'IRANSansMobile',
                                marginTop: 25,
                                fontSize: 14
                            }}>
                            {title[this.state.level]}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 40
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    flex: 1,
                                    marginRight: 15,
                                    fontSize: 14
                                }}>
                                {criteri_1[this.state.level]}
                            </Text>
                            <Surface
                                style={{position: 'absolute', right: 0}}
                                width={30}
                                height={30}>
                                <Group x={15} y={15}>
                                    <Shape
                                        key="arc"
                                        d={arcGenerator1()}
                                        fill="#F8DF1C"
                                    />
                                </Group>
                            </Surface>
                            <Image
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={stars[MainActivity.currentLevelStars[0] ? 0 : 1]}/>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 20
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    flex: 1,
                                    marginRight: 15,
                                    fontSize: 14
                                }}>
                                {criteri_2[this.state.level]}
                            </Text>
                            <Surface
                                style={{position: 'absolute', right: 0}}
                                width={30}
                                height={30}>
                                <Group x={15} y={15}>
                                    <Shape
                                        key="arc"
                                        d={arcGenerator2()}
                                        fill="#F8DF1C"
                                    />
                                </Group>
                            </Surface>
                            <Image
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={stars[MainActivity.currentLevelStars[1] ? 0 : 1]}/>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 20
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    flex: 1,
                                    marginRight: 15,
                                    fontSize: 14
                                }}>
                                {criteri_3[this.state.level]}
                            </Text>
                            <Surface
                                style={{position: 'absolute', right: 0}}
                                width={30}
                                height={30}>
                                <Group x={15} y={15}>
                                    <Shape
                                        key="arc"
                                        d={arcGenerator3()}
                                        fill="#F8DF1C"
                                    />
                                </Group>
                            </Surface>
                            <Image
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={stars[MainActivity.currentLevelStars[2] ? 0 : 1]}>
                            </Image>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 20
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    flex: 1,
                                    marginRight: 15,
                                    fontSize: 14
                                }}>
                                {criteri_4[this.state.level]}
                            </Text>
                            <Surface
                                style={{position: 'absolute', right: 0}}
                                width={30}
                                height={30}>
                                <Group x={15} y={15}>
                                    <Shape
                                        key="arc"
                                        d={arcGenerator4()}
                                        fill="#F8DF1C"
                                    />
                                </Group>
                            </Surface>
                            <Image
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={stars[MainActivity.currentLevelStars[3] ? 0 : 1]}/>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 20
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    flex: 1,
                                    marginRight: 15,
                                    fontSize: 14
                                }}>
                                {criteri_5[this.state.level]}
                            </Text>
                            <Surface
                                style={{position: 'absolute', right: 0}}
                                width={30}
                                height={30}>
                                <Group x={15} y={15}>
                                    <Shape
                                        key="arc"
                                        d={arcGenerator5()}
                                        fill="#F8DF1C"
                                    />
                                </Group>
                            </Surface>
                            <Image
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={stars[MainActivity.currentLevelStars[4] ? 0 : 1]}/>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}