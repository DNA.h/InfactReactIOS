import {
    Animated,
    Dimensions, FlatList, Image, RefreshControl, SafeAreaView,
    Text, TouchableWithoutFeedback, View
} from "react-native";
import {CardAdapter} from "./CardAdapter";
import PTRView from "react-native-pull-to-refresh";
import React from "react";
import {RequestsController} from "./utils/RequestsController";
import LottieView from "lottie-react-native";
import MainActivity from '../MainActivity'

export default class ViewSingleChannel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,

            maskShow: false,
            maskOp: new Animated.Value(),
            marginBottomAnimated: new Animated.Value(),
            marginBottomSecond: new Animated.Value(),
        };
        this.trendRef = new Map();
        this.impression = new Map();

        this.goFullScreen = this.goFullScreen.bind(this);
        this.goSingleItem = this.goSingleItem.bind(this);
        this.reportChannel = this.reportChannel.bind(this);
        this.showToast = this.showToast.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.viewItemsChanged = this.viewItemsChanged.bind(this);
        this.postLoaded = 0;
        this.loadPosts();

    }

    viewItemsChanged(props) {
        if (props.changed !== undefined)
            props.changed.forEach(async item => {
                if (this.trendRef.get(item.item.post_id) !== null && this.trendRef.get(item.item.post_id) !== undefined) {
                    if (this.impression.has(item.item.post_id)) {
                        await RequestsController.sendImpression(MainActivity.getToken(), item.item.post_id,
                            Math.floor(((new Date().getTime()) - this.impression.get(item.item.post_id)) / 400));
                        this.impression.delete(item.item.post_id);
                        if (this.trendRef.get(item.item.post_id).getImpressionSent !== undefined &&
                            this.trendRef.get(item.item.post_id).getImpressionSent !== null &&
                            this.trendRef.get(item.item.post_id).getImpressionSent()) {
                            // MainActivity.totalImpression++;
                            //         this.checkUserStats(5, null);
                        }
                    } else {
                        this.impression.set(item.item.post_id, new Date().getTime())
                    }
                    if (this.trendRef.get(item.item.post_id).itemLeftOut !== undefined &&
                        this.trendRef.get(item.item.post_id).itemLeftOut !== null)
                        this.trendRef.get(item.item.post_id).itemLeftOut();
                }
            });
    }

    removeItem(postID) {
        let data = [...this.state.data];
        for (let index in data) {
            if (data[index].post_id === postID) {
                data.splice(index, 1);
                break;
            }
        }
        this.setState({data: data});
    }

    showToast(msg, dur) {
        this.refs.toast.show(msg, dur);
    }

    mask = () => !this.state.maskShow ? null :
        <TouchableWithoutFeedback
            onPress={() => this.reportChannel(null, null)}>
            <View
                style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0
            }}>
                <Animated.View
                    style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "#000000",
                    opacity: this.state.maskOp
                }}/>
                <View style={{flex: 1}}/>
                <View>
                    <Animated.View
                        style={{
                            bottom: this.state.marginBottomSecond,
                            right: 0,
                            left: 0,
                            position: 'absolute'
                        }}>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                this.reportChannel(null, null);
                            }}>
                            <View
                                style={{
                                    backgroundColor: "#FFFFFF",
                                    opacity: 1
                                }}>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                    <Image
                                        source={require("./img/ic_back.png")}
                                        style={{
                                            alignSelf: 'flex-start',
                                            height: 22,
                                            width: 22,
                                            resizeMode: 'contain',
                                            marginTop: 15,
                                            marginLeft: 25
                                        }}/>
                                    <View style={{flex: 1}}/>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            color: "#000000",
                                            textAlign: 'right',
                                            padding: 10,
                                            fontFamily: 'IRANSansMobile'
                                        }}>
                                        دلیل گزارش</Text>
                                </View>
                                <View style={{height: 5, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(MainActivity.getToken(), this.reportPostId, 1);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>محتوای
                                    غیراخلاقی</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(MainActivity.getToken(), this.reportPostId, 2)
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>تبلیغ</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(MainActivity.getToken(), this.reportPostId, 3);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>محتوای
                                    کذب</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(MainActivity.getToken(), this.reportPostId, 4);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>تکراری</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(MainActivity.getToken(), this.reportPostId, 5);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>قدیمی</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                </View>
            </View>
        </TouchableWithoutFeedback>;

    reportChannel(post_id) {
        this.reportPostId = post_id;
        this.setState(() => {
            return {maskShow: !this.state.maskShow};
        });
        if (post_id === null) {
            this.state.marginBottomSecond.setValue(-700);
            this.state.marginBottomAnimated.setValue(-500);
            return
        }
        this.state.maskOp.setValue(0.0);
        Animated.timing(
            this.state.maskOp, {
                toValue: 0.75,
                duration: 400,
                delay: 100
            }
        ).start();
        this.state.marginBottomSecond.setValue(-700);
        Animated.timing(
            this.state.marginBottomSecond, {
                toValue: 0,
                duration: 400,
                delay: 600
            }).start();
    }

    goSingleItem(post_id) {
        this.props.navigation.navigate('SingleItem', {
            id: post_id,
            token: MainActivity.getToken()
        });
    }

    goFullScreen(media_url, type, time = 0, post_id) {
        this.props.navigation.navigate('FullScreen', {
            media_url: media_url, isVideo: type === 3, time: time,
            token: MainActivity.getToken(), post_id: post_id
        });
    }

    checkUserStats() {
        //ToDo
    }

    async loadPosts() {
        this.setState(() => {
            return {isLoading: true}
        });
        let d = await
            RequestsController.getPostsFromPublisher(MainActivity.getToken(), this.props.navigation.getParam('channel_id'), 10, this.postLoaded);
        if (d === null) {
            this.setState(() => {
                return {isLoading: false};
            });
            return;
        }
        if (d !== undefined) {
            this.setState(() => {
                return {data: this.state.data.concat(d), isLoading: false};
            });
            this.postLoaded += 10;
        } else
            this.setState(() => {
                return {isLoading: false};
            });
    }

    render() {
        let anim = this.state.isLoading ?
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <LottieView
                    source={require("./animation_loading")}
                    style={{alignSelf: 'center', height: 50, position: 'absolute'}}
                    autoPlay
                    resizeMode={"contain"}
                    loop/>
            </View> : null;

        return (
            <SafeAreaView
                style={{flex: 1}}>
                <View
                    style={{
                        height: 52,
                        backgroundColor: "#F5F4F4",
                        flexDirection: 'row'
                    }}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigation.pop()}>
                        <Image
                            source={require("./img/ic_back.png")}
                            style={{
                                alignSelf: 'flex-start', height: 22, width: 22, resizeMode: 'contain',
                                marginTop: 15, marginLeft: 25
                            }}/>
                    </TouchableWithoutFeedback>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image source={require("./img/ic_logo.png")}
                               style={{
                                   height: 32, width: 32, resizeMode: 'contain'
                               }}/>
                    </View>
                </View>
                <FlatList
                    data={this.state.data}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => item.post_id}
                    onViewableItemsChanged={this.viewItemsChanged}
                    ListFooterComponent={() => {
                        return (
                            this.state.data.length !== 0 ?
                                <TouchableWithoutFeedback
                                    onPress={() => {
                                        if (!this.state.isLoading) {
                                            this.loadPosts();
                                        }
                                    }}>
                                    <View
                                        style={{
                                            alignItems: 'center',
                                            backgroundColor: '#3F51B5',
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            paddingTop: 5,
                                            paddingBottom: 5
                                        }}>
                                        <Image
                                            style={{
                                                width: 30,
                                                height: 30,
                                                padding: 15
                                            }}
                                            source={require('./img/refresh.png')}/>
                                        <Text
                                            style={{
                                                color: '#FFFFFF',
                                                fontFamily: 'IRANSansMobile',
                                                fontSize: 14
                                            }}>
                                            در حال بارگزاری
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback> : null
                        );
                    }}
                    onEndReached={() => {
                        if (!this.state.isLoading) {
                            this.loadPosts();
                        }
                    }}
                    refreshControl={
                        <RefreshControl
                            refreshing={false}
                            onRefresh={() => this.loadPosts()}
                        />
                    }
                    onEndReachedThreshold={0.9}
                    style={{flex: 1, backgroundColor: "#FFFFFF"}}
                    renderItem={(item) =>
                        <CardAdapter
                            item={item}
                            callbackFullScreen={this.goFullScreen}
                            callbackSingleItem={this.goSingleItem}
                            callbackReport={this.reportChannel}
                            showToastCallback={this.showToast}
                            callbackRemove={this.removeItem}
                            callbackCheckStat={this.checkUserStats}
                            ref={(ref) => {
                                this.trendRef.set(item.item.post_id, ref);
                            }}
                        />
                    }
                />
                {anim}
                {this.mask()}
                {(this.state.data === null || this.state.data.length === 0) && !this.state.isLoading ?
                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <PTRView onRefresh={() => this.loadPosts()}>

                            <Text style={{
                                flex: 1,
                                marginTop: (Dimensions.get('window').height - 100) / 2,
                                textAlign: 'center',
                                fontSize: 18,
                                fontFamily: 'IRANSansMobile'
                            }}>داده
                                ای برای نمایش وجود
                                ندارد</Text>

                        </PTRView>
                    </View>
                    : null
                }
            </SafeAreaView>
        );
    }

}