import React, {Component} from 'react';
import {
    Text, ScrollView, Image, View, TouchableWithoutFeedback,
    ImageBackground, Animated, SafeAreaView
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {RequestsController} from "./utils/RequestsController";
import Modal from "react-native-modal";
import MainActivity from "../MainActivity";

export class ProfileView extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            percent: 0.0,
            robot: props.level,
            animatePercent: new Animated.Value(),
            maxWidth: 0,
            showDialog: false
        };
        this.state.animatePercent.setValue(0);
        this.callback = props.callback;
    }

    async getAI() {
        let d = await RequestsController.getAI(MainActivity.getToken());
        this.setState(() => {
            return {percent: d.score, robot: d.ai_bot.type};
        });
    }

    _setMaxWidth(event) {
        this.state.maxWidth = event.nativeEvent.layout.width;
        Animated.timing(
            this.state.animatePercent, {
                toValue: ((this.state.maxWidth - 32) * this.state.percent) - 5,
                duration: 500
            }
        ).start();
        //console.log("maxwidth ", this.state.maxWidth);
        //console.log("tovalue ", (this.state.maxWidth * this.state.percent) - 22);
    }

    _toggleModal = () =>
        this.setState({showDialog: !this.state.showDialog});

    render() {
        let robot_dis = [require('./img/mr_robot_one_disabled.png'), require('./img/mr_robot_two_disabled.png'),
            require('./img/mr_robot_three_disabled.png'), require('./img/mr_robot_four_disabled.png')];
        let robot = [require('./img/mr_robot_one.png'), require('./img/mr_robot_two.png'),
            require('./img/mr_robot_three.png'), require('./img/mr_robot_four.png')];
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: "#FFFFFF"
                }}>
                <View
                    style={{
                        height: 52,
                        backgroundColor: "#F5F4F4"
                    }}>
                    <Image
                        source={require("./img/ic_logo.png")}
                        style={{
                            alignSelf: 'center',
                            height: 22,
                            resizeMode: 'contain',
                            marginTop: 15
                        }}/>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}>
                    <View
                        style={{backgroundColor: "#FFFFFF"}}>
                        <Text
                            style={{
                                fontSize: 18,
                                textAlign: 'center',
                                marginTop: 16,
                                fontFamily: 'IRANSansMobile'
                            }}>
                            ثبت نام / ورود
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                marginTop: 25,
                                flex: 1
                            }}>
                            <TouchableWithoutFeedback
                                onPress={this._toggleModal}>
                                <Image
                                    style={{
                                        flex: 1,
                                        aspectRatio: 1,
                                        marginLeft: 30,
                                        marginRight: 30
                                    }}
                                    source={require("./img/ic_email.png")}/>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={this._toggleModal}>
                                <Image
                                    style={{
                                        flex: 1,
                                        aspectRatio: 1,
                                        marginLeft: 30,
                                        marginRight: 30
                                    }}
                                    source={require("./img/ic_google.png")}/>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={this._toggleModal}>
                                <Image
                                    style={{
                                        flex: 1,
                                        aspectRatio: 1,
                                        marginLeft: 30,
                                        marginRight: 30
                                    }}
                                    source={require("./img/ic_phone.png")}/>
                            </TouchableWithoutFeedback>
                            <Modal isVisible={this.state.showDialog}
                                   onBackdropPress={this._toggleModal}>
                                <View
                                    style={{
                                        backgroundColor: "#FFFFFF",
                                        borderRadius: 10,
                                        marginStart: 10,
                                        marginEnd: 10,
                                        paddingStart: 10,
                                        paddingEnd: 10,
                                        paddingBottom: 5,
                                        alignItems: 'center'
                                    }}>
                                    <Text
                                        style={{
                                            fontFamily: 'IRANSansMobile',
                                            fontSize: 18,
                                            marginTop: 10,
                                            textAlign: 'center'
                                        }}>
                                        این ویژگی در نسخه های بعدی در دسترس خواهد بود
                                    </Text>
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            margin: 10,
                                            paddingEnd: 25,
                                            paddingStart: 25,
                                            alignItems: 'center'
                                        }}>
                                        <TouchableWithoutFeedback
                                            onPress={this._toggleModal}>
                                            <View>
                                                <Text
                                                    style={{
                                                        fontFamily: 'IRANSansMobile',
                                                        paddingEnd: 15,
                                                        paddingStart: 15,
                                                        marginTop: 5,
                                                        marginBottom: 5,
                                                        fontSize: 17,
                                                        color: "#50E3C2",
                                                        borderWidth: 1,
                                                        borderColor: "#50E3C2",
                                                        borderRadius: 10,
                                                        marginStart: 15,
                                                        marginEnd: 15
                                                    }}>
                                                    خب
                                                </Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                </View>
                            </Modal>
                        </View>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#F5F4F4",
                            marginTop: 10,
                            marginBottom: 10
                        }}/>
                        <Text style={{
                            fontSize: 14, fontFamily: 'IRANSansBold', textAlign: 'right',
                            marginRight: 20
                        }}>
                            وضعیت هوش مصنوعی تو
                        </Text>
                        <View style={{
                            flexDirection: 'row', alignItems: 'center', marginStart: 20,
                            marginEnd: 20
                        }}>
                            <Image
                                style={{
                                    flex: this.state.robot === 0 ? 4 : 2,
                                    marginTop: 10,
                                    aspectRatio: 1.18,
                                    marginLeft: 10,
                                    marginRight: 10
                                }}
                                source={this.state.robot === 0 ? robot[0] : robot_dis[0]}
                                resizeMode={'contain'}/>
                            <Image
                                style={{
                                    flex: this.state.robot === 1 ? 4 : 2,
                                    marginTop: 10,
                                    aspectRatio: 1.18,
                                    marginLeft: 10,
                                    marginRight: 10
                                }}
                                source={this.state.robot === 1 ? robot[1] : robot_dis[1]}
                                resizeMode={'stretch'}/>
                            <Image
                                style={{
                                    flex: this.state.robot === 2 ? 4 : 2,
                                    marginTop: 10,
                                    aspectRatio: 1.18,
                                    marginLeft: 10,
                                    marginRight: 10
                                }}
                                source={this.state.robot === 2 ? robot[2] : robot_dis[2]}
                                resizeMode={'stretch'}/>
                            <Image
                                style={{
                                    flex: this.state.robot === 3 ? 4 : 2,
                                    marginTop: 10,
                                    aspectRatio: 1.18,
                                    marginLeft: 10,
                                    marginRight: 10
                                }}
                                source={this.state.robot === 3 ? robot[3] : robot_dis[3]}
                                resizeMode={'stretch'}/>
                        </View>
                        <TouchableWithoutFeedback onPress={() => {
                            this.callback.navigate('Robot')
                        }}>
                            <View
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    marginTop: 10
                                }}>
                                <LinearGradient colors={["#00CBF6", "#4babf9", "#9C2BB5"]}
                                                start={{x: 0, y: 1}} end={{x: 1, y: 0}} style={{
                                    borderRadius: 13,
                                    overflow: 'hidden'
                                }}>
                                    <View
                                        style={{
                                            paddingTop: 5,
                                            paddingBottom: 5,
                                            paddingStart: 20,
                                            paddingEnd: 20,
                                            backgroundColor: '#FFFFFF',
                                            margin: 1,
                                            borderRadius: 13
                                        }}>
                                        <Text style={{fontFamily: 'IRANSansMobile', fontSize: 14, color: '#000000'}}>
                                            ارتقای هوش
                                        </Text>
                                    </View>
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#DDD",
                            marginTop: 10,
                            marginBottom: 5
                        }}/>
                        <TouchableWithoutFeedback onPress={() => this.callback.navigate('List', {
                            token: MainActivity.getToken(),
                            page: 'history'
                        })}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{
                                    paddingTop: 5,
                                    paddingBottom: 5, textAlign: 'right', marginRight: 15, fontFamily: 'IRANSansMobile'
                                }}>تاریخچه</Text>
                                <Image source={require("./img/ic_history.png")}
                                       style={{alignSelf: 'center', marginRight: 15, padding: 5}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#DDD",
                            marginTop: 5,
                            marginBottom: 5
                        }}/>
                        <TouchableWithoutFeedback
                            onPress={() => this.callback.navigate('List', {
                                token: MainActivity.getToken(),
                                page: 'liked'
                            })}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{
                                    paddingTop: 5,
                                    paddingBottom: 5, textAlign: 'right', marginRight: 15, fontFamily: 'IRANSansMobile'
                                }}>علاقه مندی</Text>
                                <Image source={require("./img/ic_fav.png")}
                                       style={{alignSelf: 'center', marginRight: 15, padding: 5}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#DDD",
                            marginTop: 5,
                            marginBottom: 5
                        }}/>
                        <TouchableWithoutFeedback onPress={() => this.callback.navigate('List', {
                            token: MainActivity.getToken(),
                            page: 'saved'
                        })}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{
                                    paddingTop: 5,
                                    paddingBottom: 5, textAlign: 'right', marginRight: 15, fontFamily: 'IRANSansMobile'
                                }}>ذخیره شده</Text>
                                <Image source={require("./img/ic_saveds.png")}
                                       style={{alignSelf: 'center', marginRight: 15, padding: 5}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#DDD",
                            marginTop: 5,
                            marginBottom: 5
                        }}/>
                        <TouchableWithoutFeedback
                            onPress={() => this.callback.navigate('Setting', {token: MainActivity.getToken()})}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{
                                    paddingTop: 5,
                                    paddingBottom: 5, textAlign: 'right', marginRight: 15, fontFamily: 'IRANSansMobile'
                                }}>تنظیمات</Text>
                                <Image source={require("./img/ic_settings.png")}
                                       style={{alignSelf: 'center', marginRight: 15, padding: 5}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <View style={{
                            width: "100%",
                            height: 1,
                            backgroundColor: "#DDD",
                            marginTop: 5,
                            marginBottom: 5
                        }}/>
                        <TouchableWithoutFeedback onPress={() => {
                            this.callback.navigate('Contact', {token: MainActivity.getToken()});
                        }}>
                            <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                                <Text style={{
                                    paddingTop: 5,
                                    paddingBottom: 5, textAlign: 'right', marginRight: 15, fontFamily: 'IRANSansMobile'
                                }}>ارتباط با ما</Text>
                                <Image source={require("./img/ic_outbox.png")}
                                       style={{alignSelf: 'center', marginRight: 15, padding: 5}}/>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}