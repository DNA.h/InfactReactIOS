import {
    View, TextInput, Image, Text, FlatList,
    TouchableWithoutFeedback, Platform, UIManager, Animated, Linking
} from 'react-native';
import React from "react";
import {RequestsController} from "./utils/RequestsController";
import ChannelIcon from "./utils/ChannelIcon";
import LottieView from "lottie-react-native";
import MainActivity from "../MainActivity";
import CategoryIcon from "./utils/CategoryIcon";

export default class ChannelList extends React.PureComponent {

    constructor(props) {
        super(props);
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        this.state = {
            borderRadius: 0,
            data: props.data,
            all_channels: [],
            my_channels: [],
            filter: "",
            isLoading: false
        };
        this.all_channels_refs = new Map();
        this.my_channels_refs = new Map();
        this._handleLayout = this._handleLayout.bind(this);
        this._itemClicked = this._itemClicked.bind(this);
        this.loadChannels = this.loadChannels.bind(this);
        this._searchChannels = this._searchChannels.bind(this);
        this._callbackFollowPublisher = this._callbackFollowPublisher.bind(this);
        this._callbackDislikePublisher = this._callbackDislikePublisher.bind(this);
        this.viewChannel = this.viewChannel.bind(this);
    }


    async loadChannels() {
        this.setState({isLoading: true, all_channels: []});
        let t = await RequestsController.getPublishers(MainActivity.getToken());
        let items = [];
        for (let index in t) {
            if (t[index].state !== 0) items.push(t[index]);
        }
        this.setState(() => {
            return {
                all_channels: t,
                my_channels: items,
                isLoading: false
            };
        });
    }

    async _searchChannels() {
        this.setState({isLoading: true, all_channels: []});
        let t = await RequestsController.searchChannel(MainActivity.getToken(), this.state.filter);
        let items = [];
        for (let index in t) {
            if (t[index].state !== 0) items.push(t[index]);
        }
        this.setState(() => {
            return {
                all_channels: t,
                my_channels: items,
                isLoading: false
            };
        });
    }

    _handleLayout(event) {
        this.setState({
            borderRadius: event.nativeEvent.layout.width / 3
        });
    }

    _itemClicked(event) {
        this.state.data.forEach((item) => {
            if (item[0][0].item.name === event[0].item.name) {
                RequestsController.dislikeCategory(MainActivity.getToken(), event[0].item.id, !event[1].selected);
                item[0][1].selected = !item[0][1].selected;
                this.props.callbackCategory(event[0].item.id);
            }
            if (item.length > 1 && item[1][0].item.name === event[0].item.name) {
                RequestsController.dislikeCategory(MainActivity.getToken(), event[0].item.id, !event[1].selected);
                item[1][1].selected = !item[1][1].selected;
                this.props.callbackCategory(event[0].item.id);
            }
            if (item.length > 2 && item[2][0].item.name === event[0].item.name) {
                RequestsController.dislikeCategory(MainActivity.getToken(), event[0].item.id, !event[1].selected);
                item[2][1].selected = !item[2][1].selected;
                this.props.callbackCategory(event[0].item.id);
            }
        });
        this.setState({data: this.state.data});
    }

    _callbackFollowPublisher(publisher_id, flag, other) {
        if (flag && other) {
        }//Do absolutely nothing
        else this.props.callbackCheckStat(4, (flag && !other) || (other && !flag));
        if (!flag) { //we are sure we have to remove it from mychannel
            for (let index in this.state.my_channels)
                if (this.state.my_channels[index].id === publisher_id) {
                    for (let i in this.state.all_channels)
                        if (this.state.all_channels[i].id === publisher_id) {
                            this.state.all_channels[i].state = 0;
                            if (this.all_channels_refs.get(publisher_id) !== undefined &&
                                this.all_channels_refs.get(publisher_id) !== null) {
                                this.all_channels_refs.get(publisher_id).stateIsChanged(0);
                            }
                        }
                    this.setState((prev) => ({
                        my_channels: prev.my_channels.filter((_, i) => i !== parseInt(index))
                    }));
                    break;
                }
        } else {//is it already disliked or what?
            if (other) {//it is already in myChannels, just change state
                for (let index in this.state.my_channels)
                    if (this.state.my_channels[index].id === publisher_id) {
                        for (let i in this.state.all_channels)
                            if (this.state.all_channels[i].id === publisher_id) {
                                this.state.all_channels[i].state = 2;
                                this.all_channels_refs.get(publisher_id).stateIsChanged(2);
                                break;
                            }
                        this.state.my_channels[index].state = 2;
                        if (this.my_channels_refs.get(publisher_id) !== null &&
                            this.my_channels_refs.get(publisher_id) !== undefined &&
                            this.my_channels_refs.get(publisher_id).stateIsChanged !== null &&
                            this.my_channels_refs.get(publisher_id).stateIsChanged !== undefined)
                            this.my_channels_refs.get(publisher_id).stateIsChanged(2);
                        this.setState({
                            my_channels: this.state.my_channels
                        });
                        break;
                    }
            } else {//nope we have to add it and set state too
                for (let index in this.state.all_channels)
                    if (this.state.all_channels[index].id === publisher_id) {
                        this.state.all_channels[index].state = 2;
                        this.setState({
                            my_channels: this.state.my_channels.concat(this.state.all_channels[index])
                        });
                        break;
                    }
            }
        }
        RequestsController.followPublisher(MainActivity.getToken(), publisher_id, flag);
    }

    _callbackDislikePublisher(publisher_id, flag, other) {
        if (flag && other) {
        }//Do absolutely nothing
        else this.props.callbackCheckStat(4, (flag && !other) || (other && !flag));
        if (!flag) { //we are sure we have to remove it from myChannel
            for (let index in this.state.my_channels)
                if (this.state.my_channels[index].id === publisher_id) {
                    for (let i in this.state.all_channels)
                        if (this.state.all_channels[i].id === publisher_id) {
                            this.state.all_channels[i].state = 0;
                            if (this.all_channels_refs.get(publisher_id) !== undefined &&
                                this.all_channels_refs.get(publisher_id) !== null)
                                this.all_channels_refs.get(publisher_id).stateIsChanged(0);
                        }
                    this.setState((prev) => ({
                        my_channels: prev.my_channels.filter((_, i) => i !== parseInt(index))
                    }));
                    break;
                }
        } else {//is it already liked or what?
            if (other) {//it is already in myChannels, just change state
                for (let index in this.state.my_channels)
                    if (this.state.my_channels[index].id === publisher_id) {
                        for (let i in this.state.all_channels)
                            if (this.state.all_channels[i].id === publisher_id) {
                                this.state.all_channels[i].state = 1;
                                this.all_channels_refs.get(publisher_id).stateIsChanged(1);
                                break;
                            }
                        this.state.my_channels[index].state = 1;
                        if (this.my_channels_refs.get(publisher_id) !== null &&
                            this.my_channels_refs.get(publisher_id) !== undefined &&
                            this.my_channels_refs.get(publisher_id).stateIsChanged !== null &&
                            this.my_channels_refs.get(publisher_id).stateIsChanged !== undefined)
                            this.my_channels_refs.get(publisher_id).stateIsChanged(1);
                        this.setState({
                            my_channels: this.state.my_channels
                        });
                        break;
                    }
            } else {//nope we have to add it and set state too
                for (let index in this.state.all_channels)
                    if (this.state.all_channels[index].id === publisher_id) {
                        this.state.all_channels[index].state = 1;
                        this.setState({
                            my_channels: this.state.my_channels.concat(this.state.all_channels[index])
                        });
                        break;
                    }
            }
        }
        RequestsController.dislikePublisher(MainActivity.getToken(), publisher_id, flag);
    }

    viewChannel(ch_ID) {
        this.props.callback.navigate('SingleChannel', {'channel_id': ch_ID});
    }

    render() {
        let Tabs = require('react-native-scrollable-tab-view');
        let CustomTabBar = require('./utils/CustomTabBar');
        let anim = this.state.isLoading ?
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <LottieView
                    source={require("./animation_loading")}
                    style={{alignSelf: 'center', height: 50, position: 'absolute'}}
                    autoPlay
                    resizeMode={"contain"}
                    loop/>
            </View> : null;
        return (
            <View
                style={{flex: 1}}>
                <View
                    style={{
                        flexDirection: 'row',
                        marginStart: 10,
                        marginEnd: 10,
                        borderRadius: 6,
                        marginTop: 10,
                        backgroundColor: "#e1e1e1"
                    }}>
                    {this.state.filter !== "" ?
                        <TouchableWithoutFeedback
                            onPress={() => {
                                if (this.state.filter !== "") {
                                    this.refs.input.clear();
                                    this.setState({filter: ""});
                                    this.loadChannels();
                                }
                            }}>

                            <Image
                                style={{
                                    width: 22,
                                    height: 24,
                                    alignSelf: 'center',
                                    marginRight: 25
                                }}
                                source={require('./img/ic_remove.png')}/>
                        </TouchableWithoutFeedback>
                        : null}
                    <TextInput
                        style={{
                            flex: 1,
                            fontFamily: 'IRANSansMobile'
                        }}
                        onChangeText={(text) => this.setState({filter: text})}
                        onSubmitEditing={this._searchChannels}
                        ref={"input"}
                        underlineColorAndroid={"transparent"}
                        placeholder={'کانال ها رو سرچ کن… '}
                    />
                    <Image
                        style={{
                            width: 22,
                            height: 24,
                            alignSelf: 'center',
                            marginRight: 25
                        }}
                        source={require('./img/ic_search.png')}/>
                </View>
                <Tabs
                    locked={false}
                    initialPage={1}
                    style={{
                        flex: 1,
                        marginTop: 15
                    }}
                    renderTabBar={() => <CustomTabBar/>}>
                    <View style={{flex: 1}} tabLabel={'کانال‌های من'}>
                        <FlatList
                            style={{flex: 1, marginTop: 20}}
                            data={this.state.my_channels}
                            extraData={this.state.my_channels}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({item}) =>
                                <ChannelIcon
                                    callbackFollowPublisher={this._callbackFollowPublisher}
                                    callbackDislikePublisher={this._callbackDislikePublisher}
                                    callbackViewChannel={this.viewChannel}
                                    ref={(ref) => {
                                        this.my_channels_refs.set(item.id, ref);
                                    }}
                                    item={item}/>
                            }/>
                    </View>
                    <View style={{flex: 1}} tabLabel={'همه کانال‌ها'}>
                        <FlatList
                            style={{flex: 1, marginTop: 20}}
                            data={this.state.all_channels}
                            keyExtractor={(item, index) => item.id}
                            renderItem={({item}) =>
                                <ChannelIcon
                                    callbackFollowPublisher={this._callbackFollowPublisher}
                                    callbackDislikePublisher={this._callbackDislikePublisher}
                                    callbackViewChannel={this.viewChannel}
                                    ref={(ref) => {
                                        this.all_channels_refs.set(item.id, ref);
                                    }}
                                    item={item}/>
                            }/>
                    </View>
                    <View
                        style={{flex: 1}}
                        tabLabel={'دسته‌بندی‌ها'}
                        onLayout={this._handleLayout}>
                        <FlatList
                            style={{
                                flex: 1,
                                marginTop: 20
                            }}
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.data}
                            extraData={this.state.data}
                            renderItem={({item}) =>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginBottom: 10,
                                    width: '100%',
                                    height: this.state.borderRadius + 10
                                }}>
                                    <CategoryIcon
                                        height={this.state.borderRadius + 10}
                                        bool={item[0][1].selected}
                                        image={{uri: item[0][0].item.thumbnail.main_pic}}
                                        caption={item[0][0].item.name}
                                        item={item[0]}
                                        notOnBoarding={true}
                                        callbackClicked={this._itemClicked}
                                    />
                                    {item.length > 1 ?
                                        <CategoryIcon
                                            height={this.state.borderRadius + 10}
                                            bool={item[1][1].selected}
                                            image={{uri: item[1][0].item.thumbnail.main_pic}}
                                            caption={item[1][0].item.name}
                                            item={item[1]}
                                            notOnBoarding={true}
                                            callbackClicked={this._itemClicked}
                                        /> : <View style={{flex: 1}}/>}
                                    {item.length > 2 ?
                                        <CategoryIcon
                                            height={this.state.borderRadius + 10}
                                            bool={item[2][1].selected}
                                            image={{uri: item[2][0].item.thumbnail.main_pic}}
                                            caption={item[2][0].item.name}
                                            item={item[2]}
                                            notOnBoarding={true}
                                            callbackClicked={this._itemClicked}
                                        /> : <View style={{flex: 1}}/>}
                                </View>}>
                        </FlatList>
                    </View>
                </Tabs>
                {anim}
            </View>
        );
    }
}