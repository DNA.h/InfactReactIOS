import React, {Component, PureComponent} from 'react';
import {View, Text, Image, Dimensions} from 'react-native'
import ImageLoad from 'react-native-image-placeholder'

export class PostAdapter extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            post_id: props.item.item.post_id,
            post_type: props.item.item.post_type,
            post_url: props.item.item.post_url,
            width: props.item.item.width,
            height: props.item.item.height,
            channel_url: props.item.item.channel_avatar,
            channel_name: props.item.item.channel_name,
            media_url: props.item.item.media_url,
            channel_title: props.item.item.channel_title
        };
        console.log(props);
    }

    componentDidMount() {
        let srcWidth = this.state.width;
        let srcHeight = this.state.height;
        const maxWidth = Dimensions.get('window').width;

        const ratio = maxWidth / srcWidth;
        this.setState({width: maxWidth, height: srcHeight * ratio});
    }

    render() {
        let image = this.state.post_type === 2 ? this.state.media_url :
            "http://164.138.22.114:8080/file/7bff78cb-b320-4bc2-aac2-bf2518c46831.jpg";
        return (
            <View>
                <View style={{width: '100%', flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center'}}>
                    <Text>{this.state.channel_title}</Text>
                    <Image source={{uri: this.state.channel_url}}
                           style={{width: 52, height: 52}}/>
                </View>
                <ImageLoad source={{uri: image}}
                           resizeMode={'cover'}
                           placeholderSource={require('./img/place_holder.png')}
                           style={{width: this.state.width, height: this.state.height}}/>
                <Text>This
                    is
                    Example</Text>
            </View>
        );
    }
}