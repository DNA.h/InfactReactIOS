import React, {Component, PureComponent} from 'react';
import {View, Text, Image, CheckBox, TouchableWithoutFeedback} from 'react-native'
import ImageLoad from 'react-native-image-placeholder'
import {RequestsController} from "./utils/RequestsController";

export class SmallCardAdapter extends React.Component {
    constructor(props) {
        super(props);
        this.message = props.item.item.message;
        this.thumbnail = props.item.item.thumbnail;
        this.state = {
            checked: false,
            showDeletes: props.show
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState(() => {
            return {showDeletes: nextProps.show}
        });
    }

    deleteSelf() {
        if (this.state.checked === true) {
            return this.props.item.item.post_id;
        } else
            return "-1";
    }

    render() {
        let src = this.thumbnail !== "" ?
            <ImageLoad
                style={{
                    width: 100,
                    height: 80,
                    margin: 15
                }}
                source={{uri: this.thumbnail}}
                placeholderSource={require('./img/small_placeholder.png')}/>
            : <Image
                style={{
                    width: 100,
                    height: 80,
                    margin: 15
                }}
                source={require('./img/small_placeholder.png')}/>;
        let del = !this.props.show ? null :
            <CheckBox value={this.state.checked} onValueChange={() => {
                this.setState(() => {
                    return {checked: !this.state.checked};
                });
            }}/>;
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    this.props.callback(this.props.item.item.post_id)
                }}
                onLongPress={this.props.callbackDelete}>
                <View>
                    <View style={{flexDirection: 'row', backgroundColor: "#FFFFFF", alignItems: 'center'}}>
                        <Text style={{
                            flex: 3, marginStart: 10, marginEnd: 10, textAlign: 'right',
                            fontFamily: 'IRANSansMobile'
                        }}
                              numberOfLines={3}>{this.message}</Text>
                        {src}
                        {del}
                    </View>
                    <View style={{width: '100%', height: 1, backgroundColor: "#CACACA"}}/>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}