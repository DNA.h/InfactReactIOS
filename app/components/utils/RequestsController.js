import {ConnectionManager} from "./ConnectionManager";
import GLOBALS from './Globals'
import {DBManager} from "./DBManager";
import MainActivity from "../../MainActivity";

export class RequestsController {
    static async loadToken(fcm_token) {
        let uuid = await DBManager.getUUID();
        let response = await fetch(
            GLOBALS.BASE_URL + GLOBALS.URL_REGISTER, {
                method: 'POST',
                body: JSON.stringify({'id': uuid, 'fcm_token': fcm_token})
            });
        let json = await response.json();
        //console.log('token is ', json.token);
        return json.token;
    }

    static async getPosts(token, limit, offset, onlyVideo, sID) {
        let typeVideo = onlyVideo ? "&type=video" : "";
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "?limit=" + limit + "&offset=" + offset + typeVideo,
            'GET', null, new Headers({'Authorization': "Bearer " + token, 'sID': sID}), true
        );
        //return undefined;
        return json !== undefined ? json.post_list : undefined;
    }

    static async getPostsFromCategory(token, tag, limit, offset, sID) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_TAGS + '/' + tag + "/posts?limit=" + limit + "&offset=" + offset,
            'GET', null, new Headers({'Authorization': "Bearer " + token, 'sID': sID}), true
        );
        return json !== undefined ? json.post_list : undefined;
    }

    static async getPostsFromPublisher(token, ch_id, limit, offset) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + '/' + ch_id + "/posts?limit"+limit + "&offset=" + offset,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.post_list : undefined;
    }

    static async getSaveds(token, limit, offset) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_SAVED + "?limit=" + limit + "&offset=" + offset,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.post_list : undefined;
    }

    static async getLikeds(token, limit, offset) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_LIKED + "?limit=" + limit + "&offset=" + offset,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.post_list : undefined;
    }

    static async getHistory(token, limit, offset) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_HISTORY + "?limit=" + limit + "&offset=" + offset,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.post_list : undefined;
    }


    static async sendShare(token, postID) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/share/",
            'POST', null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendSave(token, postID, del) {
        let type = !del ? 'POST' : 'DELETE';
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/save/",
            type, null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendUpVote(token, postID, del) {
        let type = !del ? 'POST' : 'DELETE';
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/up-vote/",
            type, null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendDownVote(token, postID, del) {
        let type = !del ? 'POST' : 'DELETE';
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/down-vote/",
            type, null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendRemoveSaved(token, postID) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_SAVED + "/" + postID,
            'DELETE', null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendRemoveHistory(token, postID) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_HISTORY + "/" + postID,
            'DELETE', null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendRemoveUpVote(token, postID) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_LIKED + "/" + postID,
            'DELETE', null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async resetAI(token) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_RESET_AI,
            'POST', null, new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async sendImpression(token, postID, time) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/impression/",
            'POST', JSON.stringify({'time': time})
            , new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async searchChannel(token, str) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + "/find",
            'POST', JSON.stringify({'query': str})
            , new Headers({'Authorization': "Bearer " + token}), true
        );
        //console.log('channels ', json);
        return json.publisher_list;
    }

    static async reportContent(token, postID, reason) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/report/",
            'POST', JSON.stringify({'type': reason})
            , new Headers({'Authorization': "Bearer " + token})
        );
    }

    //ToDo new api
    static async reportChannel(token, userID) {
        //console.log("token is ", token);
        //console.log("userID is ", userID);
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + "/" + userID,
            'DELETE', null
            , new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async likeChannel(token, userID) {
        //console.log("token is ", token);
        //console.log("userID is ", userID);
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + "/" + userID + /follow/,
            'Post', null
            , new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async dislikeContent(token, postID) {
        //console.log("token is ", token);
        //console.log("postID is ", postID);
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/",
            'DELETE', null
            , new Headers({'Authorization': "Bearer " + token})
        );
    }

    static async getSinglePost(token, postID) {
        return await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/",
            'GET', null
            , new Headers({'Authorization': "Bearer " + token}), true
        );
    }

    static async getCategories(token) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_TAGS,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.tag_list : undefined;
    }

    static async dislikeCategory(token, id, method) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_TAGS + '/' + id + '/dislike',
            method ? 'POST' : 'DELETE', null, new Headers({'Authorization': "Bearer " + token}), false
        );
    }

    static async getMyPublishers(token) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.publisher_list : undefined;
    }

    static async getPublishers(token) {
        let json = await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + '?type=all',
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
        return json !== undefined ? json.publisher_list : undefined;
    }

    static async followPublisher(token, id, method) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + '/' + id + '/follow',
            method ? 'POST' : 'DELETE', null, new Headers({'Authorization': "Bearer " + token}), false
        );
    }

    static async dislikePublisher(token, id, method) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_PUBLISHERS + '/' + id + '/dislike',
            method ? 'POST' : 'DELETE', null, new Headers({'Authorization': "Bearer " + token}), false
        );
    }

    static async getIPInfo() {
        let json = await ConnectionManager.doFetch(
            GLOBALS.VPN_DETECT, 'GET', null, null, true
        );
        //console.log(GLOBALS.VPN_DETECT.toString());
        //console.log('The json ', json);
        return json.country;
    }

    static async getAI(token) {
        return await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_USER,
            'GET', null, new Headers({'Authorization': "Bearer " + token}), true
        );
    }

    static async sendMessage(token, name, phone_number, email, message) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_MESSAGE,
            'POST', JSON.stringify({'name': name,'phone_number': phone_number,'email': email,'message': message})
            , new Headers({'Authorization': "Bearer " + token}), false
        );
    }

    static async onBoardingStatus(token, level, star_1, star_2, star_3, star_4, star_5) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_USER + 'on-boarding-status',
            'POST', JSON.stringify({'level': level,'star_1': star_1,'star_2': star_2,'star_3': star_3,
                    'star_4': star_4, 'star_5': star_5})
            , new Headers({'Authorization': "Bearer " + token}), false
        );
    }

    static async sendVideoClick(token, postID, time) {
        await ConnectionManager.doFetch(
            GLOBALS.BASE_URL + GLOBALS.URL_POSTS + "/" + postID + "/video-click/",
            'POST', JSON.stringify({'time': time})
            , new Headers({'Authorization': "Bearer " + token}), false
        );
    }
}
