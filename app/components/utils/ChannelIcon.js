import React from 'react';
import {Animated, Image, Text, TouchableWithoutFeedback, View} from 'react-native';
import {RequestsController} from "./RequestsController";

export default class ChannelIcon extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            scaleAdd: new Animated.Value(0),
            scaleRemove: new Animated.Value(0),
            isFollowed: props.item.state === 2,
            isDisliked: props.item.state === 1
        };

        this.animAdd = this.state.scaleAdd.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animRemove = this.state.scaleRemove.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
    }

    stateIsChanged(state) {
        this.setState({
            isFollowed: state === 2,
            isDisliked: state === 1
        });
    }

    render() {
        let colorTint = !this.state.isDisliked ? "#CACACA" : "#F5A623";
        let colorDark = !this.state.isFollowed ? "#CACACA" : "#4A90DF";
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    flex: 1,
                    marginBottom: 5
                }}>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.state.scaleRemove.setValue(0);
                        let copyDis = this.state.isDisliked;
                        let copyFol = this.state.isFollowed;
                        this.state.isFollowed = false;

                        this.setState((prev) => {
                            return {isDisliked: !prev.isDisliked};
                        });

                        Animated.timing(
                            this.state.scaleRemove, {
                                toValue: 1, duration: 500
                            }
                        ).start(() => {
                                this.props.callbackDislikePublisher(this.props.item.id, !copyDis, copyFol);
                                if (!copyDis && copyFol)
                                    RequestsController.followPublisher(this.state.token, this.props.item.id, false);
                            }
                        );
                    }}>
                    <Animated.Image
                        source={require('../img/ic_del.png')}
                        style={{
                            width: 28,
                            height: 28,
                            marginLeft: 15,
                            tintColor: colorTint,
                            transform: [{scale: this.animRemove}]
                        }}/>

                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.state.scaleAdd.setValue(0);
                        let copyDis = this.state.isDisliked;
                        let copyFol = this.state.isFollowed;
                        this.state.isDisliked = false;

                        this.setState((prev) => {
                            return {isFollowed: !prev.isFollowed};
                        });
                        Animated.timing(
                            this.state.scaleAdd, {
                                toValue: 1, duration: 500
                            }
                        ).start(() => {
                            this.props.callbackFollowPublisher(this.props.item.id, !copyFol, copyDis);
                            if (!copyFol && copyDis)
                                RequestsController.dislikePublisher(this.state.token, this.props.item.id, false);
                        });
                    }}>

                    <Animated.Image
                        source={require('../img/ic_add.png')}
                        style={{
                            width: 28,
                            height: 28,
                            marginLeft: 8,
                            tintColor: colorDark,
                            transform: [{scale: this.animAdd}]
                        }}/>

                </TouchableWithoutFeedback>
                <View style={{flex: 1}}/>

                <TouchableWithoutFeedback
                    onPress={() => this.props.callbackViewChannel(this.props.item.id)}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginStart: 15,
                            marginEnd: 15
                        }}>
                        <Text
                            style={{
                                marginEnd: 7,
                                color: "#4A4A4A",
                                fontFamily: 'byekan'
                            }}>
                            {this.props.item.title.substring(0, 25)}
                        </Text>
                        <View
                            style={{
                                width: 42,
                                height: 42,
                                borderRadius: 21,
                                borderWidth: 1,
                                borderColor: "#CACACA",
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>

                            <Image
                                source={{uri: this.props.item.avatar_url}}
                                style={{
                                    width: 35,
                                    height: 35,
                                    borderRadius: 16
                                }}/>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}