export default {
    BASE_URL: "https://api.in-fact.ir",
    //BASE_URL: "http://164.138.18.61:7000",
    URL_REGISTER: '/register/',
    URL_POSTS: '/posts',
    URL_USER: '/user/',
    URL_SAVED: '/user/saved',
    URL_HISTORY: '/user/history',
    URL_LIKED: '/user/up-voted',
    URL_RESET_AI: '/user/reset-ai',
    URL_MESSAGE: '/user/message',
    URL_PUBLISHERS: '/publisher',
    URL_TAGS: '/category',
    VPN_DETECT: 'http://ip-api.com/json'
}