import React from 'react';
import {View, Image, Dimensions, Text, TouchableWithoutFeedback} from 'react-native';
import MainActivity from "../../MainActivity";
import Video from "react-native-video-controls";
import {Grayscale} from 'react-native-color-matrix-image-filters';

export default class AutosizeImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            width: this.maxWidth = Dimensions.get('window').width - 60,
            height: 0,
            isPlaying: false
        };

        //console.log('source ', this.props.src);
        Image.getSize(props.src,
            (width, height) => {
                const ratio = this.state.width / width;
                this.setState({
                    height: height * ratio
                });
            });
    }

    render() {
        return (
            this.state.isPlaying ?
                <View
                    style={{width: this.state.width, height: this.state.height}}>
                    <Video
                        source={{uri: this.props.video}}
                        disableBack={true}
                        //onEnterFullscreen={this.openFullScreen}
                        ref={(ref) => this.refPlayer = ref}/>
                </View>
                :
                <View
                    style={{
                        borderRadius: 15,
                        overflow: 'hidden',
                        borderColor: '#C0C0C0',
                        borderWidth: 1,
                        marginStart: 15,
                        marginEnd: 15
                    }}>
                    {this.props.grayState ?
                        <Grayscale>
                            <Image
                                style={{
                                    width: this.state.width,
                                    height: this.state.height
                                }}
                                source={{uri: this.props.src}}
                            />
                        </Grayscale> :
                        <Image
                            style={{
                                width: this.state.width,
                                height: this.state.height
                            }}
                            source={{uri: this.props.src}}
                        />}
                    {this.props.video !== undefined ?
                        <View style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    MainActivity.sendEvent("Posts", "Click", "playVideo");
                                    this.setState(() => {
                                        return {isPlaying: true};
                                    });
                                }}>
                                <Image source={require('../img/ic_play.png')}/>
                            </TouchableWithoutFeedback>
                        </View>
                        : null}
                    {this.props.description !== undefined && this.props.description !== "" ?
                        <Text
                            style={{
                                fontFamily: 'IRANSansMobile',
                                fontSize: 14,
                                paddingStart: 15,
                                paddingEnd: 15,
                                paddingTop: 5,
                                paddingBottom: 5
                            }}>
                            {this.props.description}
                        </Text> : null}
                </View>
        );
    }
}