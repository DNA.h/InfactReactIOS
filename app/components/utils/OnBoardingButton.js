import {Text, TouchableWithoutFeedback, View} from "react-native";
import React from "react";

export default class OnBoardingButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            enabled: false
        }
    }

    changeState(state) {
        this.setState({enabled: state});
    }

    render() {
        let backColor = !this.state.enabled ? '#ece9d8' : '#FFFFFF';
        let textColor = !this.state.enabled ? '#aca899' : '#000000';
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    if (this.state.enabled)
                        this.props.callbackClicked()
                }}>
                <View
                    style={{
                        borderRadius: 26,
                        backgroundColor: backColor,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 10,
                        marginBottom: 20,
                        width: 103,
                        height: 52
                    }}>
                    <Text
                        style={{
                            fontSize: 20,
                            fontFamily: 'IRANSansBold',
                            color: textColor
                        }}>
                        بریم
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}