// @flow
import firebase from 'react-native-firebase';
// Optional flow type
import type { RemoteMessage } from 'react-native-firebase';


export default async (message: RemoteMessage) => {
    // handle your message
    console.log("i received it ", message);
    const notification = new firebase.notifications.Notification()
        .setNotificationId('notificationId')
        .setTitle(message.data.title)
        .setBody(message.data.body)
        .android.setChannelId('channelId')
        .android.setSmallIcon('logo')
        .setData({
            title: message.data.title,
            body: message.data.body,
            thumbnail: message.data.thumbnail,
            notif_type: message.data.notif_type,
            post_id: message.data.post_id,
        });
    firebase.notifications().displayNotification(notification);
    return Promise.resolve();
}