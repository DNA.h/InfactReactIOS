import {Grayscale} from "react-native-color-matrix-image-filters";
import {Image, Text, TouchableWithoutFeedback, View} from "react-native";
import React from "react";

export default class CategoryIcon extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            selected: props.bool
        }
    }

    render() {
        let color = this.props.notOnBoarding !== true ? '#FFFFFF' : '#000000';
        let cross =
            <Image
                style={{
                    width: 40,
                    height: 40,
                    position: 'absolute',
                    right: 5,
                    borderRadius: 20,
                    top: 5
                }}
                source={require('../img/ic_remove.png')}/>;
        let img = (src) =>
            <Image
                style={{
                    height: this.props.height - 40,
                    width: this.props.height - 40,
                    borderRadius: (this.props.height - 40) / 2
                }}
                resizeMode={'cover'}
                source={src}
            />
        ;
        return (
            <TouchableWithoutFeedback
                onPress={
                    () => {
                        this.setState((prev) => {
                            return {selected: !prev.selected}
                        }, () => this.props.callbackClicked(this.props.item, this.state.selected));
                    }
                }>
                <View
                    style={{
                        flex: 1,
                        height: this.props.height,
                        alignItems: 'center'
                    }}>
                    {this.state.selected === true ?
                        <Grayscale>
                            {img(this.props.image)}
                        </Grayscale>
                        :
                        img(this.props.image)
                    }
                    {this.state.selected === true ? cross : null}
                    <Text
                        style={{
                            fontFamily: 'IRANSansMobile',
                            alignSelf: 'center',
                            fontSize: 12,
                            color: color
                        }}>
                        {this.props.caption}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}