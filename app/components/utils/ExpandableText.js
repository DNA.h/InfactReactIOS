import React, {Component} from 'react'
import {StyleSheet, Text, View, Image, TouchableHighlight, Animated} from 'react-native';
import Hyperlink from 'react-native-hyperlink';

class ExpandableText extends React.Component {
    constructor(props) {
        super(props);

        this.up = <Text style={{color: "#CACACA", textAlign: 'center'}}>بیشتر+</Text>;
        this.down = <Text style={{color: "#CACACA", textAlign: 'center'}}>-کمتر</Text>;

        this.state = {
            title: props.title,
            expanded: false,
            animation: new Animated.Value(),
            measuring: true,
            numOfLines: 0,
            maxLines: props.maxLines
        };
    }

    whatIsHeight() {
        if (this.state.animation._value !== undefined)
            return this.state.animation._value;
        else {
            if (this.state.minHeight !== undefined)
                return (this.state.baseHeight + this.state.minHeight);
            else
                return (this.state.maxHeight);
        }
    }

    toggle() {
        let initialValue = !this.state.expanded ? this.state.baseHeight + this.state.minHeight :
            this.state.baseHeight + this.state.maxHeight,
            finalValue = !this.state.expanded ? this.state.baseHeight + this.state.maxHeight :
                this.state.baseHeight + this.state.minHeight;
        this.setState({
            expanded: !this.state.expanded,
            numOfLines: this.state.numOfLines === 0 ? this.state.maxLines : 0
        });

        this.state.animation.setValue(initialValue);
        Animated.timing(
            this.state.animation, {
                toValue: finalValue,
                duration: 500
            }
        ).start();
    }

    _setMaxHeight(event) {
        if (this.state.measuring)
            this.setState({
                maxHeight: event.nativeEvent.layout.height,
                measuring: false,
                numOfLines: this.state.maxLines
            });
        else if (this.state.measuring !== undefined) {
            this.setState({
                minHeight: event.nativeEvent.layout.height,
                measuring: undefined
            });
        }
    }

    _setMinHeight(event) {
        this.setState({
            baseHeight: event.nativeEvent.layout.height
        });
    }

    render() {
        let icon = this.up;

        if (this.state.expanded) {
            icon = this.down;
        }
        if (this.state.minHeight === undefined)
            icon = null;
        let toggleArea = this.state.minHeight !== undefined ?
            <View style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                <TouchableHighlight
                    style={styles.button}
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f1">
                    {icon}
                </TouchableHighlight>
            </View> : null;

        return (
            <Animated.View
                style={[this.props.style, styles.container, {height: this.state.animation}]}>
                <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
                    <Hyperlink linkDefault={true} linkStyle={{color: '#2980b9'}}>
                        <Text style={[this.props.textStyle, {fontFamily: 'IRANSansMobile', textAlign: 'right'}]}
                              numberOfLines={this.state.numOfLines}>
                            {this.props.text}
                        </Text>
                    </Hyperlink>
                </View>
                {toggleArea}
            </Animated.View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        overflow: 'hidden'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center', justifyContent: 'center'
    },
    title: {
        flex: 1,
        color: '#2a2f43',
        fontWeight: 'bold',
        textAlignVertical: 'center'
    },
    button: {},
    buttonImage: {
        width: 30,
        height: 25
    },
    body: {
        padding: 10,
        paddingTop: 0
    }
});

export default ExpandableText;