import {AsyncStorage} from "react-native";
import uuid from 'react-native-uuid';
import MainActivity from "../../MainActivity";

export class DBManager {
    constructor() {
        this.state = {
            token: undefined
        };
    }

    static async getUUID() {
        try {
            const value = await AsyncStorage.getItem("uuid");
            if (value !== null) {
                return value;
            }
        } catch (e) {
            console.log(e);
        }
        const id = uuid.v1();
        try {
            await AsyncStorage.setItem("uuid", id);
        } catch (e) {
            console.log("Error saving data" + e);
        }
        return id;
    }

    static async getUserLevel() {
        try {
            let value = await AsyncStorage.getItem('totalSaveds');
            if (value !== null)
                MainActivity.settotalSaveds(parseInt(value));
            else
                MainActivity.settotalSaveds(0);

            value = await AsyncStorage.getItem('totalDislikeds');
            if (value !== null)
                MainActivity.setTotalDislikeds(parseInt(value));
            else
                MainActivity.setTotalDislikeds(0);

            value = await AsyncStorage.getItem('totalImpression');
            if (value !== null)
                MainActivity.settotalImpression(parseInt(value));
            else
                MainActivity.settotalImpression(0);

            value = await AsyncStorage.getItem('totalLikeds');
            if (value !== null)
                MainActivity.settotalLikeds(parseInt(value));
            else
                MainActivity.settotalLikeds(0);

            value = await AsyncStorage.getItem('totalReported');
            if (value !== null)
                MainActivity.settotalReported(parseInt(value));
            else
                MainActivity.settotalReported(0);

            value = await AsyncStorage.getItem('totalSaveds');
            if (value !== null)
                MainActivity.settotalSaveds(parseInt(value));
            else
                MainActivity.settotalSaveds(0);

            value = await AsyncStorage.getItem('totalChannels');
            if (value !== null)
                MainActivity.settotalChannels(parseInt(value));
            else
                MainActivity.settotalChannels(0);

            value = await AsyncStorage.getItem('consecutiveDays');
            if (value !== null)
                MainActivity.setconsecutiveDays(parseInt(value));
            else
                MainActivity.setconsecutiveDays(0);

            value = await AsyncStorage.getItem('star0');
            if (value !== null)
                MainActivity.currentLevelStars[0] = value === "true";
            else
                MainActivity.currentLevelStars[0] = false;

            value = await AsyncStorage.getItem('star1');
            if (value !== null)
                MainActivity.currentLevelStars[1] = value === "true";
            else
                MainActivity.currentLevelStars[1] = false;

            value = await AsyncStorage.getItem('star2');
            if (value !== null)
                MainActivity.currentLevelStars[2] = value === "true";
            else
                MainActivity.currentLevelStars[2] = false;

            value = await AsyncStorage.getItem('star3');
            if (value !== null)
                MainActivity.currentLevelStars[3] = value === "true";
            else
                MainActivity.currentLevelStars[3] = false;

            value = await AsyncStorage.getItem('star4');
            if (value !== null)
                MainActivity.currentLevelStars[4] = value === "true";
            else
                MainActivity.currentLevelStars[4] = false;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    static async setUserLevel(key, value) {
        try {
            await AsyncStorage.setItem(key, value.toString());
        } catch (e) {
            console.log("Error saving data" + e);
        }
    }

    static async getSettingValue(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null)
                return value;
            else
                return null;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    static async saveSettingValue(key, value) {
        try {
            await AsyncStorage.setItem(key, value.toString());
        } catch (e) {
            console.log("Error saving data" + e);
        }
    }

    static async getLaunchState() {
        try {
            const value = await AsyncStorage.getItem("first_run");
            if (value !== null)
                return value;
            else
                return "true";
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    static async setLaunchState(state) {
        try {
            await AsyncStorage.setItem("first_run", state);
        } catch (e) {
            console.log("Error saving data" + e);
        }
    }

    static async getSession() {
        try {
            const value = await AsyncStorage.getItem("session_time");
            if (value !== null)
                return value;
            else
                return 0;
        } catch (e) {
            console.log(e);
            return 0;
        }
    }

    static async saveSession(props) {
        try {
            await AsyncStorage.setItem("session_time", props.time);
        } catch (e) {
            console.log("Error saving data" + e);
        }
    }
}
