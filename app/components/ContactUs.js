import React, {Component} from 'react';
import {View, Text, SafeAreaView, TextInput, TouchableWithoutFeedback, Image} from 'react-native';
import {RequestsController} from "./utils/RequestsController";
import Toast, {Duration} from 'react-native-easy-toast';

export default class ContactUs extends React.Component {

    constructor(props) {
        super(props);
        this.token = this.props.navigation.getParam('token');
        this.state = {
            name: "",
            phone: "",
            email: "",
            message: ""
        };
    }


    render() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    paddingBottom: 15
                }}>
                <View style={{
                    height: 52,
                    backgroundColor: "#F5F4F4",
                    flexDirection: 'row'
                }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.pop()}>
                        <Image source={require("./img/ic_back.png")}
                               style={{
                                   alignSelf: 'flex-start', height: 22, width: 22, resizeMode: 'contain',
                                   marginTop: 15, marginLeft: 25
                               }}/>
                    </TouchableWithoutFeedback>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image source={require("./img/ic_logo.png")}
                               style={{
                                   height: 32, width: 32, resizeMode: 'contain'
                               }}/>
                    </View>
                </View>
                <View style={{
                    backgroundColor: "#F0F0F0", borderRadius: 10, marginLeft: 20, marginRight: 20, marginTop: 15,
                    paddingStart: 10, paddingEnd: 10, flex: 1, marginBottom: 10
                }}>
                    <TextInput style={{
                        marginTop: 25, paddingRight: 5, backgroundColor: "#FFFFFF", borderRadius: 10,
                        fontFamily: 'IRANSansMobile'
                    }}
                               underlineColorAndroid="transparent" placeholder={"نام"} onChangeText={(text) => {
                        this.state.name = text
                    }}/>
                    <TextInput style={{
                        marginTop: 10, paddingRight: 5, backgroundColor: "#FFFFFF", borderRadius: 10,
                        fontFamily: 'IRANSansMobile'
                    }} placeholder={"شماره تماس"} onChangeText={(text) => {
                        this.state.phone = text
                    }}/>
                    <TextInput style={{
                        marginTop: 10, paddingRight: 5, backgroundColor: "#FFFFFF", borderRadius: 10,
                        fontFamily: 'IRANSansMobile'
                    }} placeholder={"ایمیل"} onChangeText={(text) => {
                        this.state.email = text
                    }}/>
                    <TextInput style={{
                        marginTop: 10, paddingRight: 5, backgroundColor: "#FFFFFF", borderRadius: 10,
                        fontFamily: 'IRANSansMobile', flex: 1, marginBottom: 10
                    }} placeholder={"پیام"} onChangeText={(text) => {
                        this.state.message = text
                    }}/>
                    <TouchableWithoutFeedback onPress={() => {
                        let reg = "/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/";
                        //let result = reg.test(this.email);
                        if (this.state.name === "" || this.state.phone === "" || this.state.message === "")
                            this.refs.toast.show('لطفا ورودی های خود را چک نمایید.', 1200);
                        else {
                            RequestsController.sendMessage(this.token, this.state.name, this.state.phone,
                                this.state.email, this.state.message);
                            this.refs.toast.show('با تشکر از همکاری شما. پیام شما برای ادمین فرستاده شد.', 1200);
                        }
                    }}>
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    borderWidth: 1,
                                    borderRadius: 15,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    borderColor: "#BD10E0",
                                    width: 150,
                                    textAlign: 'center'
                                }}>
                                ارسال
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <Toast ref={"toast"}/>
                </View>
            </SafeAreaView>);
    }
}