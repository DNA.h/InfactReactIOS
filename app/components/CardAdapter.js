"use strict";
import React, {PureComponent} from 'react';
import {
    View, Text, Image, Dimensions, TouchableWithoutFeedback,
    Share, Linking, Clipboard, Animated
} from 'react-native';
import ImageLoad from 'react-native-image-placeholder';
import Video from 'react-native-video-controls';
import {RequestsController} from "./utils/RequestsController";
import ExpandableText from "./utils/ExpandableText";
import Toast, {Duration} from "react-native-easy-toast";
import MainActivity from "../MainActivity";
import {Grayscale} from 'react-native-color-matrix-image-filters';
import {DBManager} from "./utils/DBManager";
import {ImageCacheManager} from "react-native-cached-image";

const DOUBLE_PRESS_DELAY = 300;

export class CardAdapter extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            width: props.item.item.width,
            height: props.item.item.height,
            channel_id: props.item.item.channel_id,
            channel_url: props.item.item.channel_avatar,
            channel_name: props.item.item.channel_name,
            channel_title: props.item.item.channel_title,
            channel_type: props.item.item.channel_type,
            date_time: props.item.item.date_time,
            duration: props.item.item.duration,
            downVoteCount: props.item.item.down_vote,
            isDownVoteByMe: props.item.item.down_voted === 1,
            isSavedByMe: props.item.item.saved === 1,
            isUpVotedByMe: props.item.item.up_voted === 1,
            isSharedByMe: false,
            isReportedByMe: false,
            media_url: props.item.item.media_url,
            message: props.item.item.message,
            post_id: props.item.item.post_id,
            post_number: props.item.item.post_number,
            post_type: props.item.item.post_type,
            post_url: props.item.item.post_url,
            shareCount: props.item.item.share,
            thumbnail: props.item.item.thumbnail,
            upVoteCount: props.item.item.up_vote,
            views: props.item.item.views,

            cachedVideoURI: undefined,
            isPlaying: false,
            isFollowed: props.item.item.channel_type,
            maxHeight: new Animated.Value(),
            expandableHeight: 0,
            viewHeight: 0,
            rendering: true,
            scaleSaved: new Animated.Value(0),
            scaleUpVote: new Animated.Value(0),
            scaleDownVote: new Animated.Value(0),
            scaleShare: new Animated.Value(0),
            scaleReport: new Animated.Value(0),
            scaleAdd: new Animated.Value(0),
            scaleRemove: new Animated.Value(0)
        };

        this.lastImagePress = -1;
        this.refPlayer = null;
        this.timePlayed = 0;
        this.maxWidth = 0;
        this.isImpressionSent = 0;
        this.callbackFunc = props.callbackFullScreen;
        this.openFullScreen = this.openFullScreen.bind(this);
        this.handleImagePress = this.handleImagePress.bind(this);
        this.itemLeftOut = this.itemLeftOut.bind(this);
        this._handleLayout = this._handleLayout.bind(this);
        this.getImpressionSent = this.getImpressionSent.bind(this);

        this.animSaved = this.state.scaleSaved.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animUpVote = this.state.scaleUpVote.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.2, 1]
        });
        this.animDownVote = this.state.scaleDownVote.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.2, 1]
        });
        this.animShare = this.state.scaleShare.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animReport = this.state.scaleReport.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animAdd = this.state.scaleAdd.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animRemove = this.state.scaleRemove.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
    }

    componentDidMount() {
        if (this.state.post_type !== 1) {
            if (this.state.post_type === 2) {
                let srcWidth = this.state.width;
                let srcHeight = this.state.height;
                this.maxWidth = Dimensions.get('window').width - 30;

                const ratio = this.maxWidth / srcWidth;
                this.setState({width: this.maxWidth, height: srcHeight * ratio});
            } else {
                let srcWidth = this.state.width;
                let srcHeight = this.state.height;
                this.maxWidth = Dimensions.get('window').width - 30;

                const ratio = this.maxWidth / srcWidth;
                if (srcHeight * ratio < 300)
                    this.setState({width: this.maxWidth, height: srcHeight * ratio});
                else {
                    let w = this.maxWidth / ((srcHeight * ratio) / 300);
                    this.setState({width: w, height: 300});
                }
            }
        } else {
            this.maxWidth = Dimensions.get('window').width - 30;
            const max = Dimensions.get('window').height;
            this.setState({width: this.maxWidth, height: max});
        }
    }

    render() {
        //console.log("rendering ", this.state.post_url);
        if (this.state.post_type === 2)
            return (this.renderImage());
        if (this.state.post_type === 1)
            return (this.renderText());
        if (this.state.post_type === 3)
            return (this.renderVideo());
        if (this.state.post_type === 4)
            return (this.renderGif());
        if (this.state.post_type === 5)
            return (this.renderBeytoote());
        if (this.state.post_type === 6)
            return (this.renderVPN());
        return (
            <View>
                <Text>TEST</Text>
            </View>
        );
    }

    itemLeftOut() {
        if (this.timePlayed > 0)
            RequestsController.sendVideoClick(MainActivity.getToken(), this.state.post_id, this.timePlayed);
        this.setState(() => {
            return ({isPlaying: false});
        });
    }

    openFullScreen() {
        this.setState(() => {
                return ({isPlaying: false})
            }
        );
        this.callbackFunc(this.state.media_url, this.state.post_type, this.timePlayed, this.state.post_id);
    }

    getImpressionSent() {
        this.isImpressionSent++;
        return this.isImpressionSent === 1;
    }

    renderChannel() {
        //console.log('state is ', this.state.isFollowed);
        let colorTint = this.state.isDownVoteByMe || this.state.isFollowed !== 1 ? "#CACACA" : "#F5A623";
        let colorDark = this.state.isDownVoteByMe || this.state.isFollowed !== 2 ? "#CACACA" : "#4A90DF";
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    flex: 1,
                    marginBottom: 5
                }}>
                {this.state.channel_type !== 0 ? null :
                    <TouchableWithoutFeedback
                        onPress={() => {
                            this.state.scaleRemove.setValue(0);
                            Animated.timing(
                                this.state.scaleRemove, {
                                    toValue: 1, duration: 500
                                }
                            ).start();
                            this.setState({
                                rendering: false,
                                isPlaying: false
                            });
                            if (this.refs.expandable !== undefined)
                                this.state.maxHeight.setValue(this.state.viewHeight + this.refs.expandable.whatIsHeight());
                            else
                                this.state.maxHeight.setValue(this.state.viewHeight);
                            Animated.timing(
                                this.state.maxHeight, {
                                    toValue: 0,
                                    duration: 500
                                }
                            ).start();
                            if (this.state.isFollowed !== 2)
                                this.props.callbackCheckStat(4, true);
                            RequestsController.dislikePublisher(MainActivity.getToken(), this.state.channel_name, true);
                            if (this.state.isFollowed === 2)
                                RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, false);
                            setTimeout(() => this.props.callbackRemove(this.state.post_id, this.state.channel_name), 500);
                        }}>
                        <Animated.Image source={require('./img/ic_del.png')} style={{
                            width: 28, height: 28
                            , marginLeft: 15, tintColor: colorTint, transform: [{scale: this.animRemove}]
                        }}/>

                    </TouchableWithoutFeedback>
                }
                {this.state.channel_type !== 0 ? null :
                    <TouchableWithoutFeedback
                        onPress={() => {
                            this.state.scaleAdd.setValue(0);
                            Animated.timing(
                                this.state.scaleAdd, {
                                    toValue: 1, duration: 400
                                }
                            ).start();
                            if (this.state.isFollowed !== 2) {
                                RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, true);
                                if (this.state.isFollowed === 1) {//probably never happens
                                    RequestsController.dislikePublisher(MainActivity.getToken(), this.state.channel_name, false);
                                } else {
                                    this.props.callbackCheckStat(4, true);
                                }
                                this.setState({isFollowed: 2});
                            } else {
                                RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, false);
                                this.props.callbackCheckStat(4, false);
                                this.setState({isFollowed: 0});
                            }
                        }}>

                        <Animated.Image
                            source={require('./img/ic_add.png')}
                            style={{
                                width: 28,
                                height: 28,
                                marginLeft: 8,
                                tintColor: colorDark
                                , transform: [{scale: this.animAdd}]
                            }}/>

                    </TouchableWithoutFeedback>
                }
                <View style={{flex: 1}}/>
                <TouchableWithoutFeedback onPress={async () => {
                    MainActivity.sendEvent("Posts", "Click", "channelAvatar");
                    let flag = await Linking.canOpenURL(this.state.post_url);
                    if (flag)
                        Linking.openURL(this.state.post_url);
                    else
                        Linking.openURL('https://' + this.state.post_url);
                }}>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginStart: 15, marginEnd: 15}}>
                        <Text
                            style={{
                                marginEnd: 7,
                                color: "#4A4A4A",
                                fontFamily: 'byekan'
                            }}>
                            {this.state.channel_title.substring(0, 25)}
                        </Text>
                        <View style={{
                            width: 42, height: 42, borderRadius: 21, borderWidth: 1,
                            borderColor: "#CACACA", justifyContent: 'center', alignItems: 'center'
                        }}>
                            {this.state.isDownVoteByMe ?
                                <Grayscale>
                                    <Image source={{uri: this.state.channel_url}}
                                           style={{width: 35, height: 35, borderRadius: 16}}/>
                                </Grayscale> :
                                <Image source={{uri: this.state.channel_url}}
                                       style={{width: 35, height: 35, borderRadius: 16}}/>}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    renderButtons() {
        let saveIcon = this.state.isSavedByMe && !this.state.isDownVoteByMe ? require('./img/ic_save_filled.png') : require('./img/ic_save.png');
        let shareIcon = this.state.isSharedByMe && !this.state.isDownVoteByMe ? require('./img/ic_share_filled.png') : require('./img/ic_share.png');
        let upvoteIcon = this.state.isDownVoteByMe ? require('./img/ic_upvote_disabled.png') :
            this.state.isUpVotedByMe ? require('./img/ic_upvote_filled.png') : require('./img/ic_upvote.png');
        let downvoteIcon = this.state.isDownVoteByMe ? require('./img/ic_downvote_filled.png') : require('./img/ic_downvote.png');
        let reportIcon = this.state.isReportedByMe && !this.state.isDownVoteByMe ? require('./img/ic_report_filled.png') : require('./img/ic_report.png');

        return (
            <View style={{
                height: 46, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around',
                marginTop: -43, marginStart: 16, marginEnd: 16, paddingStart: 20, paddingEnd: 20,
                paddingTop: 0
            }}>
                <View style={{
                    position: 'absolute', top: 4, bottom: 4, borderBottomLeftRadius: 13,
                    right: 0, left: 0, backgroundColor: "#FDFEFF", borderBottomRightRadius: 13
                }}/>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleSaved.setValue(0);
                    Animated.timing(
                        this.state.scaleSaved, {
                            toValue: 1,
                            duration: 400
                        }
                    ).start();
                    this.props.callbackCheckStat(2, false);
                    RequestsController.sendSave(MainActivity.getToken(),
                        this.state.post_id, this.state.isSavedByMe);
                    MainActivity.sendEvent("Posts", "Click", "save");
                    this.props.showToastCallback(!this.state.isSavedByMe ? 'در قسمت \"ذخیره شده\" ذخیره شد' :
                        'از قسمت \"ذخیره شده\" حذف شد', 1200);
                    this.setState((prev) => {
                        return {isSavedByMe: !prev.isSavedByMe};
                    });
                }}>
                    <Animated.Image style={{
                        width: 35, height: 35,
                        transform: [{scale: this.animSaved}]
                    }}
                                    source={saveIcon}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleUpVote.setValue(0);
                    Animated.timing(
                        this.state.scaleUpVote, {
                            toValue: 1, duration: 400
                        }
                    ).start();
                    RequestsController.sendUpVote(MainActivity.getToken(),
                        this.state.post_id, this.state.isUpVotedByMe);
                    if (this.state.isDownVoteByMe) {
                        RequestsController.sendDownVote(MainActivity.getToken(),
                            this.state.post_id, true);
                        this.state.isDownVoteByMe = false;
                        this.state.downVoteCount--;
                    } else {
                        this.props.callbackCheckStat(0, this.state.isUpVotedByMe);
                    }
                    MainActivity.sendEvent("Posts", "Click", "upvote");
                    this.setState((prev) => {
                        return {isUpVotedByMe: !prev.isUpVotedByMe};
                    });
                }}>
                    {<Animated.Image style={{
                        width: 55, height: 55,
                        transform: [{scale: this.animUpVote}]
                    }}
                                     source={upvoteIcon}/>}
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleShare.setValue(0);
                    this.setState(() => {
                        return {isSharedByMe: true};
                    });
                    Animated.timing(
                        this.state.scaleShare, {
                            toValue: 1, duration: 400
                        }
                    ).start();
                    this.props.callbackCheckStat(2, this.state.isSavedByMe);
                    DBManager.setUserLevel('totalSaveds',
                        this.state.isSavedByMe ? MainActivity.totalSaveds - 1 : MainActivity.totalSaveds + 1);
                    RequestsController.sendShare(MainActivity.getToken(), this.state.post_id);
                    MainActivity.sendEvent("Posts", "Click", "share");
                    setTimeout(() => Share.share({
                        message: this.state.channel_title + '\n' +
                            this.state.post_url + '\n' +
                            this.state.message
                    }), 400);
                }}>
                    <Animated.Image style={{width: 35, height: 35, transform: [{scale: this.animShare}]}}
                                    source={shareIcon}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleDownVote.setValue(0);
                    Animated.timing(
                        this.state.scaleDownVote, {
                            toValue: 1,
                            duration: 400
                        }
                    ).start();
                    RequestsController.sendDownVote(MainActivity.getToken(),
                        this.state.post_id, this.state.isDownVoteByMe);
                    if (this.state.isUpVotedByMe) {
                        RequestsController.sendUpVote(MainActivity.getToken(),
                            this.state.post_id, true);
                        this.state.isUpVotedByMe = false;
                        this.state.upVoteCount--;
                    } else {
                        this.props.callbackCheckStat(1, this.state.isDownVoteByMe);
                    }
                    MainActivity.sendEvent("Posts", "Click", "downvote");
                    this.setState((prev) => {
                        return {isDownVoteByMe: !prev.isDownVoteByMe};
                    });
                }}>
                    {<Animated.Image style={{
                        width: 55, height: 55,
                        transform: [{scale: this.animDownVote}]
                    }}
                                     source={downvoteIcon}/>}
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleReport.setValue(0);
                    this.setState(() => {
                        return {isReportedByMe: true};
                    });
                    Animated.timing(
                        this.state.scaleReport, {
                            toValue: 1,
                            duration: 400
                        }
                    ).start();
                    DBManager.setUserLevel('totalReported',
                        this.state.isReportedByMe ? MainActivity.totalReported - 1 : MainActivity.totalReported + 1);
                    setTimeout(() => this.props.callbackReport(this.state.post_id), 400);
                }}>
                    <Animated.Image style={{width: 35, height: 35, transform: [{scale: this.animReport}]}}
                                    source={reportIcon}/>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    renderDateTime() {
        const momentDate = new Date(this.state.date_time);
        const jalaali = require("jalaali-js");
        let jalali = jalaali.toJalaali(momentDate);
        let viewCount = this.state.views > 1000000 ? (this.state.views / 1000000).toFixed(1) + " M" : (
            this.state.views > 1000 ? (this.state.views / 1000).toFixed(1) + " K" : this.state.views);
        return (
            <View
                style={{
                    width: 160,
                    position: 'absolute',
                    bottom: 5,
                    left: 26,
                    borderWidth: 1,
                    borderColor: "#9B9B9B",
                    borderRadius: 3,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}>
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        right: 0,
                        left: 0,
                        backgroundColor: "#FFFFFF",
                        opacity: 0.7
                    }}/>
                <Image
                    source={require('./img/ic_seen.png')}
                    resizeMode={'contain'}
                    style={{
                        marginLeft: 4,
                        tintColor: "#CACACA"
                    }}/>
                <Text style={{fontFamily: 'IRANSansMobile', color: "#9B9B9B", marginLeft: 5}}>
                    {viewCount}
                </Text>
                <View style={{flex: 1}}/>
                <Text style={{fontFamily: 'IRANSansMobile', color: "#9B9B9B", marginRight: 4}}>
                    {jalali.jy % 100 + "/" +
                    (jalali.jm >= 10 ? jalali.jm : "0" + jalali.jm) + "/" +
                    (jalali.jd >= 10 ? jalali.jd : "0" + jalali.jd)}
                </Text>
            </View>
        );
    }

    renderBeytoote() {
        return (
            <Animated.View
                style={{height: this.state.maxHeight}}
                onLayout={this._handleLayout}>
                <View
                    style={[{
                        marginTop: 10,
                        marginBottom: 5
                    }]}>
                    <View>
                        {this.renderChannel()}
                        <View
                            style={{
                                borderRadius: 15,
                                borderColor: "#CCC",
                                borderWidth: 1,
                                overflow: 'hidden',
                                marginStart: 15,
                                marginEnd: 15,
                                paddingBottom: 52
                            }}>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this.props.callbackSingleItem(this.state.post_id);
                                }}>
                                <View style={{flexDirection: 'row'}}>
                                    {this.state.isDownVoteByMe ?
                                        <View
                                            style={{
                                                borderRadius: 10,
                                                overflow: 'hidden'
                                            }}>
                                            <Grayscale>
                                                <Image
                                                    source={{uri: this.state.thumbnail}}
                                                    resizeMode={'contain'}
                                                    placeholderSource={require('./img/place_holder.png')}
                                                    style={{
                                                        width: 119,
                                                        height: 80,
                                                        margin: 10,
                                                        borderRadius: 10,
                                                        overflow: 'hidden'
                                                    }}/>
                                            </Grayscale>
                                        </View> :
                                        <ImageLoad
                                            source={{uri: this.state.thumbnail}}
                                            resizeMode={'contain'}
                                            isShowActivity={false}
                                            placeholderSource={require('./img/place_holder.png')}
                                            style={{
                                                width: 119,
                                                height: 80,
                                                margin: 10,
                                                borderRadius: 10,
                                                overflow: 'hidden'
                                            }}/>
                                    }
                                    <View
                                        style={{marginTop: 5, paddingStart: 10, paddingEnd: 10, flex: 1}}>
                                        <Text style={{fontFamily: 'IRANSansBold', fontSize: 10}}>
                                            {this.props.item.item.title}
                                        </Text>
                                        <Text
                                            style={{
                                                fontFamily: 'IRANSansMobile',
                                                fontSize: 10
                                            }}
                                            numberOfLines={4}>
                                            {this.state.message}
                                        </Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    {this.renderButtons()}
                </View>
            </Animated.View>
        );
    }

    renderText() {
        return (
            <Animated.View style={[{height: this.state.maxHeight}]}
                           onLayout={this._handleLayout}>
                <View style={{
                    marginTop: 15, marginBottom: 5
                }}>
                    <View>
                        {this.renderChannel()}
                        <View
                            style={{
                                borderRadius: 15,
                                borderColor: "#CCC",
                                borderWidth: 1,
                                paddingBottom: 52,
                                marginStart: 15,
                                marginEnd: 15,
                                backgroundColor: "#FFFFFF"
                            }}>
                            <TouchableWithoutFeedback
                                onPress={this.handleImagePress}
                                onLongPress={() => {
                                    Clipboard.setString(this.state.message);
                                    this.props.showToastCallback('متن پست کپی شد', 1200);
                                }}>
                                <View>
                                    <ExpandableText
                                        ref={'expandable'}
                                        style={{
                                            flex: 1,
                                            marginStart: 16,
                                            marginEnd: 16,
                                            marginTop: 10,
                                            marginBottom: 35
                                        }}
                                        textStyle={{color: "#4A4A4A"}}
                                        text={this.state.message}
                                        maxLines={6}/>
                                    {this.renderDateTime()}
                                </View>
                            </TouchableWithoutFeedback>

                        </View>
                        <Toast ref={"toast"}/>
                    </View>
                    {this.renderButtons()}
                </View>
            </Animated.View>
        );
    }

    _handleLayout(event) {
        if (this.state.rendering) {
            this.setState({
                viewHeight: event.nativeEvent.layout.height
            });
        }
    }


    renderImage() {
        return (
            <Animated.View
                style={[{height: this.state.maxHeight}]}
                onLayout={this._handleLayout}>
                <View
                    style={[{
                        marginTop: 10, marginBottom: 5
                    }]}>
                    <View>
                        {this.renderChannel()}
                        <View
                            style={{
                                borderRadius: 15,
                                borderColor: "#CCC",
                                borderWidth: 1,
                                overflow: 'hidden',
                                marginStart: 15,
                                marginEnd: 15,
                                paddingBottom: 52
                            }}>
                            <TouchableWithoutFeedback onPress={this.handleImagePress}>
                                <View>
                                    <View>
                                        {this.state.isDownVoteByMe ?
                                            <Grayscale>
                                                <ImageLoad
                                                    source={{uri: this.state.media_url}}
                                                    resizeMode={'contain'}
                                                    isShowActivity={false}
                                                    placeholderSource={require('./img/place_holder.png')}
                                                    style={{
                                                        width: this.state.width,
                                                        height: this.state.height
                                                    }}/>
                                            </Grayscale> :
                                            <ImageLoad
                                                source={{uri: this.state.media_url}}
                                                resizeMode={'contain'}
                                                isShowActivity={false}
                                                placeholderSource={require('./img/place_holder.png')}
                                                style={{
                                                    width: this.state.width,
                                                    height: this.state.height
                                                }}/>
                                        }

                                    </View>
                                    <TouchableWithoutFeedback onLongPress={
                                        () => {
                                            Clipboard.setString(this.state.message);
                                            this.props.showToastCallback('متن پست کپی شد', 1200);
                                        }
                                    }>
                                        <View>
                                            <ExpandableText
                                                style={{
                                                    flex: 1,
                                                    paddingRight: 5,
                                                    marginLeft: 5,
                                                    marginTop: 10,
                                                    marginBottom: 35
                                                }}
                                                textStyle={{color: "#4A4A4A"}}
                                                text={this.state.message}
                                                ref={"expandable"}
                                                maxLines={4}/>
                                            {this.renderDateTime()}
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    {this.renderButtons()}
                </View>
            </Animated.View>
        );
    }

    renderVideo() {
        let thumbnail = this.state.isDownVoteByMe ?
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center',
                backgroundColor: "#000000"
            }}>
                <Grayscale>
                    <ImageLoad
                        source={{uri: this.state.thumbnail}}
                        resizeMode={'cover'}
                        isShowActivity={false}
                        placeholderSource={require('./img/place_holder.png')}
                        style={{
                            width: this.state.width,
                            height: this.state.height
                        }}>
                        <TouchableWithoutFeedback onPress={() => {
                            MainActivity.sendEvent("Posts", "Click", "playVideo");
                            this.setState(
                                () => {
                                    return {isPlaying: true};
                                });
                        }}>
                            <View style={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                right: 0,
                                bottom: 0,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Image source={require('./img/ic_play.png')}/>
                            </View>
                        </TouchableWithoutFeedback>
                    </ImageLoad>
                </Grayscale>
            </View> :
            <View style={{
                flexDirection: 'row', justifyContent: 'center',
                backgroundColor: "#000000"
            }}>
                <ImageLoad
                    source={{uri: this.state.thumbnail}}
                    resizeMode={'cover'}
                    isShowActivity={false}
                    placeholderSource={require('./img/place_holder.png')}
                    style={{
                        width: this.state.width, height: this.state.height
                    }}>
                    <TouchableWithoutFeedback onPress={() => {
                        MainActivity.sendEvent("Posts", "Click", "playVideo");
                        ImageCacheManager({})
                            .downloadAndCacheUrl(this.state.media_url)
                            .then(res => {
                                this.setState({cachedVideoURI: res, isPlaying: true});
                            })
                            .catch(err => {
                                console.log('error ', err);
                            });
                    }}>
                        <View style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Image source={require('./img/ic_play.png')}/>
                        </View>
                    </TouchableWithoutFeedback>
                </ImageLoad>
            </View>;
        let player = <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Video
                source={{uri: this.state.cachedVideoURI}}
                onProgress={(time) => {
                    this.timePlayed = Math.floor(time.currentTime);
                }}
                disableBack={true}
                style={{
                    width: this.state.width, height: this.state.height
                }}
                onEnterFullscreen={this.openFullScreen}
                ref={(ref) => this.refPlayer = ref}/>
        </View>;
        let minute = Math.floor(this.state.duration / 60);
        let second = this.state.duration % 60;
        return (
            <Animated.View
                style={[{height: this.state.maxHeight}]}
                onLayout={this._handleLayout}>
                <View
                    style={{
                        marginTop: 15, marginBottom: 5
                    }}>
                    <View>
                        {this.renderChannel()}
                        <View
                            style={{
                                borderRadius: 15,
                                borderColor: "#CCC",
                                borderWidth: 1,
                                overflow: 'hidden',
                                marginStart: 15,
                                marginEnd: 15,
                                paddingBottom: 52
                            }}>
                            <TouchableWithoutFeedback onPress={this.handleImagePress}>
                                <View>
                                    <View>
                                        {!this.state.isPlaying ? thumbnail : player}
                                        {!this.state.isPlaying ?
                                            <View style={{
                                                position: 'absolute',
                                                top: 10,
                                                right: 20,
                                                borderWidth: 1,
                                                borderColor: "#FFFFFF",
                                                borderRadius: 3,
                                                alignItems: 'center'
                                            }}>
                                                <View
                                                    style={{
                                                        position: 'absolute',
                                                        top: 0,
                                                        bottom: 0,
                                                        right: 0,
                                                        left: 0,
                                                        backgroundColor: "#4A4A4A",
                                                        opacity: 0.7
                                                    }}/>
                                                <Text
                                                    style={{
                                                        fontFamily: 'IRANSansMobile',
                                                        color: "#FFFFFF",
                                                        marginLeft: 5,
                                                        marginRight: 5
                                                    }}>
                                                    {(minute >= 10 ? minute : "0" + minute) + ":" +
                                                    (second >= 10 ? second : "0" + second)}
                                                </Text>
                                            </View> : null}
                                    </View>
                                    <TouchableWithoutFeedback onLongPress={
                                        () => {
                                            Clipboard.setString(this.state.message);
                                            this.props.showToastCallback('متن پست کپی شد', 1200);
                                        }
                                    }>
                                        <View>
                                            <ExpandableText
                                                style={{
                                                    flex: 1,
                                                    marginBottom: 35
                                                }}
                                                textStyle={{
                                                    marginStart: 15,
                                                    marginEnd: 15,
                                                    marginTop: 10,
                                                    color: "#4A4A4A"
                                                }}
                                                ref={'expandable'}
                                                text={this.state.message}
                                                maxLines={4}/>
                                            {!this.state.isPlaying ? this.renderDateTime() : null}
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    {this.renderButtons()}
                </View>
            </Animated.View>
        );
    }

    renderGif() {
        let thumbnail = this.state.isDownVoteByMe ?
            <View style={{
                flexDirection: 'row', justifyContent: 'center',
                backgroundColor: "#000000"
            }}>
                <Grayscale>
                    <ImageLoad
                        source={{uri: this.state.thumbnail}}
                        resizeMode={'cover'}
                        isShowActivity={false}
                        placeholderSource={require('./img/place_holder.png')}
                        style={{
                            width: this.state.width, height: this.state.height
                        }}>
                        <TouchableWithoutFeedback onPress={() => {
                            MainActivity.sendEvent("Posts", "Click", "playVideo");
                            this.setState(
                                () => {
                                    return {isPlaying: true};
                                });
                        }}>
                            <View style={{
                                position: 'absolute', top: 0, left: 0, right: 0,
                                bottom: 0, justifyContent: 'center', alignItems: 'center'
                            }}>
                                <Image source={require('./img/ic_gif.png')}/>
                            </View>
                        </TouchableWithoutFeedback>
                    </ImageLoad></Grayscale>
            </View> :
            <View style={{
                flexDirection: 'row', justifyContent: 'center',
                backgroundColor: "#000000"
            }}><ImageLoad
                source={{uri: this.state.thumbnail}}
                resizeMode={'cover'}
                isShowActivity={false}
                placeholderSource={require('./img/place_holder.png')}
                style={{
                    width: this.state.width,
                    height: this.state.height
                }}>
                <TouchableWithoutFeedback onPress={() => {
                    MainActivity.sendEvent("Posts", "Click", "playVideo");
                    this.setState(
                        () => {
                            return {isPlaying: true};
                        });
                }}><View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image source={require('./img/ic_gif.png')}/>
                </View>
                </TouchableWithoutFeedback>
            </ImageLoad></View>;
        let player = <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Video
                source={{uri: this.state.media_url}}
                selectedAudioTrack={{type: "disabled"}}
                repeat={true}
                disableBack={true}
                disableVolume={true}
                style={{
                    flex: 1, width: this.state.width, height: this.state.height,
                    backgroundColor: "#000000"
                }}
                onEnterFullscreen={this.openFullScreen}
                ref={(ref) => this.refPlayer = ref}/>
        </View>;
        let minute = Math.floor(this.state.duration / 60);
        let second = this.state.duration % 60;
        return (
            <Animated.View style={[{height: this.state.maxHeight}]}
                           onLayout={this._handleLayout}>
                <View style={{
                    marginTop: 15, marginBottom: 5
                }}>
                    <View>
                        {this.renderChannel()}
                        <View
                            style={{
                                borderRadius: 15,
                                borderColor: "#CCC",
                                borderWidth: 1,
                                overflow: 'hidden',
                                marginStart: 15,
                                marginEnd: 15,
                                paddingBottom: 52
                            }}>
                            <TouchableWithoutFeedback onPress={this.handleImagePress}>
                                <View>
                                    <View>
                                        {!this.state.isPlaying ? thumbnail : player}

                                        {!this.state.isPlaying ?
                                            <View style={{
                                                position: 'absolute',
                                                top: 10,
                                                right: 20,
                                                borderWidth: 1,
                                                borderColor: "#FFFFFF",
                                                borderRadius: 3,
                                                alignItems: 'center'
                                            }}>
                                                <View
                                                    style={{
                                                        position: 'absolute',
                                                        top: 0,
                                                        bottom: 0,
                                                        right: 0,
                                                        left: 0,
                                                        backgroundColor: "#4A4A4A",
                                                        opacity: 0.7
                                                    }}/>
                                                <Text
                                                    style={{
                                                        fontFamily: 'IRANSansMobile',
                                                        color: "#FFFFFF",
                                                        marginLeft: 5,
                                                        marginRight: 5
                                                    }}>
                                                    {(minute >= 10 ? minute : "0" + minute) + ":" +
                                                    (second >= 10 ? second : "0" + second)}
                                                </Text>
                                            </View> : null}
                                    </View>
                                    <TouchableWithoutFeedback
                                        onLongPress={() => {
                                            Clipboard.setString(this.state.message);
                                            this.props.showToastCallback('متن پست کپی شد', 1200);
                                        }}>
                                        <View>
                                            <ExpandableText
                                                style={{
                                                    flex: 1,
                                                    marginBottom: 35
                                                }}
                                                textStyle={{
                                                    paddingStart: 10,
                                                    marginEnd: 10,
                                                    marginTop: 10,
                                                    color: "#4A4A4A"
                                                }}
                                                text={this.state.message}
                                                maxLines={4}
                                                ref={'expandable'}/>
                                            {!this.state.isPlaying ? this.renderDateTime() : null}
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                    {this.renderButtons()}
                </View>
            </Animated.View>
        );
    }

    renderVPN() {
        return (
            <View style={{
                paddingTop: 10, paddingBottom: 5, backgroundColor: "#810E7C",
                paddingStart: 10, paddingEnd: 10
            }}>
                <View style={{flexDirection: 'row'}}>
                    <Text style={{
                        flex: 1, paddingLeft: 15, paddingRight: 10, fontSize: 13, color: "#FFFFFF"
                        , fontFamily: 'IRANSansMobile', textAlign: 'right'
                    }}>
                        شما در حال استفاده از VPN هستید. برای کسب بهترین تجربه کابری (مانند تاخیر کمتر هنگام بارگذاری
                        پست ها، استفاده نیم بها از اینترنت ملی و غیره) پیشنهاد می شود که از VPN خود را خاموش نمایید.
                    </Text>
                    <Text style={{
                        padding: 5, fontSize: 13, color: "#FFFFFF", fontWeight: 'bold',
                        borderWidth: 1, borderColor: "#FFFFFF", borderRadius: 3, alignSelf: 'center'
                    }}>
                        VPN</Text>
                </View>
                <TouchableWithoutFeedback onPress={() => this.props.closeVPN()}>
                    <View>
                        <Text style={{
                            flex: 1, marginStart: 16, marginEnd: 16, marginTop: 10, textAlign: 'center',
                            paddingBottom: 5, color: "#FFFFFF", fontSize: 13, fontFamily: 'IRANSansMobile'
                        }}>
                            خب
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    handleImagePress(e) {
        const now = new Date().getTime();
        if (this.lastImagePress && (now - this.lastImagePress) < DOUBLE_PRESS_DELAY) {
            this.handleImageDoublePress();
        }
        else {
            this.lastImagePress = now;
        }
    }

    handleImageDoublePress() {
        this.setState({isUpVotedByMe: true});
        this.state.scaleUpVote.setValue(0);
        Animated.timing(
            this.state.scaleUpVote, {
                toValue: 1,
                duration: 400
            }
        ).start();
        RequestsController.sendUpVote(MainActivity.getToken(), this.state.post_id, false);
        if (this.state.isDownVoteByMe) {
            RequestsController.sendDownVote(MainActivity.getToken(),
                this.state.post_id, true);
            this.state.isDownVoteByMe = false;
            this.state.downVoteCount--;
        }
        MainActivity.sendEvent("Posts", "Click", "upvote");
    }
}