import React from 'react';
import {Text, View, TouchableWithoutFeedback, Alert, Image, Switch} from 'react-native';
import {RequestsController} from "./components/utils/RequestsController";
import {DBManager} from "./components/utils/DBManager";
import Modal from "react-native-modal";
import {SafeAreaView} from 'react-navigation';

export default class SettingsActivity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            autoPlayGif: false,
            autoPlayVideo: false,
            showDialog: false
        };
        this.token = props.navigation.getParam('token');
        this.resetAI = this.resetAI.bind(this);
        this.readSettings = this.readSettings.bind(this);

        this.readSettings();
    }

    async readSettings() {
        let apG = await DBManager.getSettingValue("autoPlayGif");
        let apV = await DBManager.getSettingValue("autoPlayVideo");

        this.setState({
            autoPlayGif: apG === null ? false : apG === "true",
            autoPlayVideo: apV === null ? false : apV === "true"
        });
    }

    async resetAI() {
        await RequestsController.resetAI(this.token);
    }

    _toggleModal = () =>
        this.setState({showDialog: !this.state.showDialog});

    render() {
        return (
            <SafeAreaView
                style={{
                    backgroundColor: "#FFFFFF",
                    flex: 1
                }}>
                <View
                    style={{
                        height: 52,
                        backgroundColor: "#F5F4F4",
                        flexDirection: 'row'
                    }}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.navigation.pop()}>
                        <Image
                            source={require("./components/img/ic_back.png")}
                            style={{
                                alignSelf: 'flex-start', height: 22, width: 22, resizeMode: 'contain',
                                marginTop: 15, marginLeft: 25
                            }}/>
                    </TouchableWithoutFeedback>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image source={require("./components/img/ic_logo.png")}
                               style={{
                                   height: 32, width: 32, resizeMode: 'contain'
                               }}/>
                    </View>
                </View>
                <View
                    style={{flexDirection: 'row'}}>
                    <Switch
                        style={{
                            alignSelf: 'center',
                            marginLeft: 20
                        }}
                        value={this.state.autoPlayGif}
                        onValueChange={() => {
                            this.setState(() => {
                                    return {autoPlayGif: !this.state.autoPlayGif}
                                }
                            );
                            DBManager.saveSettingValue("autoPlayGif", !this.state.autoPlayGif);
                        }}/>
                    <Text style={{
                        paddingTop: 10, paddingBottom: 10, textAlign: 'right',
                        marginRight: 25, flex: 1, fontFamily: 'IRANSansMobile'
                    }}>
                        پخش خود کار گیف ها
                    </Text>
                </View>
                <View
                    style={{width: "100%", height: 1, backgroundColor: "#DDD", marginTop: 10, marginBottom: 10}}/>
                <View
                    style={{flexDirection: 'row'}}>
                    <Switch
                        style={{
                            alignSelf: 'center',
                            marginLeft: 20
                        }}
                        value={this.state.autoPlayVideo}
                        onValueChange={() => {
                            this.setState(() => {
                                    return {autoPlayVideo: !this.state.autoPlayVideo};
                                }
                            );
                            DBManager.saveSettingValue("autoPlayVideo", !this.state.autoPlayVideo);
                        }}/>
                    <Text style={{
                        flex: 1, paddingTop: 10, paddingBottom: 10, textAlign: 'right',
                        marginRight: 25, fontFamily: 'IRANSansMobile'
                    }}>
                        پخش خود کار ویدیو ها
                    </Text>
                </View>
                <View style={{width: "100%", height: 1, backgroundColor: "#DDD", marginTop: 10, marginBottom: 10}}/>
                <TouchableWithoutFeedback onPress={this._toggleModal}>
                    <View>
                        <Text
                            style={{
                                paddingTop: 10,
                                paddingBottom: 10,
                                textAlign: 'right',
                                marginRight: 25,
                                fontFamily: 'IRANSansMobile'
                            }}>
                            ریست کردن شناخت هوش مصنوعی
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <Modal isVisible={this.state.showDialog}
                       onBackdropPress={this._toggleModal}>
                    <View
                        style={{
                            backgroundColor: "#FFFFFF",
                            borderRadius: 10,
                            marginStart: 10,
                            marginEnd: 10,
                            paddingStart: 10,
                            paddingEnd: 10,
                            paddingBottom: 5
                        }}>
                        <Text
                            style={{
                                fontFamily: 'IRANSansMobile',
                                fontSize: 18,
                                marginTop: 10
                            }}>
                            با زدن بله هوش مصنوعی شما را به عنوان یک فرد جدید می شناسد و دوباره از ابتدا شروع
                            به شناخت شما میکند. آیا مطمئنید؟
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                margin: 10,
                                justifyContent: 'space-between',
                                paddingEnd: 25,
                                paddingStart: 25
                            }}>
                            <TouchableWithoutFeedback
                                onPress={this._toggleModal}>
                                <View>
                                    <Text
                                        style={{
                                            fontFamily: 'IRANSansMobile',
                                            paddingEnd: 15,
                                            paddingStart: 15,
                                            marginTop: 5,
                                            marginBottom: 5,
                                            fontSize: 17,
                                            color: "#50E3C2",
                                            borderWidth: 1,
                                            borderColor: "#50E3C2",
                                            borderRadius: 10,
                                            marginStart: 15,
                                            marginEnd: 15
                                        }}>
                                        خیر
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this._toggleModal();
                                    this.resetAI();
                                }}>
                                <View>
                                    <Text
                                        style={{
                                            fontFamily: 'IRANSansMobile',
                                            paddingEnd: 15,
                                            paddingStart: 15,
                                            marginTop: 5,
                                            marginBottom: 5,
                                            fontSize: 17,
                                            color: "#D0021B",
                                            borderWidth: 1,
                                            borderColor: "#D0021B",
                                            borderRadius: 10,
                                            marginStart: 15,
                                            marginEnd: 15
                                        }}>
                                        بله
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </Modal>
                <View style={{width: "100%", height: 1, backgroundColor: "#DDD", marginTop: 10, marginBottom: 10}}/>
                <Text style={{
                    paddingTop: 10, paddingBottom: 10, textAlign: 'right',
                    marginRight: 25, fontFamily: 'IRANSansMobile'
                }}>
                    دریافت نوتیفیکیشن
                </Text>
                <View style={{width: "100%", height: 1, backgroundColor: "#DDD", marginTop: 10, marginBottom: 10}}/>
                <Text style={{
                    paddingTop: 10, paddingBottom: 10, textAlign: 'right',
                    marginRight: 25, fontFamily: 'IRANSansMobile'
                }}>
                    پاک کردن کش
                </Text>
                <View style={{width: "100%", height: 1, backgroundColor: "#DDD", marginTop: 10}}/>
            </SafeAreaView>
        );
    }
}