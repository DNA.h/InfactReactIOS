import React, {Component} from 'react';
import {Image, TouchableWithoutFeedback, View} from 'react-native';
import Video from 'react-native-video-controls';
import {RequestsController} from "./components/utils/RequestsController";
import Orientation from 'react-native-orientation';

export default class FullScreenActivity extends React.Component {
    constructor(props) {
        super(props);
        this.videoRef = null;
        this.timePlayed = 0;
    }

    componentDidMount() {
        Orientation.lockToLandscape();
        if (this.videoRef !== null) {
            this.videoRef.seekTo(this.props.navigation.getParam('time'));
        }
    }

    componentWillUnmount() {
        if (this.timePlayed > 0)
            RequestsController.sendVideoClick(this.props.navigation.getParam('token'),
                this.props.navigation.getParam('post_id'),
                this.timePlayed);
        Orientation.lockToPortrait();
    }

    render() {
        let player =
            this.props.navigation.getParam('isVideo') ?
                <Video style={{flex: 1}}
                       ref={ref => {this.videoRef = ref}}
                       source={{uri: this.props.navigation.getParam('media_url')}}
                       disableBack={true}
                       disableFullscreen={true}/> :
                <Video style={{flex: 1}}
                       ref={ref => {this.videoRef = ref}}
                       source={{uri: this.props.navigation.getParam('media_url')}}
                       onProgress={(time) => {
                           this.timePlayed = Math.floor(time.currentTime);
                       }}
                       repeat={true}
                       selectedAudioTrack={{type: "disabled"}}
                       disableBack={true}
                       disableFullscreen={true}/>;
        return (
            <View style={{flex: 1, backgroundColor: "#000000"}}>
                <TouchableWithoutFeedback onPress={() => this.props.navigation.pop()}>
                    <Image source={require("./components/img/ic_back.png")}
                           style={{
                               position: 'absolute', height: 22, width: 22, resizeMode: 'contain',
                               marginTop: 15, marginLeft: 25
                           }}/>
                </TouchableWithoutFeedback>
                {player}
            </View>
        );
    }
}