"use strict";
import React, {Component} from 'react';
import {
    Dimensions, Image, Text, TouchableWithoutFeedback,
    ScrollView, View, Animated, linking, Clipboard, Share, Linking, AppState, BackHandler, FlatList
} from 'react-native';
import ImageLoad from "react-native-image-placeholder";
import {RequestsController} from "./components/utils/RequestsController";
import Video from "react-native-video-controls";
import {SafeAreaView} from 'react-navigation';
import Toast, {Duration} from "react-native-easy-toast";
import Hyperlink from 'react-native-hyperlink';
import MainActivity from "./MainActivity";
import {Grayscale} from "react-native-color-matrix-image-filters";
import AutosizeImage from "./components/utils/AutosizeImage";
import {ImageCacheManager} from "react-native-cached-image";

const DOUBLE_PRESS_DELAY = 300;

export default class SingleItemActivity extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            width: null,
            height: null,
            channel_url: null,
            channel_name: null,
            channel_title: null,
            date_time: null,
            duration: null,
            downVoteCount: null,
            instant_view_sections: [],
            isDownVoteByMe: null,
            isSavedByMe: null,
            isUpVotedByMe: null,
            media_url: null,
            message: null,
            post_type: -1,
            post_number: null,
            post_id: props.navigation.getParam('id'),
            post_url: null,
            shareCount: null,
            thumbnail: null,
            title: undefined,
            upVoteCount: null,
            views: null,

            isPlaying: false,
            isFollowed: 0,
            scaleSaved: new Animated.Value(0),
            scaleUpVote: new Animated.Value(0),
            scaleDownVote: new Animated.Value(0),
            scaleShare: new Animated.Value(0),
            scaleReport: new Animated.Value(0),
            scaleAdd: new Animated.Value(0),
            scaleRemove: new Animated.Value(0),
            maskShow: false,
            maskOp: new Animated.Value(),
            marginBottomAnimated: new Animated.Value(),
            marginBottomSecond: new Animated.Value(),
        };
        this.token = props.navigation.getParam('token');
        this.lastImagePress = -1;
        this.refPlayer = null;
        this.timePlayed = 0;
        this.maxWidth = 0;
        this.handleImagePress = this.handleImagePress.bind(this);
        this.reportChannel = this.reportChannel.bind(this);
        this.handleBackPress = this.handleBackPress.bind(this);
        this.getPost();

        this.animSaved = this.state.scaleSaved.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animUpVote = this.state.scaleUpVote.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animDownVote = this.state.scaleDownVote.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animShare = this.state.scaleShare.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animReport = this.state.scaleReport.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animAdd = this.state.scaleAdd.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
        this.animRemove = this.state.scaleRemove.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [1, 1.3, 1]
        });
    }

    componentDidMount() {
        if (this.state.post_type !== 1) {
            let srcWidth = this.state.width;
            let srcHeight = this.state.height;
            const maxWidth = Dimensions.get('window').width;

            const ratio = maxWidth / srcWidth;
            this.setState({width: maxWidth, height: srcHeight * ratio});
        } else {
            this.maxWidth = Dimensions.get('window').width;
            const maxHeight = Dimensions.get('window').height;

        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
        this.props.navigation.goBack(null);
        return true;
    };

    mask = () => !this.state.maskShow ? null :
        <TouchableWithoutFeedback onPress={() => this.reportChannel(null, null)}>
            <View style={{
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0
            }}>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: "#000000",
                    opacity: this.state.maskOp
                }}/>
                <View style={{flex: 1}}/>
                <View>
                    <Animated.View
                        style={{
                            bottom: this.state.marginBottomSecond,
                            right: 0,
                            left: 0,
                            position: 'absolute'
                        }}>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                this.reportChannel(null, null);
                            }}>
                            <View
                                style={{
                                    backgroundColor: "#FFFFFF",
                                    opacity: 1
                                }}>
                                <View
                                    style={{
                                        lexDirection: 'row',
                                        alignItems: 'center'
                                    }}>
                                    <Image
                                        source={require("./components/img/ic_back.png")}
                                        style={{
                                            alignSelf: 'flex-start',
                                            height: 22,
                                            width: 22,
                                            resizeMode: 'contain',
                                            marginTop: 15,
                                            marginLeft: 25
                                        }}/>
                                    <View style={{flex: 1}}/>
                                    <Text
                                        style={{
                                            fontSize: 18,
                                            color: "#000000",
                                            textAlign: 'right',
                                            padding: 10,
                                            fontFamily: 'IRANSansMobile'
                                        }}>
                                        دلیل گزارش</Text>
                                </View>
                                <View style={{height: 5, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(this.state.token, this.reportPostId, 1);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>محتوای
                                    غیراخلاقی</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(this.state.token, this.reportPostId, 2)
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>تبلیغ</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(this.state.token, this.reportPostId, 3);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>محتوای
                                    کذب</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(this.state.token, this.reportPostId, 4);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>تکراری</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={() => {
                                RequestsController.reportContent(this.state.token, this.reportPostId, 5);
                                this.reportChannel(null, null);
                            }}>
                            <View style={{backgroundColor: "#FFFFFF", opacity: 1}}>
                                <Text style={{
                                    fontSize: 18,
                                    color: "#000000", textAlign: 'right',
                                    padding: 10, fontFamily: 'IRANSansMobile'
                                }}>قدیمی</Text>
                                <View style={{height: 1, backgroundColor: "#CACACA"}}/>
                            </View>
                        </TouchableWithoutFeedback>
                    </Animated.View>
                </View>
            </View>
        </TouchableWithoutFeedback>;

    async getPost() {
        let p = await RequestsController.getSinglePost(this.token, this.state.post_id);
        //console.log('the post ', this.state.post_id);
        if (p.post_type !== 1) {
            let srcWidth = p.width;
            let srcHeight = p.height;
            const maxWidth = Dimensions.get('window').width;

            const ratio = maxWidth / srcWidth;
            this.setState({
                width: maxWidth,
                height: srcHeight * ratio,
                channel_url: p.channel_avatar,
                channel_name: p.channel_name,
                channel_title: p.channel_title,
                isFollowed: p.channel_type,
                date_time: p.date_time,
                duration: p.duration,
                downVoteCount: p.down_vote,
                instant_view_sections: p.hasOwnProperty('instant_view_sections') ? p.instant_view_sections : [],
                isDownVoteByMe: p.down_voted,
                isSavedByMe: p.saved,
                isUpVotedByMe: p.up_voted,
                media_url: p.media_url,
                message: p.message,
                thumbnail: p.thumbnail,
                post_type: p.post_type,
                post_id: p.post_id,
                post_url: p.post_url,
                shareCount: p.share,
                title: p.hasOwnProperty('title') ? p.title : undefined,
                upVoteCount: p.up_vote,
                views: p.views,
            });
        } else
            this.setState({
                width: p.width,
                height: p.height,
                channel_url: p.channel_url,
                channel_name: p.channel_name,
                channel_title: p.channel_title,
                isFollowed: p.channel_type,
                downVoteCount: p.down_vote,
                isDownVoteByMe: p.down_voted,
                isSavedByMe: p.saved,
                isUpVotedByMe: p.up_voted,
                media_url: p.media_url,
                message: p.message,
                thumbnail: p.thumbnail,
                post_type: p.post_type,
                post_id: p.post_id,
                upVoteCount: p.up_vote,
                title: undefined
            });
    }

    render() {
        if (this.state.post_type === 2)
            return (this.renderImage());
        if (this.state.post_type === 1)
            return (this.renderText());
        if (this.state.post_type === 3)
            return (this.renderVideo());
        if (this.state.post_type === 4)
            return (this.renderGif());
        if (this.state.post_type === 5)
            return (this.renderBeytoote());
        return (
            <View>

            </View>
        );
    }

    renderChannel() {
        let colorTint = this.state.isDownVoteByMe || this.state.isFollowed !== 1 ? "#CACACA" : "#F5A623";
        let colorDark = this.state.isDownVoteByMe || this.state.isFollowed !== 2 ? "#707070" : "#4A90DF";
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 5,
                    marginTop: 10
                }}>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleRemove.setValue(0);
                    if (this.state.isFollowed === 0) {
                        RequestsController.dislikePublisher(MainActivity.getToken(), this.state.channel_name, true);
                        this.setState({isFollowed: 1});
                    } else if (this.state.isFollowed === 2) {
                        RequestsController.dislikePublisher(MainActivity.getToken(), this.state.channel_name, true);
                        RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, false);
                        this.setState({isFollowed: 1});
                    } else {
                        RequestsController.dislikeCategory(MainActivity.getToken(), this.state.channel_name, false);
                        this.setState({isFollowed: 0});
                    }
                    Animated.timing(
                        this.state.scaleRemove, {
                            toValue: 1, duration: 500
                        }
                    ).start(() => {
                        if (this.state.isFollowed === 1)
                            this.props.navigation.pop()
                    });
                }}>
                    <Animated.Image
                        source={require('./components/img/ic_del.png')}
                        style={{
                            width: 28,
                            height: 28,
                            marginLeft: 15,
                            tintColor: colorTint,
                            transform: [{scale: this.animRemove}]
                        }}/>

                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.state.scaleAdd.setValue(0);
                        Animated.timing(
                            this.state.scaleAdd, {
                                toValue: 1, duration: 400
                            }
                        ).start();
                        if (this.state.isFollowed === 0) {
                            RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, true);
                            this.setState({isFollowed: 2});
                        } else if (this.state.isFollowed === 1) {
                            RequestsController.dislikePublisher(MainActivity.getToken(), this.state.channel_name, false);
                            RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, true);
                            this.setState({isFollowed: 2});
                        } else {
                            RequestsController.followPublisher(MainActivity.getToken(), this.state.channel_name, false);
                            this.setState({isFollowed: 0});
                        }
                    }}>

                    <Animated.Image
                        source={require('./components/img/ic_add.png')}
                        style={{
                            width: 28,
                            height: 28,
                            marginLeft: 8,
                            tintColor: colorDark
                            , transform: [{scale: this.animAdd}]
                        }}/>

                </TouchableWithoutFeedback>
                <View style={{flex: 1}}/>
                <TouchableWithoutFeedback
                    onPress={() => {
                        MainActivity.sendEvent("Posts", "Click", "channelAvatar");
                        Linking.openURL(this.state.post_url);
                    }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginStart: 15,
                            marginEnd: 15
                        }}>
                        <Text
                            style={{
                                marginEnd: 7,
                                color: "#4A4A4A",
                                fontFamily: 'byekan'
                            }}>{this.state.channel_title}</Text>
                        <View
                            style={{
                                width: 42,
                                height: 42,
                                borderRadius: 21,
                                borderWidth: 1,
                                borderColor: "#CACACA",
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            {this.state.isDownVoteByMe ?
                                <Grayscale>
                                    <Image
                                        source={{uri: this.state.channel_url}}
                                        style={{
                                            width: 35,
                                            height: 35,
                                            borderRadius: 16
                                        }}/>
                                </Grayscale> :
                                <Image
                                    source={{uri: this.state.channel_url}}
                                    style={{
                                        width: 35,
                                        height: 35,
                                        borderRadius: 16
                                    }}/>}
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    renderLogo() {
        return (
            <View style={{
                height: 52, backgroundColor: "#F5F4F4", flexDirection: 'row', alignItems: 'center'
            }}>
                <TouchableWithoutFeedback
                    onPress={() => this.props.navigation.pop()}>
                    <Image
                        source={require("./components/img/ic_back.png")}
                        style={{
                            alignSelf: 'flex-start', height: 22, width: 22, resizeMode: 'contain',
                            marginTop: 15, marginLeft: 25
                        }}/>
                </TouchableWithoutFeedback>
                <View style={{
                    position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,
                    alignItems: 'center', justifyContent: 'center'
                }}>
                    <Image source={require("./components/img/ic_logo.png")}
                           style={{
                               height: 32, width: 32, resizeMode: 'contain'
                           }}/>
                </View>
            </View>
        );
    }

    renderButtons() {
        let saveIcon = this.state.isSavedByMe && !this.state.isDownVoteByMe ?
            require('./components/img/ic_save_filled.png') : require('./components/img/ic_save.png');
        let shareIcon = this.state.isSharedByMe && !this.state.isDownVoteByMe ?
            require('./components/img/ic_share_filled.png') : require('./components/img/ic_share.png');
        let upvoteIcon = this.state.isDownVoteByMe ? require('./components/img/ic_upvote_disabled.png') :
            this.state.isUpVotedByMe ? require('./components/img/ic_upvote_filled.png') : require('./components/img/ic_upvote.png');
        let downvoteIcon = this.state.isDownVoteByMe ? require('./components/img/ic_downvote_filled.png') :
            require('./components/img/ic_downvote.png');
        let reportIcon = this.state.isReportedByMe && !this.state.isDownVoteByMe ?
            require('./components/img/ic_report_filled.png') : require('./components/img/ic_report.png');

        return (
            <View style={{
                flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around',
                marginTop: 0, paddingStart: 10, paddingEnd: 10, paddingTop: 4,
                marginBottom: 5
            }}>
                <View style={{
                    position: 'absolute', top: 10, bottom: 10, borderBottomLeftRadius: 12,
                    right: 0, left: 0, backgroundColor: "#FDFEFF", borderBottomRightRadius: 12
                }}/>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.state.scaleSaved.setValue(0);
                        Animated.timing(
                            this.state.scaleSaved, {
                                toValue: 1, duration: 400
                            }
                        ).start();
                        RequestsController.sendSave(this.token,
                            this.state.post_id, this.state.isSavedByMe);
                        MainActivity.sendEvent("Posts", "Click", "save");
                        this.setState((prev) => {
                            return {isSavedByMe: !prev.isSavedByMe};
                        });
                    }}>
                    <Animated.Image
                        style={{
                            width: 42,
                            height: 42,
                            transform: [{scale: this.animSaved}]
                        }}
                        source={saveIcon}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleUpVote.setValue(0);
                    Animated.timing(
                        this.state.scaleUpVote, {
                            toValue: 1, duration: 400
                        }
                    ).start();
                    RequestsController.sendUpVote(this.token,
                        this.state.post_id, this.state.isUpVotedByMe);
                    if (this.state.isDownVoteByMe) {
                        RequestsController.sendDownVote(this.token,
                            this.state.post_id, true);
                        this.state.isDownVoteByMe = false;
                        this.state.downVoteCount--;
                    }
                    MainActivity.sendEvent("Posts", "Click", "upvote");
                    this.setState((prev) => {
                        return {isUpVotedByMe: !prev.isUpVotedByMe};
                    });
                }}>
                    {<Animated.Image style={{
                        width: 65, height: 65,
                        transform: [{scale: this.animUpVote}]
                    }}
                                     source={upvoteIcon}/>}
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleShare.setValue(0);
                    this.setState(() => {
                        return {isSharedByMe: true};
                    });
                    Animated.timing(
                        this.state.scaleShare, {
                            toValue: 1, duration: 400
                        }
                    ).start();
                    RequestsController.sendShare(this.token, this.state.post_id);
                    MainActivity.sendEvent("Posts", "Click", "share");
                    setTimeout(() => Share.share({
                        message: this.state.channel_title + '\n' +
                            this.state.post_url + '\n' +
                            this.state.message
                    }), 400);
                }}>
                    <Animated.Image style={{width: 42, height: 42, transform: [{scale: this.animShare}]}}
                                    source={shareIcon}/>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.state.scaleDownVote.setValue(0);
                    Animated.timing(
                        this.state.scaleDownVote, {
                            toValue: 1,
                            duration: 400
                        }
                    ).start();
                    RequestsController.sendDownVote(this.token,
                        this.state.post_id, this.state.isDownVoteByMe);
                    if (this.state.isUpVotedByMe) {
                        RequestsController.sendUpVote(this.token,
                            this.state.post_id, true);
                        this.state.isUpVotedByMe = false;
                        this.state.upVoteCount--;
                    }
                    MainActivity.sendEvent("Posts", "Click", "downvote");
                    this.setState((prev) => {
                        return {isDownVoteByMe: !prev.isDownVoteByMe};
                    });
                }}>
                    {<Animated.Image
                        style={{
                            width: 65, height: 65,
                            transform: [{scale: this.animDownVote}]
                        }}
                        source={downvoteIcon}/>}
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback
                    onPress={() => {
                        this.state.scaleReport.setValue(0);
                        this.setState(() => {
                            return {isReportedByMe: true};
                        });
                        Animated.timing(
                            this.state.scaleReport, {
                                toValue: 1,
                                duration: 400
                            }
                        ).start();
                        this.reportChannel(this.state.post_id);
                    }}>
                    <Animated.Image
                        style={{
                            width: 42,
                            height: 42,
                            transform: [{scale: this.animReport}]
                        }}
                        source={reportIcon}/>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    reportChannel(post_id) {
        this.reportPostId = post_id;
        this.setState(() => {
            return {maskShow: !this.state.maskShow};
        });
        if (post_id === null) {
            this.state.marginBottomSecond.setValue(-700);
            this.state.marginBottomAnimated.setValue(-500);
            return
        }
        this.state.maskOp.setValue(0.0);
        Animated.timing(
            this.state.maskOp, {
                toValue: 0.75,
                duration: 1000,
                delay: 300
            }
        ).start();
        this.state.marginBottomSecond.setValue(-700);
        Animated.timing(
            this.state.marginBottomSecond, {
                toValue: 0,
                duration: 400,
                delay: 600
            }).start();
    }

    renderDateTime() {
        const momentDate = new Date(this.state.date_time);
        const jalaali = require("jalaali-js");
        let jalali = jalaali.toJalaali(momentDate);
        let viewCount = this.state.views > 1000000 ? (this.state.views / 1000000).toFixed(1) + " M" : (
            this.state.views > 1000 ? (this.state.views / 1000).toFixed(1) + " K" : this.state.views);
        return (
            <View style={{
                width: 160,
                marginBottom: 5,
                marginLeft: 26,
                borderWidth: 1,
                borderColor: "#9B9B9B",
                borderRadius: 3,
                flexDirection: 'row',
                alignItems: 'center'
            }}>
                <View style={{
                    marginTop: 20,
                    backgroundColor: "#FFFFFF",
                    opacity: 0.7
                }}/>
                <Image
                    source={require('./components/img/ic_seen.png')}
                    resizeMode={'contain'}
                    style={{
                        marginLeft: 4,
                        tintColor: '#CACACA'
                    }}/>
                <Text style={{fontFamily: 'IRANSansMobile', color: "#9B9B9B", marginLeft: 5}}>
                    {viewCount}
                </Text>
                <View style={{flex: 1}}/>
                <Text style={{fontFamily: 'IRANSansMobile', color: "#9B9B9B", marginRight: 4}}>
                    {jalali.jy % 100 + "/" +
                    (jalali.jm >= 10 ? jalali.jm : "0" + jalali.jm) + "/" +
                    (jalali.jd >= 10 ? jalali.jd : "0" + jalali.jd)}
                </Text>
            </View>
        );
    }

    parseDateTime() {
        const momentDate = new Date(this.state.date_time);
        const jalaali = require("jalaali-js");
        let jalali = jalaali.toJalaali(momentDate);
        let months = ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "تیر", "شهریور",
            "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"];
        let value = jalali.jy + " " + months[jalali.jm] + " " + jalali.jd;
        var arabicNumbers = ['۰', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
        var chars = value.split('');
        for (let index in chars)
            if (chars[index] >= '0' && chars[index] <= '9')
                chars[index] = arabicNumbers[chars[index] - '0'];
        return chars.join('');
    }

    renderText() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "#FFFFFF"}}>
                {this.renderLogo()}
                {this.renderChannel()}
                <View style={{
                    borderRadius: 15, borderColor: "#CCC", borderWidth: 1, flex: 1,
                    marginStart: 15, marginEnd: 15, marginTop: 15, overflow: 'hidden'
                }}>
                    <ScrollView>
                        <TouchableWithoutFeedback
                            onPress={this.handleImagePress}
                            onLongPress={
                                () => {
                                    Clipboard.setString(this.state.message);
                                    this.refs.toast.show('متن پست کپی شد', 1200);
                                }
                            }>
                            <View>
                                <Hyperlink
                                    linkDefault={true}
                                    linkStyle={{color: '#2980b9'}}>
                                    <Text
                                        style={{
                                            fontSize: 14,
                                            paddingStart: 16,
                                            paddingEnd: 16,
                                            textAlign: 'right',
                                            fontFamily: 'IRANSansMobile',
                                            color: "#4A4A4A",
                                        }}>
                                        {this.state.message}
                                    </Text>
                                </Hyperlink>
                                {this.renderDateTime()}
                            </View>
                        </TouchableWithoutFeedback>
                    </ScrollView>
                    {this.renderButtons()}
                </View>
                {this.mask()}
                <Toast ref={"toast"}/>
            </SafeAreaView>
        );
    }

    renderImage() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: "#FFFFFF"
                }}>
                {this.renderLogo()}
                {this.renderChannel()}
                <View
                    style={{
                        borderRadius: 15,
                        borderColor: "#CCC",
                        borderWidth: 1,
                        flex: 1,
                        marginStart: 15,
                        marginEnd: 15,
                        marginTop: 15,
                        overflow: 'hidden'
                    }}>
                    <ScrollView
                        style={{marginBottom: 10}}
                        showsVerticalScrollIndicator={false}>
                        <View>
                            {this.state.isDownVoteByMe ?
                                <Grayscale>
                                    <ImageLoad
                                        source={{uri: this.state.media_url}}
                                        resizeMode={'cover'}
                                        placeholderSource={require('./components/img/place_holder.png')}
                                        style={{
                                            width: this.state.width,
                                            height: this.state.height
                                        }}/>
                                </Grayscale> :
                                <ImageLoad
                                    source={{uri: this.state.media_url}}
                                    resizeMode={'cover'}
                                    placeholderSource={require('./components/img/place_holder.png')}
                                    style={{
                                        width: this.state.width,
                                        height: this.state.height
                                    }}/>}
                            <TouchableWithoutFeedback
                                onLongPress={
                                    () => {
                                        Clipboard.setString(this.state.message);
                                        this.refs.toast.show('متن پست کپی شد', 1200);
                                    }
                                }>
                                <View>
                                    <Hyperlink linkDefault={true} linkStyle={{color: '#2980b9'}}>
                                        <Text
                                            style={{
                                                paddingEnd: 25,
                                                paddingStart: 25,
                                                marginTop: 10,
                                                textAlign: 'right',
                                                color: "#4A4A4A",
                                                fontFamily: 'IRANSansMobile'
                                            }}>
                                            {this.state.message}
                                        </Text>
                                    </Hyperlink></View>
                            </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                    {this.renderButtons()}
                </View>
                {this.mask()}
                <Toast ref={"toast"}/>
            </SafeAreaView>
        );
    }

    renderVideo() {
        let thumbnail = this.state.isDownVoteByMe ?
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    backgroundColor: "#000000"
                }}>
                <Grayscale>
                    <ImageLoad
                        source={{uri: this.state.thumbnail}}
                        resizeMode={'cover'}
                        placeholderSource={require('./components/img/place_holder.png')}
                        style={{
                            width: this.state.width,
                            height: this.state.height,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>

                        <View style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <TouchableWithoutFeedback onPress={() => {
                                MainActivity.sendEvent("Posts", "Click", "playVideo");
                                this.setState(
                                    () => {
                                        return {isPlaying: true};
                                    });
                            }}>
                                <Image source={require('./components/img/ic_play.png')}/>
                            </TouchableWithoutFeedback>
                        </View>
                    </ImageLoad>
                </Grayscale>
            </View> :
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    backgroundColor: "#000000"
                }}>
                <ImageLoad
                    source={{uri: this.state.thumbnail}}
                    resizeMode={'cover'}
                    placeholderSource={require('./components/img/place_holder.png')}
                    style={{
                        width: this.state.width,
                        height: this.state.height,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                    <View style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <TouchableWithoutFeedback onPress={() => {
                            MainActivity.sendEvent("Posts", "Click", "playVideo");
                            ImageCacheManager({})
                                .downloadAndCacheUrl(this.state.media_url)
                                .then(res => {
                                    this.setState({cachedVideoURI: res, isPlaying: true});
                                })
                                .catch(err => {
                                    console.log('error ', err);
                                });
                        }}>
                            <Image source={require('./components/img/ic_play.png')}/>
                        </TouchableWithoutFeedback>
                    </View>
                </ImageLoad>
            </View>;
        let player = <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Video
                source={{uri: this.state.cachedVideoURI}}
                onProgress={(time) => {
                    this.timePlayed = Math.floor(time.currentTime);
                }}
                style={{
                    width: this.state.width, height: this.state.height, backgroundColor: "#000000"
                }}
                ref={(ref) => this.refPlayer = ref}/>
            <TouchableWithoutFeedback onPress={() =>
                this.props.navigation.navigate('FullScreen', {
                    media_url: this.state.media_url, isVideo: this.state.post_type === 3, time: this.timePlayed,
                    token: this.token, post_id: this.state.post_id
                })}>
                <Image source={require('./components/img/ic_fullscreen.png')}
                       style={{
                           position: 'absolute',
                           top: 10,
                           left: (this.maxWidth - this.state.width) / 2 + 20,
                           width: 25,
                           height: 25
                       }}/>
            </TouchableWithoutFeedback>
        </View>;
        let minute = Math.floor(this.state.duration / 60);
        let second = this.state.duration % 60;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "#FFFFFF"}}>
                {this.renderLogo()}
                {this.renderChannel()}
                <View style={{
                    borderRadius: 15, borderColor: "#CCC", borderWidth: 1, flex: 1,
                    marginStart: 15, marginEnd: 15, marginTop: 15, overflow: 'hidden'
                }}>
                    <ScrollView>
                        <View>
                            <TouchableWithoutFeedback onPress={this.handleImagePress}>

                                <View>
                                    <View>
                                        {!this.state.isPlaying ? thumbnail : player}
                                        <View style={{
                                            position: 'absolute',
                                            top: 10,
                                            right: 20,
                                            borderWidth: 1,
                                            borderColor: "#FFFFFF",
                                            borderRadius: 3,
                                            alignItems: 'center'
                                        }}>
                                            <View style={{
                                                position: 'absolute',
                                                top: 0,
                                                bottom: 0,
                                                right: 0,
                                                left: 0,
                                                backgroundColor: "#4A4A4A",
                                                opacity: 0.7
                                            }}/>
                                            <Text style={{
                                                fontFamily: 'IRANSansMobile', color: "#FFFFFF", marginLeft: 5,
                                                marginRight: 5
                                            }}>
                                                {(minute >= 10 ? minute : "0" + minute) + ":" +
                                                (second >= 10 ? second : "0" + second)}
                                            </Text>
                                        </View>
                                    </View>
                                    <TouchableWithoutFeedback
                                        onPress={this.handleImagePress}
                                        onLongPress={
                                            () => {
                                                Clipboard.setString(this.state.message);
                                                this.refs.toast.show('متن پست کپی شد', 1200);
                                            }
                                        }>
                                        <View>
                                            <Hyperlink linkDefault={true} linkStyle={{color: '#2980b9'}}>
                                                <Text style={{
                                                    paddingEnd: 15, paddingStart: 15, marginTop: 10,
                                                    textAlign: 'right', fontFamily: 'IRANSansMobile'
                                                    , color: "#4A4A4A",
                                                }}>{this.state.message}</Text></Hyperlink>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {this.renderDateTime()}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                    {this.renderButtons()}
                </View>
                {this.mask()}
                <Toast ref={"toast"}/>
            </SafeAreaView>
        );
    }

    renderGif() {
        let thumbnail = this.state.isDownVoteByMe ?
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    backgroundColor: "#000000"
                }}>
                <Grayscale>
                    <ImageLoad
                        source={{uri: this.state.thumbnail}}
                        resizeMode={'cover'}
                        placeholderSource={require('./components/img/place_holder.png')}
                        style={{
                            width: this.state.width,
                            height: this.state.height,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>

                        <View style={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <TouchableWithoutFeedback onPress={() => {
                                MainActivity.sendEvent("Posts", "Click", "playVideo");
                                this.setState(
                                    () => {
                                        return {isPlaying: true};
                                    });
                            }}>
                                <Image source={require('./components/img/ic_gif.png')}/>
                            </TouchableWithoutFeedback>
                        </View>
                    </ImageLoad>
                </Grayscale>
            </View> :
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    backgroundColor: "#000000"
                }}>
                <ImageLoad
                    source={{uri: this.state.thumbnail}}
                    resizeMode={'cover'}
                    placeholderSource={require('./components/img/place_holder.png')}
                    style={{
                        width: this.state.width,
                        height: this.state.height,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                    <View style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <TouchableWithoutFeedback onPress={() => {
                            MainActivity.sendEvent("Posts", "Click", "playVideo");
                            this.setState(
                                () => {
                                    return {isPlaying: true};
                                });
                        }}>
                            <Image source={require('./components/img/ic_gif.png')}/>
                        </TouchableWithoutFeedback>
                    </View>
                </ImageLoad>
            </View>;
        let player = <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Video
                source={{uri: this.state.media_url}}
                onProgress={(time) => {
                    this.timePlayed = Math.floor(time.currentTime);
                }}
                selectedAudioTrack={{type: "disabled"}}
                repeat={true}
                disableBack={true}
                style={{
                    width: this.state.width, height: this.state.height,
                    backgroundColor: "#000000"
                }}
                onEnterFullscreen={() =>
                    this.props.navigation.navigate('FullScreen', {
                        media_url: this.state.media_url, isVideo: this.state.post_type === 3, time: this.timePlayed,
                        token: this.token, post_id: this.state.post_id
                    })}
                ref={(ref) => this.refPlayer = ref}/>
        </View>;
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: "#FFFFFF"}}>
                {this.renderLogo()}
                {this.renderChannel()}
                <View style={{
                    borderRadius: 15, borderColor: "#CCC", borderWidth: 1, flex: 1,
                    overflow: 'hidden', marginStart: 15, marginEnd: 15, marginTop: 15
                }}>
                    <ScrollView>
                        <View>
                            <TouchableWithoutFeedback onPress={this.handleImagePress}>
                                <View>
                                    <View>
                                        {!this.state.isPlaying ? thumbnail : player}
                                    </View>
                                    <TouchableWithoutFeedback
                                        onPress={this.handleImagePress}
                                        onLongPress={
                                            () => {
                                                Clipboard.setString(this.state.message);
                                                this.refs.toast.show('متن پست کپی شد', 1200);
                                            }
                                        }>
                                        <View>
                                            <Hyperlink linkDefault={true} linkStyle={{color: '#2980b9'}}>
                                                <Text style={{
                                                    paddingEnd: 15, paddingStart: 15, marginTop: 10,
                                                    textAlign: 'right', fontFamily: 'IRANSansMobile'
                                                    , color: "#4A4A4A",
                                                }}>{this.state.message}</Text></Hyperlink>
                                        </View>
                                    </TouchableWithoutFeedback>
                                    {this.renderDateTime()}
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </ScrollView>
                    {this.renderButtons()}
                </View>
                {this.mask()}
                <Toast ref={"toast"}/>
            </SafeAreaView>
        );
    }

    renderBeytoote() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: "#FFFFFF"
                }}>
                {this.renderLogo()}
                {this.renderChannel()}
                <View style={{
                    borderRadius: 15,
                    borderColor: "#CCC",
                    borderWidth: 1,
                    flex: 1,
                    marginStart: 15,
                    marginEnd: 15,
                    marginTop: 5,
                    marginBottom: 5,
                    overflow: 'hidden'
                }}>
                    <ScrollView>
                        <View>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansBold',
                                    fontSize: 18,
                                    marginTop: 5,
                                    marginBottom: 5,
                                    paddingStart: 10,
                                    paddingEnd: 10
                                }}>
                                {this.state.title}
                            </Text>
                            <View
                                style={{
                                    height: 1,
                                    backgroundColor: '#CCC',
                                    alignItems: 'center'
                                }}/>
                            <Text
                                style={{
                                    fontFamily: 'IRANSansMobile',
                                    fontSize: 15,
                                    width: '100%',
                                    textAlign: 'left',
                                    paddingEnd: 10,
                                    paddingStart: 10
                                }}>
                                {this.parseDateTime()}
                            </Text>
                            <View
                                style={{
                                    height: 1,
                                    backgroundColor: '#CCC'
                                }}/>
                            <FlatList
                                style={{marginTop: 20}}
                                data={this.state.instant_view_sections}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item}) =>
                                    <View
                                        style={{
                                            width: '100%',
                                            alignItems: 'center',
                                            marginBottom: 5
                                        }}>
                                        {item.title !== "" ?
                                            <Text
                                                style={{
                                                    fontFamily: 'IRANSansBold',
                                                    fontSize: 15,
                                                    marginTop: 20,
                                                    paddingStart: 10,
                                                    paddingEnd: 10
                                                }}>
                                                {item.title}
                                            </Text> :
                                            null
                                        }
                                        {item.media.type === 0 || item.media.type === 1 ?
                                            item.media.type === 0 ?
                                                <AutosizeImage
                                                    src={item.media.url}
                                                    description={item.media.description}
                                                    grayState={this.state.isDownVoteByMe}
                                                    resizeMode={'cover'}
                                                />
                                                :
                                                <AutosizeImage
                                                    src={this.state.thumbnail}
                                                    description={item.media.description}
                                                    grayState={this.state.isDownVoteByMe}
                                                    video={item.media.url}
                                                    resizeMode={'cover'}>
                                                    <View style={{
                                                        position: 'absolute',
                                                        top: 0,
                                                        left: 0,
                                                        right: 0,
                                                        bottom: 0,
                                                        justifyContent: 'center',
                                                        alignItems: 'center'
                                                    }}>
                                                        <TouchableWithoutFeedback
                                                            onPress={() => {
                                                                MainActivity.sendEvent("Posts", "Click", "playVideo");
                                                                this.setState(
                                                                    () => {
                                                                        return {isPlaying: true};
                                                                    });
                                                            }}>
                                                            <Image
                                                                source={require('./components/img/ic_play.png')}/>
                                                        </TouchableWithoutFeedback>
                                                    </View>
                                                </AutosizeImage>
                                            :
                                            null
                                        }
                                        {item.paragraphs.length !== 0 ?
                                            <Hyperlink linkDefault={true} linkStyle={{color: '#2980b9'}}>
                                                <Text
                                                    style={{
                                                        fontFamily: 'IRANSansMobile',
                                                        fontSize: 18,
                                                        marginTop: 20,
                                                        paddingStart: 10,
                                                        paddingEnd: 10
                                                        , color: "#4A4A4A",
                                                    }}>
                                                    {this.renderParagraphs(item.paragraphs)}
                                                </Text>
                                            </Hyperlink> :
                                            null
                                        }
                                    </View>
                                }/>
                        </View>
                    </ScrollView>
                    {this.renderButtons()}
                </View>
                {this.mask()}
                <Toast ref={"toast"}/>
            </SafeAreaView>
        );
    }

    renderParagraphs(paragraphs) {
        let context = "";
        paragraphs.forEach(item => {
            context += item + '\n';
        });
        return context;
    }

    handleImagePress(e) {
        const now = new Date().getTime();
        if (this.lastImagePress && (now - this.lastImagePress) < DOUBLE_PRESS_DELAY) {
            this.handleImageDoublePress();
        }
        else {
            this.lastImagePress = now;
        }
    }

    handleImageDoublePress() {
        this.state.scaleUpVote.setValue(0);
        Animated.timing(
            this.state.scaleUpVote, {
                toValue: 1,
                duration: 400
            }
        ).start();
        RequestsController.sendUpVote(this.token,
            this.state.post_id, false);
        if (this.state.isDownVoteByMe) {
            RequestsController.sendDownVote(this.token,
                this.state.post_id, true);
            this.state.isDownVoteByMe = false;
            this.state.downVoteCount--;
        }
        MainActivity.sendEvent("Posts", "Click", "upvote");
        this.state.isUpVotedByMe = true;
    }
}