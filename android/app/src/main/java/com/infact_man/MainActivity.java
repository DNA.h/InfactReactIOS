package com.infact.man;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import org.devio.rn.splashscreen.SplashScreen; // here

public class MainActivity extends ReactActivity {
    public static Activity activity;

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        activity = this;
        return "infact_man";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SplashScreen.show(this);  // here
        super.onCreate(savedInstanceState);
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new AlarmActivityDelegate(this, getMainComponentName());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Intent intent = new Intent("onConfigurationChanged");
        intent.putExtra("newConfig", newConfig);
        this.sendBroadcast(intent);
    }

    private static class AlarmActivityDelegate extends ReactActivityDelegate {
        private static final String NOTIF_TYPE = "notif_type";
        private static final String POST_ID = "post_id";
        private Bundle mInitialProps = null;
        private final @Nullable
        Activity mActivity;

        public AlarmActivityDelegate(Activity activity, String mainComponentName) {
            super(activity, mainComponentName);
            this.mActivity = activity;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            // bundle is where we put our alarmID with launchIntent.putExtra
            Bundle bundle = mActivity.getIntent().getExtras();
            if (bundle != null && bundle.containsKey(NOTIF_TYPE)) {
                mInitialProps = new Bundle();
                // put any initialProps here
                mInitialProps.putString(NOTIF_TYPE, bundle.getString(NOTIF_TYPE));
                mInitialProps.putString(POST_ID, bundle.getString(POST_ID));
            }
            if (bundle != null){
                Log.d("bundle ",bundle.toString());
            }
            super.onCreate(savedInstanceState);
        }

        @Override
        protected Bundle getLaunchOptions() {
            if (mInitialProps == null) {
                mInitialProps = new Bundle();
            }
            return mInitialProps;
        }
    }
}
