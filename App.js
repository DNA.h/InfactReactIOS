import {createStackNavigator} from 'react-navigation';
import React, {Component} from 'react';
import MainActivity from "./app/MainActivity";
import SettingsActivity from "./app/SettingsActivity";
import {SmallListActivity} from "./app/SmallListActivity";
import SingleItemActivity from "./app/SingleItemActivity";
import FullScreenActivity from "./app/FullScreenActivity";
import ContactUs from "./app/components/ContactUs";
import {RobotView} from "./app/components/RobotView";
import ViewSingleChannel from "./app/components/ViewSingleChannel";
import firebase from "react-native-firebase";

console.disableYellowBox = true;

export default class App extends Component {
    static post_id;

    static getPostID(){
        return App.post_id
    }
    render() {
        //console.log('App props', this.props);
        if (this.props.hasOwnProperty('post_id'))
            App.post_id = this.props.post_id;
        else
            App.post_id = null;
        return (<RootStack/>);
    }
}

const RootStack = createStackNavigator(
    {
        Home: {screen: MainActivity},
        List: {screen: SmallListActivity},
        Setting: {screen: SettingsActivity},
        SingleItem: {screen: SingleItemActivity},
        FullScreen: {screen: FullScreenActivity},
        Contact: {screen: ContactUs},
        Robot: {screen: RobotView},
        SingleChannel: {screen: ViewSingleChannel}
    },
    {
        initialRouteName: 'Home',
        navigationOptions: {header: null},
    }
);